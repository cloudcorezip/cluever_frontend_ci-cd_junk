import axios from "axios";
import { CHECK_TOKEN_URI, LOG_IN_URI } from "../constants/apis";

export const isTokenValid = (token) => {
  console.log("istokenvalid token")
  console.log(token)
  if (token == undefined || token.name === "Error") {
    return false
  } else {
    const config = {
      headers: {
        Authorization: token,
      }
    }
    return axios.post(CHECK_TOKEN_URI, null,config)
    .then((response) => {
      return true
    })
    .catch((error) => {
      return false
    });
  }
}

export const createNewToken = (phone_no, password) => {
  return axios.post(LOG_IN_URI, {
    phone_no: phone_no,
    password: password,
  }, null)
  .then((response) => {
    return response
  })
  .catch((error) => {
    return error
  });
}
