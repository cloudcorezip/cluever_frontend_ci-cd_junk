import axios from "axios";
import { GET_PROFILE_URI } from "../constants/apis";

export const getProfile = (token) => {
  console.log("token getprofile")
  console.log(token)
  return axios.post(GET_PROFILE_URI, null, {
    headers: {
      'Authorization': token
    }
  }).then((response) => {
    // console.log("get profile response")
    // console.log(response)
    return response
  }).catch(() => {
    return null
  })
}