import axios from "axios";
import { GET_INSTALLMENTS_URI } from "../constants/apis" 

export const getInstallment = (token) => {

  var items = null;
  return axios.post(GET_INSTALLMENTS_URI, null, {
    headers: {
      'Authorization': token
    }
  }).then((response) => {
    //add collapsed value for base and installment object
    if (Object.keys(response.data[0]).length > 0) {
      items = response.data[0];
      console.log(items)
      Object.keys(items).map((item, key) => {
        if(items[key].installments != null) {
          items[key]["collapsed"] = true
          items[key].installments["collapsed"] = true
        } else {
          items[key]["collapsed"] = true
          items[key].installments = []
          items[key].installments["collapsed"] = false
        }
      })
      console.log("installments collapsed ahhaah")
      console.log(items)
    }
    return items
  }).catch((err) => {
    return err
  })
}