export const TAGIHAN_USER_URI = "http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/api/v1a/helper/get_all_pending_payment";

export const GET_INSTALLMENTS_URI = "http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/api/v1a/installment/initialize_list";
export const IS_INSTALLMENTS_UPDATED_URI = "http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/api/v1a/installment/check_for_update";

export const CHECK_TOKEN_URI = "http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/check_token";
export const LOG_IN_URI = "http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/authenticate"

export const GET_PROFILE_URI = "http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/api/v1a/helper/get_profile"

// const GET_PAYMENT_URI = "http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/api/v1a/payment/charge";
