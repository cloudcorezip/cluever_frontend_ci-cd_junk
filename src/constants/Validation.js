VALID_EMAIL_REGEX = /([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,8})/i
VALID_FULLNAME_REGEX = /[a-zA-Z ]{5,30}/
VALID_PHONENO_REGEX = /08[0-9]{9,11}\b/
VALID_PASSWORD_REGEX = /^.{6,}$/
