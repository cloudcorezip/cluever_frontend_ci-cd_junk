import * as ACTION_TYPES from "../constants/ActionTypes";

export const UserLoginRequest = ( phone_no, password ) => ({
  type: USER_LOGIN_REQUEST,
  payload: {
    data: {
      phone_no: phone_no,
      password: password
    }
  }
})

export const userLogoutRequest = () => ({
  type: USER_LOGOUT_REQUEST,
  payload: {}
})

export const setUserToken = (token) => ({
	type: SET_USER_TOKEN,
	payload: {
		token: token
	}
})

