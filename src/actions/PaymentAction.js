import * as ACTION_TYPES from "../constants/ActionTypes";

export const getPaymentResponse = ( 
	// gross_amount, bank, fullname, token
	data
	 ) => ({
  type: GET_PAYMENT_RESPONSE,
  payload : {
  	data: data
  }
  // {
  //   gross_amount: gross_amount,
  //   bank: bank,
  //   fullname: fullname,
  //   token: token
  // }
})