import * as ACTION_TYPES from "../constants/ActionTypes"

// export const getInstallmentsRequest = ( token ) => ({
//   type: GET_INSTALLMENTS_REQUEST,
//   payload : {
//     token: token
//   }
// })

export const setInstallment = (data) => ({
	type: SET_INSTALLMENT,
	payload: {
		data: data
	}
})