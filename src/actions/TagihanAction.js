import * as ACTION_TYPES from "../constants/ActionTypes";

// export const getTagihanRequest = (token) => ({
//   type: GET_TAGIHAN_RESPONSE,
//   payload: {
//     token: token
//   }
// })

export const setTagihan = (data) => ({
  type: SET_TAGIHAN,
  payload: {
    data: data
  }
})