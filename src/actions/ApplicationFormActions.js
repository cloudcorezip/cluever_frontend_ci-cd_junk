import * as ACTION_TYPES from "../constants/ActionTypes";

export const FormSubmitApplication = ( data ) => ({
  type: FORM_SUBMIT_APPLICATION,
  payload: {
    data: data
  }
})

export const FormClearSubmission = () => ({
  type: FORM_CLEAR_SUBMISSION,
  payload: ""
})
