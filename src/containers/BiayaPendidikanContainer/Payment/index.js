import React, { Component } from "react";
import BiayaPendidikanPayment from "../../../components/BiayaPendidikan/Payment";

export default class BiayaPendidikanPaymentContainer extends Component {

	navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
	  return (
	    <BiayaPendidikanPayment
	      navigation={this.props.navigation}
	      onPress={(data) => this.navigator(data)}
	    />
	  );
  }
}

