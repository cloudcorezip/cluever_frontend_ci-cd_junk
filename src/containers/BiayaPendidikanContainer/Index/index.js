import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import BiayaPendidikanIndexIsNotEmpty from "../../../components/BiayaPendidikan/Index/indexIsNotEmpty";
import BiayaPendidikanIndexEmpty from "../../../components/BiayaPendidikan/Index/indexIsEmpty";
import { setInstallment } from "../../../actions"
class BiayaPendidikanIndexContainer extends Component {

  state = {
    data: this.getListData(this.props.installment.items),
  }

  getListData(response) {
    if (response !== null) {
      var temp = [];
      for (var i = 0; i < Object.keys(response).length; i++) {
        temp.push(response[i]);
      }
      return temp;
    }
    return null;
  }


  placeholder =
    {
      tagihan: this.numberWithDots(this.props.tagihan.data.unpaid / 10)
    }

  numberWithDots(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  //TODO: Bug -> everytime refresh triggered
  //array of collapsed doesnt updated  
  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.data !== nextProps.installment.items) {
      var temp = []
      for (var i = 0; i < Object.keys(nextProps.installment.items).length; i++) {
        temp.push(nextProps.installment.items[i]);
      }
      console.log(temp)
      return {
        data: temp
      }
    }
    return null;
  }

  navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
    if (this.state.data < 1 || this.state.data === null) {
      return (
        <BiayaPendidikanIndexEmpty
          navigation={this.props.navigation}
          onPress={(data) => this.navigator(data)}
        />
      )
    } else {
      return (
        <BiayaPendidikanIndexIsNotEmpty
          data={this.state.data}
          navigation={this.props.navigation}
          onPress={(data) => this.navigator(data)}
          placeholder={this.placeholder}
        />
      )
    }
  }
}


const mapDispatchToProps = (dispatch) => (
  bindActionCreators({
    setInstallment
  }, dispatch)
)

const mapStateToProps = (state) => {
  const { installment, auth, tagihan } = state;
  return { installment, auth, tagihan };
}

export default connect(mapStateToProps, mapDispatchToProps)(BiayaPendidikanIndexContainer);