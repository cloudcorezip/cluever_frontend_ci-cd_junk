import React, { Component } from "react";
import Notification from "../../components/Notification";

export default class NotificationContainer extends Component {

	navigator(dest) {
    	this.props.navigation.navigate(dest);
  	}

  	render() {
	    return (
	      <Notification 
	      	navigation={this.props.navigation}
	      	onPress={(data) => this.navigator(data)}
	      />
	    );
  }
}

