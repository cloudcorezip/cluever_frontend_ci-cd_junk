import React, { Component } from "react";
import ProfileUser from "../../components/ProfileUser";

export default class ProfileUserContainer extends Component {

	navigator(dest) {
    	this.props.navigation.navigate(dest);
  	}

  	render() {
	    return (
	      <ProfileUser 
	      	navigation={this.props.navigation}
	      	onPress={(data) => this.navigator(data)}
	      />
	    );
  }
}

