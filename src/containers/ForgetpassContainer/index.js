import React, { Component } from "react";
import Forgetpass from "../../components/Forgetpass";

export default class ForgetpassContainer extends Component {

	navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
	  return (
	    <Forgetpass
	      navigation={this.props.navigation}
	      onPress={(data) => this.navigator(data)}
	    />
	  );
  }
}

