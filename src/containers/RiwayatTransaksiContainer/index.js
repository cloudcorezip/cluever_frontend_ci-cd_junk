import React, { Component } from "react";
import RiwayatTranskasi from "../../components/RiwayatTransaksi";

export default class RiwayatTransaksiContainer extends Component {

	navigator(dest) {
    	this.props.navigation.navigate(dest);
  	}

  	render() {
	    return (
	      <RiwayatTranskasi 
	      	navigation={this.props.navigation}
	      	onPress={(data) => this.navigator(data)}
	      />
	    );
  }
}

