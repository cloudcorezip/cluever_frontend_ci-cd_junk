import React, { Component } from "react";
import Home from "../../components/Home";

export default class HomeContainer extends Component {

	navigator(dest) {
    	this.props.navigation.navigate(dest);
  	}

  	render() {
	    return (
	      <Home 
	      	navigation={this.props.navigation}
	      	onPress={(data) => this.navigator(data)}
	      />
	    );
  }
}

