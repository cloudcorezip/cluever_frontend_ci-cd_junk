import React, { Component } from "react";
import Tagihan from "../../components/Tagihan";

export default class TagihanContainer extends Component {

	navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
	  return (
	    <Tagihan
	      navigation={this.props.navigation}
	      onPress={(data) => this.navigator(data)}
	    />
	  );
  }
}

