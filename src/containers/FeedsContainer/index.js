import React, { Component } from "react";
import Feeds from "../../components/Feeds";

const json = require("../../../public/feeds.json");

export default class FeedsContainer extends Component {
  constructor(props){
    super(props)
    
    this.state = {
      data: json.data
    }
  }

  navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
    return (
      <Feeds
        navigation={this.props.navigation}
        onPress={data => this.navigator(data)}
        data={this.state.data}
      />
    );
  }
}
