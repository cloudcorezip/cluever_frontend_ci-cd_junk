import React, { Component } from "react";
import DetailPembayaran from "../../components/DetailPembayaran";

export default class DetailPembayaranContainer extends Component {

	navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
	  return (
	    <DetailPembayaran
	      navigation={this.props.navigation}
	      onPress={(data) => this.navigator(data)}
	    />
	  );
  }
}

