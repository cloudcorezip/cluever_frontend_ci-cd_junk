import React, { Component } from "react";
import Register from "../../components/Register";

export default class RegisterContainer extends Component {

	navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  handleOpen(modal) {
    this.props.show(modal, { message: `This is a ${modal} modal` });
  }

  render() {
	  return (
	    <Register
	      navigation={this.props.navigation}
	      onPress={(data) => this.navigator(data)}
        handleOpen={(modal) => this.handleOpen(modal)}
	    />
	  );
  }
}

