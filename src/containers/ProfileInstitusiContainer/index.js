import React, { Component } from "react";
import ProfileInstitusi from "../../components/ProfileInstitusi";

export default class ProfileInstitusiContainer extends Component {

	navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
	  return (
	    <ProfileInstitusi
	      navigation={this.props.navigation}
	      onPress={(data) => this.navigator(data)}
	    />
	  );
  }
}
