import React, { Component } from "react";
import CariInstitusi from "../../components/CariInstitusi";

export default class CariInstitusiContainer extends Component {

	navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
	  return (
	    <CariInstitusi
	      navigation={this.props.navigation}
	      onPress={(data) => this.navigator(data)}
	    />
	  );
  }
}

