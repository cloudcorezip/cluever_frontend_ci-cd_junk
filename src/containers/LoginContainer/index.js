import React, { Component } from "react";
import Login from "../../components/Login";

export default class LoginContainer extends Component {

	constructor (props) {
		super(props);
	}

  navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
		return (
	    <Login
	      onPress={(data) => this.navigator(data)}
	      navigation={ this.props.navigation }
	    />
	  );
  }
}

