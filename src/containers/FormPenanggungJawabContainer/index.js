import React, { Component } from "react";
import FormPenanggungJawab from "../../components/FormPenanggungJawab";

export default class FormPenanggungJawabContainer extends Component {

	navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
	  return (
	    <FormPenanggungJawab
	      navigation={this.props.navigation}
	      onPress={(data) => this.navigator(data)}
	    />
	  );
  }
}
