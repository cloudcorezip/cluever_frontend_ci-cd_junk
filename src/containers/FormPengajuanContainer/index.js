import React, { Component } from "react";
import FormPengajuan from "../../components/FormPengajuan";

export default class FormPengajuanContainer extends Component {

	navigator(dest) {
    this.props.navigation.navigate(dest);
  }

  render() {
	  return (
	    <FormPengajuan
	      navigation={this.props.navigation}
	      onPress={(data) => this.navigator(data)}
	    />
	  );
  }
}
