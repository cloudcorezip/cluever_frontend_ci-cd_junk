import React, { Component } from "react";
import Sidebar from "../../components/Sidebar";
import { connect } from "react-redux";
import { userRequestLogout } from "../../actions";

export default class SidebarContainer extends Component {
  constructor(props) {
    super(props);
    this.data = [
      {
        name: "Home",
        route: "Home",
        icon: "home"
      },
      {
        name: "Biaya Pendidikan",
        route: "BiayaPendidikan",
        icon: "logo-usd"
      },
      {
        name: "Program Pendidikan",
        route: "Modal",
        icon: "book"
      },
      {
        name: "Marketplace",
        route: "Modal",
        icon: "cart"
      },
      {
        name: "Notifikasi",
        route: "Modal",
        icon: "notifications"
      },
      {
        name: "Logout",
        route: "Logout",
        icon: "log-out"
      }
    ];
  }

  navigator(data) {
    if (data.route === "Logout") {
      this.props.logout();
      this.props.navigation.navigate("Login");
    } else {
      this.props.navigation.navigate(data.route);
    }
  }

  render() {
    return (
      <Sidebar
        data={this.data}
        onPress={(data) => this.navigator(data)}/>
    );
  }
}
