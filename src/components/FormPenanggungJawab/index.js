import React, { Component } from "react";
import { Header, Left, Container, Button, Body, Title, Right, Text, Content, Footer, FooterTab } from "native-base";
import { StatusBar, Image, View, Switch, TextInput, StyleSheet, ScrollView, TouchableOpacity, Picker, ImageBackground, Dimensions } from "react-native";
import { Icon, Divider } from "react-native-elements";
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux'
import axios from 'axios'
import { bindActionCreators } from "redux";
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import { findNodeHandle } from 'react-native'
import TextInputState from 'react-native/lib/TextInputState'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as Validation from "../../constants/Validation"
import { getInstallment } from "../../utils";
import { setInstallment } from "../../actions"
import styles from './styles'
const thanks = require("../../../assets/tunggu_verif_3x.png")
const thanksA = require("../../../assets/pop_thanks.png")

var devHeight = Dimensions.get('window').height;
var devWidth = Dimensions.get('window').width;

class FormPenanggungJawab extends Component {

  constructor(props) {
      super(props);

      this.state = {
        isVisible: false,
        switch_value: false,
        name: "",
        born_city: "",
        date: "15-07-2019",
        address: "",
        phone: "",
        email: "",
        buttonDisabled: true,
        slip_1: null,
        slip_2: null,
        slip_3: null,
        slip_4: null,
        slip_5: null,
        image_self: null,
        image_ktp: null,
        slip_1_name: null,
        slip_2_name: null,
        slip_3_name: null,
        slip_4_name: null,
        slip_5_name: null,
        image_self_name: null,
        image_ktp_name: null,
        prov: "",
        kk: "",
        kec: "",
        kel: "",
        prov_index: -1,
        kk_index: -1,
        kec_index: -1,
        kel_index: -1,
        selected_prov_index: 0,
        selected_kk_index: 0,
        selected_kec_index: 0,
        selected_kel_index: null,
        name_error: null,
        born_city_error: null,
        date_error: null,
        address_error: null,
        phone_error: null,
        email_error: null,
        image_self_error: null,
        image_ktp_error: null,
        image_slip_error: null,
        arr_prov: null,
        arr_kk: null,
        arr_kec: null,
        arr_kel: null
      }
    }

  switchToggle(value) {
    if (value) {
      this.setState({
        switch_value: value,
        name: this.props.profile.data.user.fullname,
        born_city: "Bandung",
        date: "27-08-1980",
        address: "Jl. Kalimantan No. 7 Sumur Bandung",
        phone: this.props.profile.data.user.phone_no,
        email: this.props.profile.data.user.email
      })
    } else {
      this.setState({
        switch_value: value,
        name: "",
        born_city: "",
        date: "15-07-2019",
        address: "",
        phone: "",
        email: ""
      })
    }
  }

  uploadImage = (type, name) => {
    const options = {
      customButtons: [{ name: 'delete', title: 'Delete Photo...' }],
    };
    ImagePicker.showImagePicker(options , (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        this.setState({
          [type]: null,
          [name]: null
        });
        this.shiftState(type)
      } else {
        const source = { uri: response.uri };
        const file_name = response.fileName
        this.setState({
          [type]: source,
          [name]: file_name,
        });
      }
    });
  }

  shiftState = (type) => {
    if (this.state.slip_1 == null && this.state.slip_2 == null) {

    } else {

      stateArr = ["", "", "", "", ""]
      stateArr[0] = this.state.slip_1
      stateArr[1] = this.state.slip_2
      stateArr[2] = this.state.slip_3
      stateArr[3] = this.state.slip_4
      stateArr[4] = this.state.slip_5

      stateArrName = ["", "", "", "", ""]
      stateArrName[0] = this.state.slip_1_name
      stateArrName[1] = this.state.slip_2_name
      stateArrName[2] = this.state.slip_3_name
      stateArrName[3] = this.state.slip_4_name
      stateArrName[4] = this.state.slip_5_name
      var i;
      for (i = 0; i < 4; i++) {
        if (stateArr[i] == null) {
          stateArr[i] = stateArr[i+1]
          stateArr[i+1] = null
          stateArrName[i] = stateArrName[i+1]
          stateArrName[i+1] = null
          console.log(i)
        }
      }
      this.setState({
        slip_1: stateArr[0],
        slip_2: stateArr[1],
        slip_3: stateArr[2],
        slip_4: stateArr[3],
        slip_5: stateArr[4],
        slip_1_name: stateArrName[0],
        slip_2_name: stateArrName[1],
        slip_3_name: stateArrName[2],
        slip_4_name: stateArrName[3],
        slip_5_name: stateArrName[4],
      })
    }
  }

  formBuilder = () => {
    return {
      fullname: this.state.name,
      birthplace: this.state.born_city,
      dob: this.state.date,
      address: this.state.address,
      postcode_id: Object.keys(this.state.arr_kel)[parseInt(this.state.selected_kel_index)],
      self_image_id: "1",
      phone_no: this.state.phone,
      email: this.state.email,
      idcard_image_id: "1"
    }
  }

  buildImages = () => {
    self_image = {
      name: this.state.image_self_name,
      image: this.state.image_self
    }

    id_image = {
      name: this.state.image_ktp_name,
      image: this.state.image_ktp
    }

    salary = []

    stateArr = ["", "", "", "", ""]
    stateArr[0] = this.state.slip_1
    stateArr[1] = this.state.slip_2
    stateArr[2] = this.state.slip_3
    stateArr[3] = this.state.slip_4
    stateArr[4] = this.state.slip_5

    stateArrName = ["", "", "", "", ""]
    stateArrName[0] = this.state.slip_1_name
    stateArrName[1] = this.state.slip_2_name
    stateArrName[2] = this.state.slip_3_name
    stateArrName[3] = this.state.slip_4_name
    stateArrName[4] = this.state.slip_5_name

    var i;
    for (i = 0; i < 5; i++) {
      if (stateArr[i] != null && stateArrName[i] != null) {
        salary.push({
          name: stateArrName[i],
          image: stateArr[i]
        })
      }
    }

    return ({
      self_image: self_image, id_image: id_image, salary: salary
    })
  }

  formSubmit = () => {

    console.log(this.props.auth.token)

    var name_error = null
    var born_city_error = null
    var date_error = null
    var address_error = null
    var phone_error = null
    var email_error = null
    var image_self_error = null
    var image_ktp_error = null
    var image_slip_error = null

    if (this.state.name == "")
      name_error = "Nama harus diisi"
    else if (!VALID_FULLNAME_REGEX.test(this.state.name))
      name_error = "Nama yang dimasukkan salah. Pastikan nama terdiri dari huruf 5-30 karater."
    if (this.state.born_city == "")
      born_city_error = "Tempat lahir harus diisi"
    if (this.state.date == "")
      date_error = "Tanggal lahir harus diisi"
    if (this.state.address == "")
      address_error = "Alamat harus diisi"
    if (this.state.phone == "")
      phone_error = "Nomor telepon harus diisi"
    else if (!VALID_PHONENO_REGEX.test(this.state.phone))
      phone_error = "Nomor telepon yang dimasukkan salah. Pastikan nomor telepon terdiri dari angka 9-11 karakter"
    if (this.state.email == "")
      email_error = "Email harus diisi"
    else if (!VALID_EMAIL_REGEX.test(this.state.email))
      email_error = "Email yang dimasukkan salah. Pastikan format email benar"
    if (!this.state.image_self)
      image_self_error = "Foto diperlukan"
    if (!this.state.image_ktp)
      image_ktp_error = "Foto diperlukan"
    if (!this.state.slip_1)
      image_slip_error = "Foto diperlukan"

    this.setState({
      name_error: name_error,
      born_city_error: born_city_error,
      date_error: date_error,
      address_error: address_error,
      phone_error: phone_error,
      email_error: email_error,
      image_self_error: image_self_error,
      image_ktp_error: image_ktp_error,
      image_slip_error: image_slip_error,
    })

    if (name_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.nameTextInput))
      this.scrollToItem("nameTextInput")
      return
    }
    if (born_city_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.birthplaceTextInput))
      this.scrollToItem("birthplaceTextInput")
      return
    }
    if (date_error) {
      this.refs.birthDatePicker.onPressDate()
      this.scrollToItem("birthDatePicker")
      return
    }
    if (address_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.addressTextInput))
      this.scrollToItem("addressTextInput")
      return
    }
    if (phone_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.phoneNumberTextInput))
      this.scrollToItem("phoneNumberTextInput")
      return
    }
    if (email_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.emailTextInput))
      this.scrollToItem("emailTextInput")
      return
    }

    if (!name_error && !born_city_error && !date_error && !address_error && !phone_error && !email_error && !image_self_error && !image_ktp_error && !image_slip_error
      ) {
      axios.post('http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/api/v1a/application', {
        appform: this.props.appform, picform: this.formBuilder(), images: this.buildImages()
      }, {
        headers: {
          'Authorization': this.props.auth.token
        }
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });

      this.set_isVisible()
    }
  }

  async componentDidMount() {
    await axios.post('http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/api/v1a/helper/get_provinces', 
      null, {
      headers: {
        'Authorization': this.props.auth.token
      }
    })
    .then((response) => {
      this.setState({
        arr_prov: response.data,
        prov_index: 0,
        selected_prov_index: 0
      })
    })
    .catch(function (error) {
      console.log(error);
    });
    this.getArray(0, "arr_kk")
      .then(() => this.getArray(0, "arr_kec"))
      .then(() => this.getArray(0, "arr_kel"))
  }

  componentDidUpdate(prevState) {
    if (this.state.prov_index != this.state.selected_prov_index) {
      this.getArray(this.state.selected_prov_index, "arr_kk")
        .then(() => this.getArray(0, "arr_kec"))
        .then(() => this.getArray(0, "arr_kel"))
    }
    if (this.state.kk_index != this.state.selected_kk_index) {
      this.getArray(this.state.selected_kk_index, "arr_kec")
        .then(() => this.getArray(0, "arr_kel"))
    }
    if (this.state.kec_index != this.state.selected_kec_index) {
      this.getArray(this.state.selected_kec_index, "arr_kel")
    }
  }

  getArray = async (index, arrayName) => {
    var path = ""
    var params = ""
    var indexState = ""

    console.log("getArray index and array name")
    console.log(index)
    console.log(arrayName)

    switch (arrayName) {
      case "arr_kk":
        keyArrayName = "arr_prov"
        path = "get_cities"
        params = "province_id"
        indexState = "prov_index"
        break;
      case "arr_kec":
        keyArrayName = "arr_kk"
        path = "get_districts"
        params = "city_id"
        indexState = "kk_index"
        break;
      case "arr_kel":
        keyArrayName = "arr_kec"
        path = "get_postcodes"
        params = "district_id"
        indexState = "kec_index"
        break;
    }

    await axios.post('http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/api/v1a/helper/' + path, 
      {
        [params] : Object.keys(this.state[keyArrayName])[parseInt(index)]
      }, 
      {
      headers: {
        'Authorization': this.props.auth.token
      }
    })
    .then((response) => {
      this.setState({
        [arrayName]: response.data,
        [indexState]: index
      })
      console.log("entries data")
      console.log(params)
      console.log(this.state)
    })
    .catch((error) => {
      console.log(error);
    });
  }

  checkState = () => {
    console.log(this.props.auth.token)
  }

  pickerItem = (arr) => {
    if (arr != null) {
      var val_arr = Object.values(arr)

      return val_arr.map((data) => {
        return (
          <Picker.Item label={String(data)} value={data} style={{ fontSize: 12 }} />
        )
      })
      this.setState({
        prov: val_arr[0]
      })
      console.log(this.state)
    }
  }

  set_isVisible = () => {
    this.setState({ isVisible: !this.state.isVisible })
  }

  redirect = async () => {
    await this.setState({ isVisible: !this.state.isVisible })
    getInstallment(this.props.auth.token).then((res) => {
      this.props.setInstallment(res)
      this.props.navigation.navigate("BiayaPendidikanIndex")
    }).catch((err) => { console.log(err)})
  }

  renderLabel = (type, error_message) => {
    var label = ""
    var text_color = "#5A6978"

    switch (type) {
      case "name":
        label = "Nama Lengkap (sesuai KTP)"
        break;
      case "born_city":
        label = "Kota Kelahiran"
        break;
      case "date":
        label = "Tanggal Lahir"
        break;
      case "address":
        label = "Alamat Lengkap (sesuai KTP)"
        break;
      case "phone":
        label = "No. HP"
        break;
      case "email":
        label = "Email"
        break;
    }

    if (error_message) {
      text_color = "#CA020F"
    }

    return (
      <View style={{ flexDirection: "row", alignSelf: "stretch", alignItems: "center", justifyContent: "space-between" }}>
        <Text style={{ color: text_color, fontSize: 12, marginRight: "1.5%" }} >
        {label}
        </Text>
        { 
          error_message != null &&
          <View style={{ flexDirection: "row", flex: 1, alignItems: "center" }} >
            <Icon name="error" size={14} color="#CA020F" />
            <Text style={{ marginLeft: "1.5%", color: "#CA020F", fontSize: 10 }} >
            {error_message}
            </Text>
          </View>
        }
      </View>
    )
  }

  scrollToItem = (target) => {
    this.refs[target].measureLayout(
      findNodeHandle(this.refs.scroll),
      (x, y) => {
        this.refs.scroll.scrollTo({x: 0, y: y - (devHeight / 40) , animated: true});
      }
    );
  }

  render() {
    return(
      <Container style={styles.container}>
        <StatusBar translucent={true} hidden={true}/>
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ borderBottomWidth: 1, borderBottomColor: '#95251d', backgroundColor: '#fff' }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" color='#CC0611' style={{  }}/>
            </Button>
          </Left>
          <Body style={styles.headerBody}>
          <Title style={styles.textBody}>Pengajuan</Title>
          </Body>
          <Right style={styles.headerRight}>
            <Button transparent>
              <Icon name="tune" color='#CC0611' style={{  }}/>
            </Button>
          </Right>
        </Header>
          
            <ScrollView ref={"scroll"} contentContainerStyle={{ }} >
              <View style={{ width: "100%" }} >
                <View style={{ width: "90%", alignSelf: "center" }} >
                  <Image source={require("../../../assets/biayapend_clr_3x.png")} style={{ marginTop: "5%", alignSelf: "center" }} />
                  <Text style={{ color: "#969FAA", fontSize: 14, alignSelf: "flex-start", width: "100%", marginTop: 15 }} >
                  Data Penanggungjawab Pembayaran
                  </Text>
                  <View style={{  width: "100%", flexDirection: "row", justifyContent: "flex-end", marginTop: 15 }} >
                    <Text style={{ fontSize: 14, color: "#969FAA" }} >
                      Gunakan biodata saya
                    </Text>
                    <Switch value={this.state.switch_value} onValueChange={(value) => this.switchToggle(value)} />
                  </View>
                  <View style={{ marginTop: 15, alignItems: "flex-start", width: "100%" }} >

                    {this.renderLabel("name", this.state.name_error)}
                    <TextInput 
                    onChangeText={(value) => this.setState({name: value})}
                      value={this.state.name}
                      underlineColorAndroid={this.state.name_error != null ? "#CA020F" : "#5A6978"}
                      style={{ width: "100%" }}
                      ref={"nameTextInput"}
                      returnKeyType="next"
                      onSubmitEditing={() => {this.refs.birthplaceTextInput.focus()}}
                      blurOnSubmit={false} />

                    {this.renderLabel("born_city", this.state.born_city_error)}
                    <TextInput onChangeText={(value) => this.setState({born_city: value})}
                      value={this.state.born_city}
                      underlineColorAndroid={this.state.born_city_error != null ? "#CA020F" : "#5A6978"}
                      style={{ width: "100%" }}
                      ref={"birthplaceTextInput"}
                      returnKeyType="next"
                      onSubmitEditing={() => {this.refs.birthDatePicker.onPressDate()}}
                      blurOnSubmit={false} />

                    {this.renderLabel("date", this.state.date_error)}
                    <DatePicker style={{ width: "100%", marginVertical: 10 }}
                      customStyles={{dateInput:{borderColor: this.state.date_error != null ? "#CA020F" : "#5A6978", borderWidth: StyleSheet.hairlineWidth}}}
                      date={this.state.date}
                      mode="date"
                      format="DD-MM-YYYY"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      onDateChange={(date) => {this.setState({date:date}); this.refs.addressTextInput.focus();}}
                      ref={"birthDatePicker"}
                    />

                    {this.renderLabel("address", this.state.address_error)}
                    <TextInput onChangeText={(value) => this.setState({address: value})}
                      value={this.state.address}
                      underlineColorAndroid={this.state.address_error != null ? "#CA020F" : "#5A6978"}
                      style={{ width: "100%" }}
                      ref={"addressTextInput"}
                      returnKeyType="next"
                      onSubmitEditing={() => {this.refs.phoneNumberTextInput.focus()}}
                      blurOnSubmit={false}/>

                    <View style={{ width: "100%", marginVertical: 10 }} >
                      <Text style={{ color: "#5A6978", fontSize: 10 }} >
                      Provinsi
                      </Text>
                      <Picker
                        selectedValue={this.state.prov}
                        mode="dropdown"
                        onValueChange={(value, key) => this.setState({prov: value, selected_prov_index: key})}
                      >
                        {this.pickerItem(this.state.arr_prov)}
                      </Picker>

                      <Text style={{ color: "#5A6978", fontSize: 10 }} >
                      Kabupaten / Kota
                      </Text>
                      <Picker
                        selectedValue={this.state.kk}
                        onValueChange={(value, key) => this.setState({kk: value, selected_kk_index: key})}
                        mode="dropdown"
                      >
                        {this.pickerItem(this.state.arr_kk)}
                      </Picker>

                      <Text style={{ color: "#5A6978", fontSize: 10 }} >
                      Kecamatan
                      </Text>
                      <Picker
                        selectedValue={this.state.kec}
                        onValueChange={(value, key) => this.setState({kec: value, selected_kec_index: key})}
                        mode="dropdown"
                      >
                        {this.pickerItem(this.state.arr_kec)}
                      </Picker>

                      <Text style={{ color: "#5A6978", fontSize: 10 }} >
                      Kode Pos
                      </Text>
                      <Picker
                        selectedValue={this.state.kel}
                        onValueChange={(value, key) => this.setState({kel: value, selected_kel_index: key})}
                        mode="dropdown"
                      >
                        {this.pickerItem(this.state.arr_kel)}
                      </Picker>
                    </View>

                    {this.renderLabel("phone", this.state.phone_error)}
                    <TextInput onChangeText={(value) => this.setState({phone: value})}
                      value={this.state.phone}
                      underlineColorAndroid={this.state.phone_error != null ? "#CA020F" : "#5A6978"}
                      style={{ width: "100%" }}
                      ref={"phoneNumberTextInput"}
                      returnKeyType="next"
                      onSubmitEditing={() => {this.refs.emailTextInput.focus()}}
                      blurOnSubmit={false} />

                    {this.renderLabel("email", this.state.email_error)}
                    <TextInput onChangeText={(value) => this.setState({email: value})}
                      value={this.state.email}
                      underlineColorAndroid={this.state.email_error != null ? "#CA020F" : "#5A6978"}
                      style={{ width: "100%" }}
                      ref={"emailTextInput"} />

                  </View>

                  <View style={{ width: "100%", aspectRatio: 3/1, flexDirection: "row", marginTop: 5, justifyContent: "space-between" }}>
                    <View style={{ width: "30%", aspectRatio: 1/1, borderRadius: 25, borderWidth: 1, borderColor: this.state.image_self_error != null ? "#CA020F" : "#8492A6" }}>
                      <TouchableOpacity onPress={ () => this.uploadImage("image_self", "image_self_name") }>
                        {
                          this.state.image_self_error != null ? 
                            <View style={{ flexDirection: "row", backgroundColor: "#FFF", marginTop: -devHeight / 65, marginLeft: devWidth / 30, alignItems: "center", width: devWidth / 7, backgroundColor: "white", justifyContent: "center" }}>
                              <Text style={{ width: "65%", fontSize: 14, color: this.state.image_self_error != null ? "#CA020F" : "#5A6978", textAlign: "center", backgroundColor: "white" }}>
                                Foto
                              </Text>
                              <Icon name="error" size={14} color="#CA020F" style={{ backgroundColor: "#FFF", }} />
                            </View> 
                            : 
                            <View style={{ flexDirection: "row", backgroundColor: "#FFF", marginTop: -devHeight / 65, marginLeft: devWidth / 30, alignItems: "center", justifyContent: "center", width: devWidth / 9, backgroundColor: "white" }}>
                              <Text style={{ width: "95%", fontSize: 14, color: this.state.image_self_error != null ? "#CA020F" : "#5A6978", textAlign: "center", backgroundColor: "white" }}>
                                Foto
                              </Text>
                            </View>
                        }
                        <View style={{ alignItems: "center", justifyContent: 'center'}} >
                          <Image
                            source={ this.state.image_self == null ? require("../../../assets/black_avatar.png") : this.state.image_self }
                            style={{ width: "90%", height: "90%", resizeMode: "contain" }}
                          />
                        </View>
                      </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={ () => this.uploadImage("image_ktp", "image_ktp_name") } style={{ width: "65%", aspectRatio: 65/30, borderRadius: 25, borderWidth: 1, borderColor: this.state.image_ktp_error != null ? "#CA020F" : "#8492A6" }} >
                      {
                        this.state.image_ktp_error != null ?
                          <View style={{ flexDirection: "row", marginTop: -devHeight / 65, marginLeft: devWidth / 30, justifyContent: "center", alignItems: "center", backgroundColor: "white", width: devWidth / 7.5 }} >
                            <Text style={{ width: "65%", fontSize: 14, color: this.state.image_ktp_error != null ? "#CA020F" : "#5A6978", backgroundColor: "white", textAlign: "center" }}>
                              KTP
                            </Text>
                            <Icon name="error" size={14} color="#CA020F" style={{ backgroundColor: "#FFF", }} />
                          </View>
                          :
                          <View style={{ flexDirection: "row", marginTop: -devHeight / 65, marginLeft: devWidth / 30, alignItems: "center", justifyContent: "center", backgroundColor: "white", width: devWidth / 8 }} >
                            <Text style={{ width: "95%", fontSize: 14, color: this.state.image_ktp_error != null ? "#CA020F" : "#5A6978", textAlign: "center", backgroundColor: "white" }}>
                              KTP
                            </Text>
                          </View>
                      }
                      <View style={{ alignItems: "center"}} >
                        <Image
                          source={ this.state.image_ktp == null ? require("../../../assets/ktp.png") : this.state.image_ktp }
                          style={{ width: "95%", height: "90%", resizeMode: "contain" }}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>

                  <View style={{ width: "100%", aspectRatio: 4/1, marginTop: 10, borderRadius: 25, borderWidth: 1, borderColor: this.state.image_slip_error != null ? "#CA020F" : "#8492A6" }}>
                    {
                      this.state.image_slip_error != null ?
                        <View style={{ flexDirection: "row", marginTop: -devHeight / 65, marginLeft: devWidth / 35, alignItems: "center", justifyContent: "center", backgroundColor: "white", width: devWidth / 5 }} >
                          <Text style={{ width: "75%", fontSize: 14, color: "#CA020F" , textAlign: "center", backgroundColor: "white" }}>
                          Slip Gaji
                          </Text>
                          <Icon name="error" size={14} color="#CA020F" style={{ backgroundColor: "#FFF", }} />
                        </View>
                        :
                        <View style={{ flexDirection: "row", marginTop: -devHeight / 65, marginLeft: devWidth / 30, alignItems: "center", justifyContent: "center", backgroundColor: "white", width: devWidth / 6 }} >
                          <Text style={{  width: "95%", fontSize: 14, color: "#5A6978", paddingLeft: "0%", backgroundColor: "white", textAlign: "center" }}>
                          Slip Gaji
                          </Text>
                        </View>
                    }
                    <View style={{ flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", marginTop: 6 }} >
                      <TouchableOpacity onPress={ () => this.uploadImage("slip_1", "slip_1_name")} style={{alignItems: 'center', justifyContent: 'center',  width: "18%", aspectRatio: 1/1, borderRadius: 15, borderWidth: 1, borderColor: "#8492A6" }}>
                        { !this.state.slip_1 &&
                          <Text style={{ fontSize: 30 }}>+</Text>
                        }
                        { this.state.slip_1 &&
                          <Image
                            source={ this.state.slip_1 }
                            style={{ width: "100%", height: "100%", resizeMode: "contain" }}
                          />
                        }
                      </TouchableOpacity>
                      <TouchableOpacity onPress={ () => this.uploadImage("slip_2", "slip_2_name")} style={{alignItems: 'center', justifyContent: 'center',  width: "18%", aspectRatio: 1/1, borderRadius: 15, borderWidth: 1, borderColor: "#8492A6" }} disabled={ this.state.slip_1 == null}>
                        { !this.state.slip_2 && this.state.slip_1 &&
                          <Text style={{ fontSize: 30 }}>+</Text>
                        }
                        { this.state.slip_2 &&
                          <Image
                            source={ this.state.slip_2 }
                            style={{ width: "100%", height: "100%", resizeMode: "contain" }}
                          />
                        }
                      </TouchableOpacity>
                      <TouchableOpacity onPress={ () => this.uploadImage("slip_3", "slip_3_name")} style={{alignItems: 'center', justifyContent: 'center',  width: "18%", aspectRatio: 1/1, borderRadius: 15, borderWidth: 1, borderColor: "#8492A6" }} disabled={ this.state.slip_2 == null}>
                        { !this.state.slip_3 && this.state.slip_2 &&
                          <Text style={{ fontSize: 30 }}>+</Text>
                        }
                        { this.state.slip_3 &&
                          <Image
                            source={ this.state.slip_3 }
                            style={{ width: "100%", height: "100%", resizeMode: "contain" }}
                          />
                        }
                      </TouchableOpacity>
                      <TouchableOpacity onPress={ () => this.uploadImage("slip_4", "slip_4_name")} style={{alignItems: 'center', justifyContent: 'center',  width: "18%", aspectRatio: 1/1, borderRadius: 15, borderWidth: 1, borderColor: "#8492A6" }} disabled={ this.state.slip_3 == null}>
                        { !this.state.slip_4 && this.state.slip_3 &&
                          <Text style={{ fontSize: 30 }}>+</Text>
                        }
                        { this.state.slip_4 &&
                          <Image
                            source={ this.state.slip_4 }
                            style={{ width: "100%", height: "100%", resizeMode: "contain" }}
                          />
                        }
                      </TouchableOpacity>
                      <TouchableOpacity onPress={ () => this.uploadImage("slip_5", "slip_5_name")} style={{alignItems: 'center', justifyContent: 'center',  width: "18%", aspectRatio: 1/1, borderRadius: 15, borderWidth: 1, borderColor: "#8492A6" }} disabled={ this.state.slip_4 == null}>
                        { !this.state.slip_5 && this.state.slip_4 &&
                          <Text style={{ fontSize: 30 }}>+</Text>
                        }
                        { this.state.slip_5 &&
                          <Image
                            source={ this.state.slip_5 }
                            style={{ width: "100%", height: "100%", resizeMode: "contain" }}
                          />
                        }
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View style={{ marginVertical: "3%" }} >
                    <Button block rounded onPress={() => this.submitForm()} style={{ width: "80%", backgroundColor: "#CC0611", alignSelf: "center" }} onPress={() => this.formSubmit()}>
                      <Text uppercase={false} style={{ fontSize: 12, color: "#FFF", width: "100%", textAlign: "center" }} >
                      Ajukan Cicilan
                      </Text>
                    </Button>
                  </View>
                </View>
              </View>
            </ScrollView>
          
        <Dialog
          visible={this.state.isVisible}
          onTouchOutside={() => this.set_isVisible()}
          width={0.9}
          dialogStyle={{ backgroundColor: 'transparent' }}
        >
          <DialogContent>
            <View style={{ backgroundColor: 'transparent' }}>
              <ImageBackground source={ thanksA } style={{ zIndex: 1, justifyContent: 'flex-end', position: 'absolute', alignItems: 'flex-end', width: 150, height: 150, alignSelf: 'center' }} imageStyle={{ borderWidth: 4, borderColor: 'white', borderRadius: 75 }}>
                <Image source={ thanks } style={{ tintColor: '#FEB100', backgroundColor: 'white', borderRadius: 24, borderWidth: 4, borderColor: 'white' }}/>
              </ImageBackground>
              <View style={{ borderTopLeftRadius:12, borderTopRightRadius:12, marginTop: 80, height:80, backgroundColor: 'white', alignItems: 'center' }}>
              </View>
            </View>
            <View style={{ backgroundColor: 'white', borderBottomLeftRadius:12, borderBottomRightRadius:12 }}>
              <Text style={{ color: '#FEB100', fontWeight: '700', fontSize: 20, textAlign: 'center' }}>Terima kasih</Text>
              <View style={{ flexDirection: 'row', padding: 10, flexWrap: 'wrap', justifyContent: 'center' }}>
                <Text style={{ fontWeight: '400', fontSize: 17, textAlign: 'center'}}>Data pengajuan Anda telah kami terima</Text>
                <Text style={{ fontWeight: '400', fontSize: 17, textAlign: 'center'}}>
                  Kami akan melakukan verifikasi
                    <Text style={{ fontWeight: '700', fontSize: 17, textAlign: 'center'}}> 2x24 </Text>
                  jam ke depan </Text>
              </View>
              <Text style={{ fontSize: 13, textAlign: 'center' }}>Kami akan mengirimkan notifikasi setelah veriikasi selesai</Text>
              <Button transparent onPress={() => this.redirect()} style={{ borderTopWidth:0.5, width: '100%', justifyContent: 'center', marginTop: 10 }}>
                <Text style={{ color: 'black', fontWeight: '700', fontSize: 19, marginVertical: 10 }}>Lanjut</Text>
              </Button>
            </View>
          </DialogContent>
        </Dialog>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const { appform, auth, profile } = state
  return { appform, auth, profile }
};

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({
    setInstallment
  }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(FormPenanggungJawab);