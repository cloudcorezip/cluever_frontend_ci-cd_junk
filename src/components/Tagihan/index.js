import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Text, Content, Card, CardItem, Footer, List, ListItem } from "native-base";
import { StatusBar, Image, View, ImageBackground, LayoutAnimation, ScrollView, TouchableOpacity, Platform, UIManager, Dimensions } from "react-native";
import Slideshow from 'react-native-image-slider-show'
import { Icon } from 'react-native-elements'
import styles from './styles'
import Collapsible from 'react-native-collapsible'

var img_tagihan = require("../../../assets/tagihananda_clr_3x.png")
var img_biayapend = require("../../../assets/biayapend_clr_2x.png")
var img_marketplace = require("../../../assets/mrktplc_clr_2x.png")
var img_progpend = require("../../../assets/progpend_clr_2x.png")
var img_banner = require("../../../assets/banner_tagihan.png")

var screen_height = Dimensions.get('window').height
var flex_percent = 200/screen_height

export default class BiayaPendidikanPayment extends Component {
  constructor() {
    super()

    this.state = {
      banner: img_banner,
      total_due: 10000000,
      biaya_pendidikan: 5000000,
      program_pendidikan: 2500000,
      marketplace: 2500000,
      bp_amount: [1500000,2500000],
      bp_name: ["Patrick Arevalo", "Ananda Kemal"],
      bp_pic: ["https://pbs.twimg.com/profile_images/433975350423732224/lLQ5IxLv_400x400.jpeg","https://i.imgur.com/MQHYB.jpg"],
      bp_institution: ["Edulab, Bandung + 1 institusi lain","Edulab, Medan+ 2 institusi lain"],
      bp_date: ["24-09-1997", "22-12-2019" ],
      collapsed: [true,true]

    }
  }

  numberWithDots(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  setDate(date){
    var parts = date.split('-');
    var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    parts[1] = months[parseInt(parts[1]) - 1 ]
    return parts
  };

  toggle_expand_biayapendidikan = () => {
    this.setState({
      collapsed: [!this.state.collapsed[0],!this.state.collapsed[1]]
    });
  }

 render () {
   return (
     <Container style={{ backgroundColor: '#fff' }}>
       <StatusBar translucent={true} hidden={true}/>
       <Header
         noShadow
         iosBarStyle={"dark-content"}
         style={{ backgroundColor: '#fff' }}>
         <Left style={styles.headerLeft}>
           <Button transparent onPress={() => this.props.navigation.goBack()}>
             <Icon name="arrow-back" color="#CC0611" />
           </Button>
         </Left>
         <Body style={styles.headerBody}>
           <Title style={styles.textBody}>Tagihan Anda</Title>
         </Body>
         <Right style={styles.headerRight}>
           <Button transparent>
             <Icon name="tune" color="#CC0611"/>
           </Button>
         </Right>
       </Header>

       <Content
        style={{ backgroundColor: '#FFF' }}
        contentContainerStyle={{ flexDirection: 'column', justifyContent: 'space-between' }}>
        <View style={{ flex: flex_percent }}>
          <ImageBackground source={ this.state.banner } style={{ height: 200, flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
            <Image
              source={ img_tagihan }
              style={{ tintColor: '#fff' }}
              align='center'/>
            <View style={{ paddingHorizontal: 20 }}>
              <Text style={{ color: '#fff', fontSize: 15.5, fontWeight: "400" }}>Nikmati kemudahan pembayaran</Text>
              <Text style={{ color: '#fff', fontSize: 23, fontWeight: "700", fontStyle: 'italic', textAlign: 'right' }}>Tanpa Bunga</Text>
            </View>
          </ImageBackground>
        </View>

        <View style={{ backgroundColor: '#969FAA' }}>
          <View style={{ backgroundColor: "#FFF", paddingVertical: 10 }}>
            <View style={{ flexDirection: 'row', backgroundColor: "#FFF", marginLeft: 17, marginRight: 10 }}>
              <Text style={{ flex: 0.5, color: "#969FAA", fontSize: 19, fontWeight: "400" }}>Total tagihan Anda</Text>
              <Text style={{ flex: 0.5, color: "#5A6978", fontSize: 19, fontWeight: "400", textAlign: 'right' }}>{"Rp " + this.numberWithDots(this.state.total_due)}</Text>
            </View>
          </View>

          <View style={{ backgroundColor: "#FFF", marginTop: 1.5, paddingVertical: 10 }}>
            <Text style={{ color: "#969FAA", fontSize: 14, fontWeight: "400", backgroundColor: "#FFF", marginLeft: 17, marginRight: 10 }}>Daftar Tagihan</Text>
          </View>

          <View style={{ backgroundColor: "#F5F5F5" }}>
            <TouchableOpacity style={{ marginHorizontal: 10, paddingVertical: 10, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: "#F5F5F5" }} onPress={  this.toggle_expand_biayapendidikan }>
              <Image
                source={ img_biayapend }
                style={{ margin: 7 }}/>
              <Left>
                <Text style={{ color: "#5A6978", fontSize: 19, fontWeight: "700" }}>Biaya Pendidikan</Text>
              </Left>
              <View style={{ selfAlign: 'right' }}>
                <Text style={{ textAlign: 'right', color: "#CB0714", fontSize: 18, fontWeight: "700" }}>{"Rp " + this.numberWithDots(this.state.biaya_pendidikan)}</Text>
                <Text style={{ textAlign: 'right', color: "#969FAA", fontSize: 14, fontWeight: "400", textDecorationLine: 'underline' }}>Rincian</Text>
              </View>
            </TouchableOpacity>
          </View>

          <Collapsible collapsed={this.state.collapsed[0]} style={{ backgroundColor: '#F5F5F5' }}>
            <View style={{ backgroundColor: 'white', marginVertical: 5, paddingVertical: 5 }}>
              <View style={{ flexDirection: 'row', alignItems: 'stretch', backgroundColor: 'white', marginHorizontal: 10  }}>
                <Image
                  style={{ width: 32, height: 32, borderRadius: 16, margin: 7}}
                  source={{ uri: this.state.bp_pic[0]}}
                />
                <View style={{ flexDirection: 'column', flex: 1 }}>
                  <Text style={{ color: "#5A6978", fontSize: 16, fontWeight: "700" }}>{ this.state.bp_name[0] }</Text>
                  <Text style={{ color: "#969FAA", fontSize: 13, fontWeight: "400" }}>{ this.state.bp_institution[0] }</Text>
                  <Text style={{ color: "#969FAA", fontSize: 12, fontWeight: "400", fontStyle: 'italic' }}>{ "Diajukan pada " + this.setDate(this.state.bp_date[0]) }</Text>
                </View>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={{ textAlign: 'right', color: "#CB0714", fontSize: 16, fontWeight: "700" }}>{"Rp " + this.numberWithDots(this.state.bp_amount[0])}</Text>
                  <Text style={{ textAlign: 'right', color: "#969FAA", fontSize: 14, fontWeight: "400", textDecorationLine: 'underline' }}>Rincian</Text>
                </View>
              </View>
            </View>
          </Collapsible>

          <Collapsible collapsed={this.state.collapsed[1]} style={{ backgroundColor: '#F5F5F5' }}>
            <View style={{ backgroundColor: 'white', marginTop: 5, marginBottom: 10, paddingVertical: 5 }}>
              <View style={{ flexDirection: 'row', alignItems: 'stretch', backgroundColor: 'white', marginHorizontal: 10 }}>
                <Image
                  style={{ width: 32, height: 32, borderRadius: 16, margin: 7 }}
                  source={{ uri: this.state.bp_pic[1]}}
                />
                <View style={{ flexDirection: 'column', flex: 1 }}>
                  <Text style={{ color: "#5A6978", fontSize: 16, fontWeight: "700" }}>{ this.state.bp_name[1] }</Text>
                  <Text style={{ color: "#969FAA", fontSize: 13, fontWeight: "400" }}>{ this.state.bp_institution[1] }</Text>
                  <Text style={{ color: "#969FAA", fontSize: 12, fontWeight: "400", fontStyle: 'italic' }}>{ "Diajukan pada " + this.setDate(this.state.bp_date[1]) }</Text>
                </View>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={{ textAlign: 'right', color: "#CB0714", fontSize: 16, fontWeight: "700" }}>{"Rp " + this.numberWithDots(this.state.bp_amount[1])}</Text>
                  <Text style={{ textAlign: 'right', color: "#969FAA", fontSize: 14, fontWeight: "400", textDecorationLine: 'underline' }}>Rincian</Text>
                </View>
              </View>
            </View>
          </Collapsible>

          <View style={{  backgroundColor: 'white' }}>
            <View style={{ marginHorizontal: 10, paddingVertical: 10, flexDirection: 'row', justifyContent: 'center', backgroundColor: "#FFF" }} >
              <Image
                source={ img_progpend }
                style={{ margin: 7  }}/>
              <Left>
               <Text style={{ color: "#5A6978", fontSize: 19, fontWeight: "700" }}>Program Pendidikan</Text>
              </Left>
              <View style={{ selfAlign: 'right' }}>
                <Text style={{ textAlign: 'right', color: "#CB0714", fontSize: 18, fontWeight: "700" }}>{"Rp " + this.numberWithDots(this.state.marketplace)}</Text>
                <Text style={{ textAlign: 'right', color: "#969FAA", fontSize: 14, fontWeight: "400", textDecorationLine: 'underline' }}>Rincian</Text>
              </View>
            </View>
          </View>

          <View style={{ backgroundColor: '#F5F5F5' }}>
            <View style={{ marginHorizontal: 10, paddingVertical: 10, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: "#F5F5F5" }} >
              <Image
                source={ img_marketplace }
                style={{  margin: 7  }}/>
              <Left>
                <Text style={{ color: "#5A6978", fontSize: 19, fontWeight: "700" }}>Marketplace</Text>
              </Left>
              <View style={{ selfAlign: 'right' }}>
                <Text style={{ textAlign: 'right', color: "#CB0714", fontSize: 18, fontWeight: "700" }}>{"Rp " + this.numberWithDots(this.state.program_pendidikan)}</Text>
                <Text style={{ textAlign: 'right', color: "#969FAA", fontSize: 14, fontWeight: "400", textDecorationLine: 'underline' }}>Rincian</Text>
              </View>
            </View>
          </View>

        </View>
       </Content>
     </Container>
   );
 }

}
