import masterStyles, { color, devHeight } from '../styles';

export default {
  container: {
    fontFamily: 'Montserrat',
    flex: 1,
    backgroundColor: color.white
  },
  headerLeft: {
    flex: 0.3
  },
  headerBody: {
    flex: 0.4
  },
  textBody:{
    alignSelf: "center",
    color: color.white
  },
  headerRight: {
    flex: 0.3
  },
  fullWidthAbsolute: {
    position: 'absolute',
    top: 0,
    left: 8,
    right: 8,
    bottom: 0
  },
  transparent: {
    opacity: 0.6
  },
  darkRed: {
    // backgroundColor: '#95251d'
    backgroundColor: '#000'
  },
  rounded: {
    borderRadius: 8
  },
  padding: {
    padding: 8
  },
  cardTitleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  cardTitle: {
    fontSize: 20,
    fontWeight: 'bold', 
    color: '#fff',
    marginLeft: 12
  },
  cardImage: {
    height: 120,
    width: null,
    flex: 1
  },
  menuItemCard: {
    ...masterStyles.radius,
    height: devHeight / 4
  },
  menuCol: {
    marginHorizontal: 8,
    marginVertical: 4
  },
  menuItemContainer: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: color.grey,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold'
  },
  menuIcon: {
    height: 48,
    width: 48,
    marginBottom: 16,
  }
};
