import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Text, Content, Card, CardItem, Footer, FooterTab, Thumbnail, Grid, Col
} from "native-base";
import { ScrollView, RefreshControl, StatusBar, Image, View, StyleSheet, ImageBackground } from "react-native";
import { Icon, SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';
import Slideshow from 'react-native-image-slider-show'
import axios from 'axios'
import { bindActionCreators } from "redux";
import { TAGIHAN_USER_URI } from "../../constants/apis"
import { isTokenValid } from '../../utils/AuthUtils.js'
import { getProfile } from '../../utils/ProfileUtils.js'
import { getInstallment } from '../../utils/InstallmentUtils.js'
import { getTagihan } from '../../utils/TagihanUtils.js'
import { userLogoutRequest, setProfile, setInstallment, setTagihan } from "../../actions";
import masterStyles, { devWidth, devHeight, color } from '../styles'
import styles from "./styles";



const biayaPendidikanButtonImg = require("../../../assets/biaya_pend_button_img_821x183.png");
const biayaPendidikanButtonIcon = require("../../../assets/biayapend_clr_3x.png");
const programPendidikanButtonImg = require("../../../assets/prog_pend_button_img_821x183.png");
const programPendidikanButtonIcon = require("../../../assets/progpend_clr_3x.png");
const marketplaceButtonImg = require("../../../assets/mrktplc_clr_3x.png");;
const shuttleButtonImg = require("../../../assets/cart_clr_3x.png");;
const banner = require("../../../assets/home_carousel_1074x393.png");

class Home extends Component {

  constructor(props) {
    super(props);

    console.log("Props")
    console.log(props)

    this.state = {
      position: 0,
      // TODO: ADD Images from assets for carousel
      // dataSource: [
      //   { url: 'https://cdn.pixabay.com/photo/2017/09/25/17/38/chart-2785979_960_720.jpg' },
      //   { url: 'https://live.staticflickr.com/3810/9606789399_02a0579525_n.jpg' },
      //   { url: 'https://cdn.pixabay.com/photo/2017/01/12/16/19/kit-1975078_960_720.png' }
      // ],
      tagihan: '',
      refreshing: false
    }
  }

  // componentWillMount() {
  //   this.setState({
  //     interval: setInterval(() => {
  //       this.setState({
  //         position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
  //       });
  //     }, 2000)
  //   });
  // }

  async componentDidMount() {
    const doAsync = await isTokenValid(this.props.auth.token)
    // const valid = await doAsync;

    if (doAsync != false) {
      getProfile(this.props.auth.token).then((res) => {
        this.props.setProfile(res.data)
      })

      getInstallment(this.props.auth.token).then((res) => {
        // console.log("installmentAsycn")
        this.props.setInstallment(res)
      })

      getTagihan(this.props.auth.token).then((res) => {
        this.setState({ tagihan: res.data.unpaid / 10 })
        this.props.setTagihan(res.data)
      })

    } else {
      this.props.userLogoutRequest();
      this.props.navigation.navigate("Login")
    }
  }

  //TODO: ADD Image from assets for carousel
  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  checkToken = () => {
    const config = {
      headers: {
        Authorization: this.props.auth.token,
      }
    }
    axios.post("http://192.168.1.11:3000/check_token", {}, config)
      .then((response) => {
        console.log(response)
      })
      .catch()
  }

  numberWithDots(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    axios.post(TAGIHAN_USER_URI, null, {
      headers: {
        'Authorization': this.props.auth.token
      }
    }).then((response) => {
      this.setState({ tagihan: response.data.unpaid / 10, refreshing: false })
    }).catch((err) => {
      console.log(err)
    })

  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar
          hidden={true}
        />
        <View>
          <Image
            style={StyleSheet.absoluteFill}
            source={{ uri: 'https://img.freepik.com/free-photo/card-soft-template-paper-report_1258-167.jpg?size=626&ext=jpg' }}
          />
          <Header
            iosBarStyle={"dark-content"}
            androidStatusBarColor={"#CC0611"}
            style={{
              backgroundColor: 'transparent',
              borderBottomColor: 'transparent',
              height: devHeight / 12
            }}>
            <Left style={styles.headerLeft}>
              <Button transparent onPress={() => alert('Fitur scan QR code belum tersedia')}>
                <Icon name="camera" color="#FFF" />
              </Button>
            </Left>
            <Body style={[styles.headerBody, { justifyContent: 'center', alignItems: 'center' }]}>
              <Image source={require('../../../assets/logoHeader.png')} />
            </Body>
            <Right style={styles.headerRight}>
              <Button transparent onPress={() => this.props.navigation.navigate("Notification")} >
                <Icon name="notifications" color="#FFF" />
              </Button>
            </Right>
          </Header>
        </View>
        <ScrollView refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh} />
        }>
          <Content>
            <View>
              {/* TODO: Slideshow images from assets */}
              {/* <Slideshow
              height={160}
              dataSource={this.state.dataSource}
              position={this.state.position}
              onPositionChanged={position => this.setState({ position })}
              arrowSize={0}
            /> */}
              <ImageBackground
                source={banner}
                style={{
                  height: 150,
                  flexDirection: "column",
                  justifyContent: "flex-end",
                  alignItems: "center"
                }}
              />
            </View>
            {this.state.tagihan != "" &&
              <View style={{ backgroundColor: color.red }} >
                <Card transparent>
                  <CardItem style={{ flexDirection: "column", alignItems: "center", backgroundColor: color.red }} onPress={() => this.props.onPress("Tagihan")}>
                    <View>
                      <Text style={{
                        fontSize: 14,
                        color: color.white,
                        fontStyle: "italic",
                        fontWeight: 'bold'
                      }} >
                        Total Tagihan Bulan Ini
                          </Text>
                    </View>
                    <View style={{ marginTop: 8 }} >
                      <Text style={{
                        fontFamily: 'Montserrat',
                        fontSize: 32,
                        fontWeight: 'bold',
                        color: color.white
                      }} >
                        Rp. {this.numberWithDots(this.state.tagihan)}
                      </Text>
                    </View>
                  </CardItem>
                </Card>
              </View>
            }
            <View style={{ alignItems: "center", marginTop: 15 }} >
              <Grid>
                <Col style={styles.menuCol}>
                  <Card
                    data={this.props.installment.items}
                    style={styles.menuItemCard}>
                    <CardItem
                      cardBody
                      style={styles.menuItemContainer}
                      button onPress={() => this.props.onPress("BiayaPendidikanIndex")}
                    >
                      <Thumbnail square source={biayaPendidikanButtonIcon} style={styles.menuIcon} />
                      <Text style={{ fontWeight: 'bold' }}>Biaya Pendidikan</Text>
                    </CardItem>
                  </Card>
                </Col>
                <Col style={styles.menuCol}>
                  <Card style={[masterStyles.radius, { height: devHeight / 4 }]}>
                    <CardItem
                      cardBody
                      style={styles.menuItemContainer}
                      button onPress={() => this.props.onPress("UnderDevelopment")}
                    >
                      <Thumbnail square source={programPendidikanButtonIcon} style={styles.menuIcon} />
                      <Text style={{ fontWeight: 'bold' }}>Program Pendidikan</Text>
                    </CardItem>
                  </Card>
                </Col>
              </Grid>
              <Grid>
                <Col style={styles.menuCol}>
                  <Card style={styles.menuItemCard}>
                    <CardItem
                      cardBody
                      style={styles.menuItemContainer}
                      button onPress={() => this.props.onPress("UnderDevelopment")}
                    >
                      <Thumbnail square source={marketplaceButtonImg} style={styles.menuIcon} />
                      <Text style={{ fontWeight: 'bold' }}>Marketplace</Text>
                    </CardItem>
                  </Card>
                </Col>
                <Col style={styles.menuCol}>
                  <Card style={styles.menuItemCard}>
                    <CardItem
                      cardBody
                      style={styles.menuItemContainer}
                      button onPress={() => console.log(this.props)}
                    >
                      <Thumbnail square source={shuttleButtonImg} style={styles.menuIcon} />
                      <Text style={{ fontWeight: 'bold' }}>Antar Jemput</Text>
                    </CardItem>
                  </Card>
                </Col>
              </Grid>
            </View>
          </Content>
        </ScrollView>
        <Footer style={{ backgroundColor: 'transparent' }} >
          <FooterTab style={{ backgroundColor: 'transparent' }} >
            <Button>
              <View style={{}} >
                <Icon name="home" color="#CA020F" style={{}} />
              </View>
            </Button>
            <Button onPress={() => this.props.onPress("RiwayatTransaksi")}>
              <Icon name="attach-money" color="gray" />
            </Button>
            <Button onPress={() => this.props.onPress("Feeds")}>
              <Icon type="font-awesome" name="newspaper-o" color="gray" />
            </Button>
            <Button onPress={() => this.props.onPress("ProfileUser")}>
              <Icon name="person" color="gray" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}


const mapStateToProps = (state) => {
  const { auth, installment, profile, tagihan } = state
  return { auth, installment, profile, tagihan }
};

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({
    userLogoutRequest, setProfile, setInstallment, setTagihan
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Home)
