import React, { Component } from "react";
import axios from "axios";
import { Header, Left, Container, Button, Body, Title, Right, Text, Content, Footer, FooterTab } from "native-base";
import { StatusBar, Image, View, Switch, TextInput, StyleSheet, ScrollView, TouchableOpacity, Dimensions } from "react-native";
import { Icon, Divider } from "react-native-elements";
import DatePicker from 'react-native-datepicker';
import styles from './styles'
import { connect } from 'react-redux'
import { FormSubmitApplication } from "../../actions"
import { bindActionCreators } from 'redux';
import ImagePicker from 'react-native-image-picker';
import { findNodeHandle } from 'react-native'
import TextInputState from 'react-native/lib/TextInputState'
import * as Validation from "../../constants/Validation"

var devHeight = Dimensions.get('window').height;
var devWidth = Dimensions.get('window').width;

class FormPengajuan extends Component {

  constructor(props) {
      super(props);

      this.state = {
        switch_value: false,
        name: "",
        born_city: "",
        date: "15-07-2019",
        address: "",
        phone: "",
        email: "",
        name_error: null,
        born_city_error: null,
        date_error: null,
        address_error: null,
        phone_error: null,
        email_error: null,
        buttonDisabled: true,
        image_self: null,
        image_ktp: null,
        image_self_name: null,
        image_ktp_name: null,
        image_self_error: null,
        image_ktp_error: null
      }
    }

  switchToggle(value) {
    if (value) {
      this.setState({
        switch_value: value,
        name: this.props.profile.data.user.fullname,
        born_city: "Bandung",
        date: "27-08-1980",
        address: "Jl. Kalimantan No. 7 Sumur Bandung",
        phone: this.props.profile.data.user.phone_no,
        email: this.props.profile.data.user.email
      })
    } else {
      this.setState({
        switch_value: value,
        name: "",
        born_city: "",
        date: "15-07-2019",
        address: "",
        phone: "",
        email: ""
      })
    }
  }

  uploadImage = (type, name) => {
    const options = {
      customButtons: [{ name: 'delete', title: 'Delete Photo...' }],
    };
    ImagePicker.showImagePicker(options , (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        this.setState({
          [type]: null,
          [name]: null
        });
        this.shiftState(type)
      } else {
        const source = { uri: response.uri };
        const file_name = response.fileName
        this.setState({
          [type]: source,
          [name]: file_name,
        });
      }
    });
  }

  shiftState = (type) => {
    if (this.state.slip_1 == null && this.state.slip_2 == null) {

    } else {

      stateArr = ["", "", "", "", ""]
      stateArr[0] = this.state.slip_1
      stateArr[1] = this.state.slip_2
      stateArr[2] = this.state.slip_3
      stateArr[3] = this.state.slip_4
      stateArr[4] = this.state.slip_5

      stateArrName = ["", "", "", "", ""]
      stateArrName[0] = this.state.slip_1_name
      stateArrName[1] = this.state.slip_2_name
      stateArrName[2] = this.state.slip_3_name
      stateArrName[3] = this.state.slip_4_name
      stateArrName[4] = this.state.slip_5_name
      var i;
      for (i = 0; i < 4; i++) {
        if (stateArr[i] == null) {
          stateArr[i] = stateArr[i+1]
          stateArr[i+1] = null
          stateArrName[i] = stateArrName[i+1]
          stateArrName[i+1] = null
        }
      }
      this.setState({
        slip_1: stateArr[0],
        slip_2: stateArr[1],
        slip_3: stateArr[2],
        slip_4: stateArr[3],
        slip_5: stateArr[4],
        slip_1_name: stateArrName[0],
        slip_2_name: stateArrName[1],
        slip_3_name: stateArrName[2],
        slip_4_name: stateArrName[3],
        slip_5_name: stateArrName[4],
      })
    }
  }

  submitForm = () => {
    var name_error = null
    var born_city_error = null
    var date_error = null
    var address_error = null
    var phone_error = null
    var email_error = null
    var image_self_error = null
    var image_ktp_error = null

    if (this.state.name == "")
      name_error = "Nama harus diisi"
    else if (!VALID_FULLNAME_REGEX.test(this.state.name))
      name_error = "Nama yang dimasukkan salah. Pastikan nama terdiri dari huruf 5-30 karater."
    if (this.state.born_city == "")
      born_city_error = "Tempat lahir harus diisi"
    if (this.state.date == "")
      date_error = "Tanggal lahir harus diisi"
    if (this.state.address == "")
      address_error = "Alamat harus diisi"
    if (this.state.phone == "")
      phone_error = "Nomor telepon harus diisi"
    else if (!VALID_PHONENO_REGEX.test(this.state.phone))
      phone_error = "Nomor telepon yang dimasukkan salah. Pastikan nomor telepon terdiri dari angka 9-11 karakter"
    if (this.state.email == "")
      email_error = "Email harus diisi"
    else if (!VALID_EMAIL_REGEX.test(this.state.email))
      email_error = "Email yang dimasukkan salah. Pastikan format email benar"
    if (!this.state.image_self)
      image_self_error = "Foto diperlukan"
    if (!this.state.image_ktp)
      image_ktp_error = "Foto diperlukan"

    this.setState({
      name_error: name_error,
      born_city_error: born_city_error,
      date_error: date_error,
      address_error: address_error,
      phone_error: phone_error,
      email_error: email_error,
      image_self_error: image_self_error,
      image_ktp_error: image_ktp_error,
    })

    if (name_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.nameTextInput))
      return
    }
    if (born_city_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.birthplaceTextInput))
      return
    }
    if (date_error) {
      this.refs.birthDatePicker.onPressDate()
      return
    }
    if (address_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.addressTextInput))
      return
    }
    if (phone_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.phoneNumberTextInput))
      return
    }
    if (email_error) {
      TextInputState.focusTextInput(findNodeHandle(this.refs.emailTextInput))
      return
    }

    if (!name_error && !born_city_error && !date_error && !address_error && !phone_error && !email_error && !image_self_error
      //  && !image_ktp_error 
      ) {
      this.props.FormSubmitApplication(this.formBuilder())
      this.props.navigation.navigate("FormPenanggungJawab")
    }
  }

  formBuilder(){
    return {
      fullname: this.state.name,
      birthplace: this.state.born_city,
      dob: this.state.date,
      address: this.state.address,
      postcode_id: "1",
      program_id: "1",
      phone_no: this.state.phone,
      email: this.state.email,
    }
  }

  renderLabel = (type, error_message) => {
    var label = ""
    var text_color = "#5A6978"

    switch (type) {
      case "name":
        label = "Nama Lengkap (sesuai KTP)"
        break;
      case "born_city":
        label = "Kota Kelahiran"
        break;
      case "date":
        label = "Tanggal Lahir"
        break;
      case "address":
        label = "Alamat Lengkap (sesuai KTP)"
        break;
      case "phone":
        label = "No. HP"
        break;
      case "email":
        label = "Email"
        break;
    }

    if (error_message) {
      text_color = "#CA020F"
    }

    return (
      <View style={{ flexDirection: "row", alignSelf: "stretch", alignItems: "center", justifyContent: "space-between" }}>
        <Text style={{ color: text_color, fontSize: 12, marginRight: "1.5%" }} >
        {label}
        </Text>
        { 
          error_message != null &&
          <View style={{ flexDirection: "row", flex: 1, alignItems: "center" }} >
            <Icon name="error" size={14} color="#CA020F" />
            <Text style={{ marginLeft: "1.5%", color: "#CA020F", fontSize: 10 }} >
            {error_message}
            </Text>
          </View>
        }
      </View>
    )
  }

  render() {
    return(
      <Container style={styles.container}>
        <StatusBar translucent={true} hidden={true}/>
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ borderBottomWidth: 1, borderBottomColor: '#95251d', backgroundColor: '#fff' }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" color='#CC0611' style={{  }}/>
            </Button>
          </Left>
          <Body style={styles.headerBody}>
          <Title style={styles.textBody}>Pengajuan</Title>
          </Body>
          <Right style={styles.headerRight}>
          </Right>
        </Header>
        <Content contentContainerStyle={{ alignItems: 'center', width: "90%", alignSelf: "center" }} >
          <ScrollView contentContainerStyle={{ }} >
            <Image source={require("../../../assets/biayapend_clr_3x.png")} style={{ marginTop: "5%", alignSelf: "center" }} />
            <Text style={{ color: "#969FAA", fontSize: 14, alignSelf: "flex-start", width: "100%", marginTop: 15 }} >
            Data Pengajuan
            </Text>
            <View style={{  width: "100%", flexDirection: "row", justifyContent: "flex-end", marginTop: 15 }} >
              <Text style={{ fontSize: 14, color: "#969FAA" }} >
                Gunakan biodata saya
              </Text>
              <Switch value={this.state.switch_value} onValueChange={(value) => this.switchToggle(value)} />
            </View>
            <View style={{ marginTop: 15, alignItems: "flex-start", width: "100%" }} >

              {this.renderLabel("name", this.state.name_error)}
              <TextInput 
              onChangeText={(value) => this.setState({name: value})}
                value={this.state.name}
                underlineColorAndroid={this.state.name_error != null ? "#CA020F" : "#5A6978"}
                style={{ width: "100%" }}
                ref={"nameTextInput"}
                returnKeyType="next"
                onSubmitEditing={() => {this.birthplaceTextInput.focus()}}
                blurOnSubmit={false} />

              {this.renderLabel("born_city", this.state.born_city_error)}
              <TextInput onChangeText={(value) => this.setState({born_city: value})}
                value={this.state.born_city}
                underlineColorAndroid={this.state.born_city_error != null ? "#CA020F" : "#5A6978"}
                style={{ width: "100%" }}
                ref={"birthplaceTextInput"}
                returnKeyType="next"
                onSubmitEditing={() => {this.birthDatePicker.onPressDate()}}
                blurOnSubmit={false} />

              {this.renderLabel("date", this.state.date_error)}
              <DatePicker style={{ width: "100%", marginVertical: 10 }}
                customStyles={{dateInput:{borderColor: this.state.date_error != null ? "#CA020F" : "#5A6978", borderWidth: StyleSheet.hairlineWidth}}}
                date={this.state.date}
                mode="date"
                format="DD-MM-YYYY"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                onDateChange={(date) => {this.setState({date:date}); this.addressTextInput.focus();}}
                ref={"birthDatePicker"}
              />

              {this.renderLabel("address", this.state.address_error)}
              <TextInput onChangeText={(value) => this.setState({address: value})}
                value={this.state.address}
                underlineColorAndroid={this.state.address_error != null ? "#CA020F" : "#5A6978"}
                style={{ width: "100%" }}
                ref={"addressTextInput"}
                returnKeyType="next"
                onSubmitEditing={() => {this.phoneNumberTextInput.focus()}}
                blurOnSubmit={false}/>

              {this.renderLabel("phone", this.state.phone_error)}
              <TextInput onChangeText={(value) => this.setState({phone: value})}
                value={this.state.phone}
                underlineColorAndroid={this.state.phone_error != null ? "#CA020F" : "#5A6978"}
                style={{ width: "100%" }}
                ref={"phoneNumberTextInput"}
                returnKeyType="next"
                onSubmitEditing={() => {this.emailTextInput.focus()}}
                blurOnSubmit={false} />

              {this.renderLabel("email", this.state.email_error)}
              <TextInput onChangeText={(value) => this.setState({email: value})}
                value={this.state.email}
                underlineColorAndroid={this.state.email_error != null ? "#CA020F" : "#5A6978"}
                style={{ width: "100%" }}
                ref={"emailTextInput"} />
            </View>

            <View style={{ width: "100%", aspectRatio: 3/1, flexDirection: "row", marginTop: 5, justifyContent: "space-between" }}>
              <View style={{ width: "30%", aspectRatio: 1/1, borderRadius: 25, borderWidth: 1, borderColor: this.state.image_self_error != null ? "#CA020F" : "#8492A6" }}>
                <TouchableOpacity onPress={ () => this.uploadImage("image_self", "image_self_name") }>
                  {
                    this.state.image_self_error != null ? 
                      <View style={{ flexDirection: "row", backgroundColor: "#FFF", marginTop: -devHeight / 65, marginLeft: devWidth / 30, alignItems: "center", width: devWidth / 7, backgroundColor: "white", justifyContent: "center" }}>
                        <Text style={{ width: "65%", fontSize: 14, color: this.state.image_self_error != null ? "#CA020F" : "#5A6978", textAlign: "center", backgroundColor: "white" }}>
                          Foto
                        </Text>
                        <Icon name="error" size={14} color="#CA020F" style={{ backgroundColor: "#FFF", }} />
                      </View> 
                      : 
                      <View style={{ flexDirection: "row", backgroundColor: "#FFF", marginTop: -devHeight / 65, marginLeft: devWidth / 30, alignItems: "center", justifyContent: "center", width: devWidth / 9, backgroundColor: "white" }}>
                        <Text style={{ width: "95%", fontSize: 14, color: this.state.image_self_error != null ? "#CA020F" : "#5A6978", textAlign: "center", backgroundColor: "white" }}>
                          Foto
                        </Text>
                      </View>
                  }
                  <View style={{ alignItems: "center", justifyContent: 'center'}} >
                    <Image
                      source={ this.state.image_self == null ? require("../../../assets/black_avatar.png") : this.state.image_self }
                      style={{ width: "90%", height: "90%", resizeMode: "contain" }}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <TouchableOpacity onPress={ () => this.uploadImage("image_ktp", "image_ktp_name") } style={{ width: "65%", aspectRatio: 65/30, borderRadius: 25, borderWidth: 1, borderColor: this.state.image_ktp_error != null ? "#CA020F" : "#8492A6" }} >
                {
                  this.state.image_ktp_error != null ?
                    <View style={{ flexDirection: "row", marginTop: -devHeight / 65, marginLeft: devWidth / 30, justifyContent: "center", alignItems: "center", backgroundColor: "white", width: devWidth / 7.5 }} >
                      <Text style={{ width: "65%", fontSize: 14, color: this.state.image_ktp_error != null ? "#CA020F" : "#5A6978", backgroundColor: "white", textAlign: "center" }}>
                        KTP
                      </Text>
                      <Icon name="error" size={14} color="#CA020F" style={{ backgroundColor: "#FFF", }} />
                    </View>
                    :
                    <View style={{ flexDirection: "row", marginTop: -devHeight / 65, marginLeft: devWidth / 30, alignItems: "center", justifyContent: "center", backgroundColor: "white", width: devWidth / 8 }} >
                      <Text style={{ width: "95%", fontSize: 14, color: this.state.image_ktp_error != null ? "#CA020F" : "#5A6978", textAlign: "center", backgroundColor: "white" }}>
                        KTP
                      </Text>
                    </View>
                }
                <View style={{ alignItems: "center"}} >
                  <Image
                    source={ this.state.image_ktp == null ? require("../../../assets/ktp.png") : this.state.image_ktp }
                    style={{ width: "95%", height: "90%", resizeMode: "contain" }}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ width: "100%", aspectRatio: 20/1, flexDirection: "row", justifyContent: "space-between" }}>
              <Text style={{ width: "30%", fontSize: 10, color: "#969FAA", fontStyle: "italic" }}>
              Tap untuk upload foto
              </Text>
              <Text style={{ width: "65%", fontSize: 10, color: "#969FAA", fontStyle: "italic" }}>
              Tap untuk upload foto (optional)
              </Text>
            </View>
            <View style={{ marginVertical: "3%" }} >
              <Button block rounded onPress={() => this.submitForm()} style={{ width: "80%", backgroundColor: "#CC0611", alignSelf: "center" }} >
                <Text uppercase={false} style={{ fontSize: 12, color: "#FFF", width: "100%", textAlign: "center" }} >
                Selanjutnya
                </Text>
              </Button>
            </View>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}


const mapStateToProps = (state) => {
  const { appform, profile } = state
  return { appform, profile }
 };

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    FormSubmitApplication,
  }, dispatch)
);

export default connect(mapStateToProps ,mapDispatchToProps)(FormPengajuan);