import React, { Component } from "react";
import { Image, StatusBar, TextInput } from "react-native";
import { Container, Content, Text, View, Button, Header, Left, Right, Body, Title } from "native-base";
import { Input, Icon } from "react-native-elements";
import Spinner from "react-native-loading-spinner-overlay";
import styles from "./styles";

const logo = require("../../../assets/logoApp.png");

export default class Forgetpass extends Component {
  render() {
    return (
      <Container style={styles.container}>
        <StatusBar hidden={true}/>
        <Spinner visible={this.props.loading}/>
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ borderBottomWidth: 1, borderBottomColor: '#95251d', backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <Left style={[styles.headerLeft, {flex: 1}]}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" color='#CC0611' style={{  }}/>
            </Button>
          </Left>
          <Body style={[ styles.headerBody, { flex: 3 }]}>
            <Title style={styles.textBody}>Reset Password</Title>
          </Body>
          <Right style={{ flex: 1 }}>
          </Right>
        </Header>
        <Content>
          <Image source={logo} style={styles.logo}/>
          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 80, marginBottom: 12 }}>
            <Button transparent style={{ flex: 0.8, borderWeight: 2.5}}>
              <Input
                placeholder="Email"
                containerStyle={ styles.input }
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={{ fontSize: 16}}
                style={{ fontWeight: "400"}}
              />
            </Button>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 35 }}>
            <Button
              dark
              block
              rounded
              style={{ backgroundColor: "#CC0611", flex: 0.8 }}>
              <Text uppercase={false}>Reset Password</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
