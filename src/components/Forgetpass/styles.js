import { color } from '../styles.js';
export default {
  container: {
    backgroundColor: "#fff"
  },
  logo: {
    marginTop: 80,
    alignSelf: "center",
    width: 240
  },
  topText: {
    marginTop: 28,
    alignSelf: "center",
    color: "#CA020F",
    fontSize: 30,
    fontWeight: "700"
  },
  input: {
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#CCD2D9'
  },
  headerBody: {
    flexDirection: 'row',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textBody: {
    color: color.red,
    textAlign: 'center'
  },
};
