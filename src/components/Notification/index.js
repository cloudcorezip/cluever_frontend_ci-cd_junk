import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Text, Content, Card, CardItem, Footer, List, ListItem } from "native-base";
import { StatusBar, Image, View, ImageBackground, LayoutAnimation, ScrollView, TouchableOpacity, Platform, UIManager, FlatList, StyleSheet, Dimensions } from "react-native";
import Slideshow from 'react-native-image-slider-show'
import { Icon, SearchBar } from 'react-native-elements'
import styles from './styles'
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';

var devHeight = Dimensions.get('window').height;
var devWidth= Dimensions.get('window').width;

var data = [
        {
          title: "Selamat! Pembayaran Biaya Pendidikan telah diselesaikan",
          subtitle: "Hai! Wahid, pembayaran Biaya Pendidikan atas nama Bagas Alfiansyah telah dilakukan pada tanggal 24 Mei 2019. Lihat rincian disini.",
          date_time: "24 Mei 2019 - 19:00 WIB",
          type: "biaya-pendidikan",
          read: true,
          image: "https://images-na.ssl-images-amazon.com/images/I/71N-SUx1lHL._UX679_.jpg"
        },
        {
          title: "Ransel Anak Infikids ini hampir jadi milikmu!",
          subtitle: "Hai! Wahid, Ransel Anak Infikids selangkah lagi jadi milikmu, selesaikan pembayaran segera. Lihat rincian disini.",
          date_time: "22 Mei 2019 - 14:33 WIB",
          type: "marketplace",
          read: false,
          image: "https://images-na.ssl-images-amazon.com/images/I/71N-SUx1lHL._UX679_.jpg"
        },
      ]

export default class Notification extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: [],
    }

  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: "100%",
          backgroundColor: "#5A6978",
          marginVertical: "2%",
        }}
      />
    );
  };

  emptyList = (img) => {
    return <View style={{ width: devWidth / 3, alignItems: "center", justifyContent: "center", alignSelf: "center"}} ><Image resizeMethod source={img} /></View>
  }

  render () {
    return (
      <Container>
        <StatusBar translucent={true} hidden={true}/>
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ backgroundColor: '#fff' }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="chevron-left" color="#CC0611" />
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>Notifikasi</Title>
          </Body>
          <Right style={styles.headerRight}>
          </Right>
        </Header>
        <Content style={{ backgroundColor: 'white' }} contentContainerStyle={{ alignItems: "center", width: "100%" }}>
          <View style={{ width: "100%" }}>
            <ScrollableTabView
                style={{ width: "100%", aspectRatio: 1 / 1.5 }}
                initialPage={0}
                renderTabBar={() => <ScrollableTabBar style={{}} />}
                tabBarActiveTextColor={"#5A6978"}
                tabBarInactiveTextColor={"#969FAA"}
                tabBarUnderlineStyle={{ backgroundColor: "#CA020F",  }}
                tabBarTextStyle={{ fontSize: 14 }}
              >

                <ScrollView tabLabel='Semua Notifikasi' style={{ width: "95%", alignSelf: "center", marginTop: "2%" }}>
                  <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={ this.state.data }
                    ItemSeparatorComponent={this.renderSeparator}
                    ListEmptyComponent={<View style={{ height: devHeight / 1.5,flexDirection: "column", alignItems: "center", justifyContent: "center", alignSelf: "center"}} ><Image style={{width: devWidth / 1.8, height: devHeight / 3.5}} source={require("../../../assets/notif_empty.png")} /></View>}
                    renderItem={({item, index}) => (
                      <View style={{ width: "100%", borderRadius: 14, borderWidth: StyleSheet.hairlineWidth, borderColor: "#CCD2D9", alignItems: "center" }} >
                        <View style={{ width: "95%", paddingTop: "2%" }} >
                          { 
                            item.type == "marketplace" &&
                            <View style={{ flexDirection: "row", width: "100%", justifyContent: "flex-start" }}>
                              <View style={{ flexDirection: "column", width: "80%", marginRight: "3%" }}>
                                <Text style={{ fontWeight: "bold", fontSize: 11, color: "#5A6978"  }} >{item.title}</Text>
                                <Text style={{ fontSize: 10, color: "#5A6978" }} >{item.subtitle}</Text>
                              </View>
                              <Image
                                source={{uri: item.image }}
                                style={{ width: "15%", aspectRatio: 1 / 1 }}
                              />
                            </View> 
                          }
                          { 
                            item.type != "marketplace" &&
                            <View>
                              <Text style={{ fontWeight: "bold", fontSize: 11, color: "#5A6978"  }} >{item.title}</Text>
                              <Text style={{ fontSize: 10, color: "#5A6978" }} >{item.subtitle}</Text>
                            </View>
                          }
                          <View style={{ width: "100%", flexDirection: "row", alignItems: "center", justifyContent: "space-between", paddingTop: "1%", paddingBottom: "5%" }} >
                            <Image 
                              style={{ width: "8.5%", aspectRatio: 1 / 1, opacity: 0.4 }}
                              source={ item.type == "biaya-pendidikan" ? require("../../../assets/biayapend_b_3x.png") : require("../../../assets/mrktplc_b_3x.png")}
                            />
                            <Text style={{ fontSize: 9, fontStyle: "italic", color: "#969FAA" }} >{item.date_time}</Text>
                          </View>
                        </View>
                      </View>
                    )}
                  />
                </ScrollView>

                <ScrollView tabLabel='Biaya Pendidikan' style={{ width: "95%", alignSelf: "center", marginTop: "2%" }}>
                  <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={ this.state.data.filter((item) => item.type == "biaya-pendidikan").map((item) => item) }
                    ItemSeparatorComponent={this.renderSeparator}
                    ListEmptyComponent={<View style={{ height: devHeight / 1.5,flexDirection: "column", alignItems: "center", justifyContent: "center", alignSelf: "center"}} ><Image style={{width: devWidth / 1.8, height: devHeight / 3.5}} source={require("../../../assets/notif_empty.png")} /></View>}
                    renderItem={({item, index}) => (
                      <View style={{ width: "100%", borderRadius: 14, borderWidth: StyleSheet.hairlineWidth, borderColor: "#CCD2D9", alignItems: "center" }} >
                        <View style={{ width: "95%", paddingTop: "2%" }} >
                          <Text style={{ fontWeight: "bold", fontSize: 11, color: "#5A6978"  }} >{item.title}</Text>
                          <Text style={{ fontSize: 10, color: "#5A6978" }} >{item.subtitle}</Text>
                          <View style={{ width: "100%", flexDirection: "row", alignItems: "center", justifyContent: "space-between", paddingTop: "1%", paddingBottom: "5%" }} >
                            <Image 
                              style={{ width: "8.5%", aspectRatio: 1 / 1, opacity: 0.4 }}
                              source={ require("../../../assets/biayapend_b_3x.png") }
                            />
                            <Text style={{ fontSize: 9, fontStyle: "italic", color: "#969FAA" }} >{item.date_time}</Text>
                          </View>
                        </View>
                      </View>
                    )}
                  />
                </ScrollView>

                <ScrollView tabLabel='Program Pendidikan' style={{ width: "95%", alignSelf: "center", marginTop: "2%" }}>
                  <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={ this.state.data.filter((item) => item.type == "program-pendidikan").map((item) => item) }
                    ItemSeparatorComponent={this.renderSeparator}
                    ListEmptyComponent={<View style={{ height: devHeight / 1.5,flexDirection: "column", alignItems: "center", justifyContent: "center", alignSelf: "center"}} ><Image style={{width: devWidth / 1.8, height: devHeight / 3.5}} source={require("../../../assets/notif_empty.png")} /></View>}
                    renderItem={({item, index}) => (
                      <View style={{ width: "100%", borderRadius: 14, borderWidth: StyleSheet.hairlineWidth, borderColor: "#CCD2D9", alignItems: "center" }} >
                        <View style={{ width: "95%", paddingTop: "2%" }} >
                          <Text style={{ fontWeight: "bold", fontSize: 11, color: "#5A6978"  }} >{item.title}</Text>
                          <Text style={{ fontSize: 10, color: "#5A6978" }} >{item.subtitle}</Text>
                          <View style={{ width: "100%", flexDirection: "row", alignItems: "center", justifyContent: "space-between", paddingTop: "1%", paddingBottom: "5%" }} >
                            <Image 
                              style={{ width: "8.5%", aspectRatio: 1 / 1, opacity: 0.4 }}
                              source={ require("../../../assets/progpend_b_3x.png") }
                            />
                            <Text style={{ fontSize: 9, fontStyle: "italic", color: "#969FAA" }} >{item.date_time}</Text>
                          </View>
                        </View>
                      </View>
                    )}
                  />
                </ScrollView>

                <ScrollView tabLabel='Marketplace' style={{ width: "95%", alignSelf: "center", marginTop: "2%" }}>
                  <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={ this.state.data.filter((item) => item.type == "biaya-pendidikan").map((item) => item) }
                    ItemSeparatorComponent={this.renderSeparator}
                    ListEmptyComponent={<View style={{ height: devHeight / 1.5,flexDirection: "column", alignItems: "center", justifyContent: "center", alignSelf: "center"}} ><Image style={{width: devWidth / 1.8, height: devHeight / 3.5}} source={require("../../../assets/notif_empty.png")} /></View>}
                    renderItem={({item, index}) => (
                      <View style={{ width: "100%", borderRadius: 14, borderWidth: StyleSheet.hairlineWidth, borderColor: "#CCD2D9", alignItems: "center" }} >
                        <View style={{ width: "95%", paddingTop: "2%" }} >
                          <View style={{ flexDirection: "row", width: "100%", justifyContent: "flex-start" }}>
                            <View style={{ flexDirection: "column", width: "80%", marginRight: "3%" }}>
                              <Text style={{ fontWeight: "bold", fontSize: 11, color: "#5A6978"  }} >{item.title}</Text>
                              <Text style={{ fontSize: 10, color: "#5A6978" }} >{item.subtitle}</Text>
                            </View>
                            <Image
                              source={{uri: item.image }}
                              style={{ width: "15%", aspectRatio: 1 / 1 }}
                            />
                          </View>
                          <View style={{ width: "100%", flexDirection: "row", alignItems: "center", justifyContent: "space-between", paddingTop: "1%", paddingBottom: "5%" }} >
                            <Image 
                              style={{ width: "8.5%", aspectRatio: 1 / 1, opacity: 0.4 }}
                              source={ require("../../../assets/mrktplc_b_3x.png") }
                            />
                            <Text style={{ fontSize: 9, fontStyle: "italic", color: "#969FAA" }} >{item.date_time}</Text>
                          </View>
                        </View>
                      </View>
                    )}
                  />
                </ScrollView>

              </ScrollableTabView>

          </View>
        </Content>
      </Container>
   );
 }

}

