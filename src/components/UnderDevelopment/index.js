import React, { Component } from "react";
import { Header, Left, Body, Right, Text, Button, Container, Content, View } from "native-base";
import { ImageBackground, StatusBar, Dimensions, Image, TouchableOpacity } from "react-native" ;
import { Icon } from "react-native-elements"

var devWidth = Dimensions.get('window').width;
var devHeight = Dimensions.get('window').height;

export default class UnderDevelopment extends Component {
	render() {
		return (
			<Container style={{ flex: 1, backgroundColor: "#FFF" }}>
				<StatusBar 
		          	hidden={true}
		        />
				<ImageBackground
					source={require('../../../assets/under_construct_page.png')}
					style={{ height: '100%' }}
				>
					<Header transparent style={{ shadowColor: 'transparent', marginTop: -25 }} >
						<Left style={{ flex: 0.3 }} >
							<Button transparent onPress={() => this.props.navigation.goBack()}>
								<Icon name="arrow-back" color="#D32B36"/>
							</Button>
						</Left>
						<Body style={{ flex: 0.4 }} >
						</Body>
						<Right style={{ flex: 0.3 }} >
						</Right>
					</Header>
					<Content contentContainerStyle={{ alignItems: "center" }} >
						<View style={{ width: "85%", marginTop: 9 * devHeight / 16 }} >
							<Text style={{ color: "#CA020F", fontSize: 29, fontStyle: "italic", fontWeight: "bold" }} >
							Sorry,
							</Text>
							<Text style={{ color: "#CA020F", fontSize: 25, fontStyle: "italic", fontWeight: "bold" }} >
							Page Under Construction
							</Text>
						</View>
					</Content>
				</ImageBackground>
			</Container>
		);
	}
}