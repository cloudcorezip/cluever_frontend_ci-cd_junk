import React, { Component } from "react"
import { WebView } from "react-native-webview";
import { ActivityIndicator } from "react-native"
import { View, Header, Text } from "native-base";

export default class Faq extends Component {
  Loading = () => {
    return (
      <ActivityIndicator color='#009b88' style={{ flex: 1, justifyContent: "center", width: 240}} />
    )
  }
  

  render() {
    return (
      <WebView source={{ uri: "https://docs.google.com/document/u/1/d/e/2PACX-1vSPcyCuiNRUDaLUEA59ThAlhknRN7pU_ALSJ9lPqh_416GfJ9WFnb72UCEjSjoFDLRcWC_61JfaDNb9/pub"}} 
        useWebKit={true}
        renderLoading={this.Loading}
        startInLoadingState={true}
      />
    )
  }

}
