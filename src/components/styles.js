import { Dimensions } from "react-native";

export const devWidth = Dimensions.get('window').width;
export const devHeight = Dimensions.get('window').height;

export default {
  margin: {
    margin: 8
  },
  radius: {
    borderRadius: 8
  }
}

export const color = {
  grey: '#F5F5F5',
  darkGrey: '#464E51',
  red: '#CA020F',
  white: '#FFFFFF',
  black: '#3B4045',
  button: {
    disabled: '#464E51'
  }
}
