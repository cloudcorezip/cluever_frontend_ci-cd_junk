import { color, devHeight, devWidth } from "../styles";

export default {
  container: {
    flex: 1,
    backgroundColor: color.white
  },
  headerLeft: {
    flex: 0.3
  },
  headerBody: {
    flex: 0.4
  },
  textBody: {
    alignSelf: "center",
    color: "#CC0611",
    fontWeight: "bold",
    fontSize: 18
  },
  headerRight: {
    flex: 0.3
  },
  feedsCardTitle: {
    color: color.white,
    fontWeight: "600"
  },
  feedsCardUrl: {
    color: color.white,
    fontSize: 12,
    marginTop: 2
  },
  feedsCardDescription: {
    color: color.white,
    fontSize: 8,
    marginTop: 3
  },
};
