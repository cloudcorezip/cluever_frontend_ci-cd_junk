import React, { Component } from "react";
import {
  Text,
  View,
  Header,
  Container,
  Left,
  Title,
  Body,
  Right,
  Button,
  Content,
  Footer,
  FooterTab
} from "native-base";
import { StatusBar, ImageBackground, FlatList } from "react-native";
import { ScrollView } from "react-native";
import { Icon, Image, Divider } from "react-native-elements";
import ScrollableTabView, {
  ScrollableTabBar
} from "react-native-scrollable-tab-view";

import styles from "./styles";
import { devHeight } from "../styles";

const imgBanner = require("../../../assets/banner_feed_1015x293.png");

export default class Feeds extends Component {
  constructor(props) {
    super(props);

    this.state = {
      banner: imgBanner,

    };
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar hidden={true} translucent={true} />
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ backgroundColor: "#fff" }}
        >
          <Left style={styles.headerLeft} />
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>Feeds</Title>
          </Body>
          <Right style={styles.headerRight}>
            <Button transparent>
              <Icon name="notifications" color="#CC0611" />
            </Button>
          </Right>
        </Header>
        <View>
          <ImageBackground source={this.state.banner} style={{ height: 120 }} />
        </View>
        <Content>
          <ScrollableTabView
            style={{ width: "100%", aspectRatio: 1 / 1.5 }}
            initialPage={0}
            renderTabBar={() => <ScrollableTabBar />}
            tabBarActiveTextColor={"#5A6978"}
            tabBarInactiveTextColor={"#969FAA"}
            tabBarUnderlineStyle={{ backgroundColor: "#CA020F" }}
            tabBarTextStyle={{ fontSize: 14 }}
          >
            <ScrollView tabLabel='Hot'>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={this.props.data}
                renderItem={({ item, index }) => (
                  <View>
                    <View>
                      {
                        item.visited >= 100 &&
                        <View style={{ flex: 1 }}>
                          <ImageBackground source={{ uri: item.img }} style={{ height: devHeight / 2.5, flexDirection: "column-reverse", marginTop: 20 }}>
                            <View style={{ width: "100%", marginLeft: 20, height: devHeight / 12 }}>
                              <Text style={styles.feedsCardTitle}>{item.title}</Text>
                              <Text style={styles.feedsCardUrl}>{item.url}</Text>
                              <Text style={styles.feedsCardDescription}>{item.description}</Text>
                            </View>
                          </ImageBackground>
                          <Text style={{ fontSize: 12, marginLeft: 15, marginVertical: 10 }}>{item.caption}</Text>
                          <Divider style={{ marginVertical: "1%", height: 1, width: "95%", alignSelf: "center" }} />
                        </View>
                      }
                    </View>
                  </View>
                )}
              >
              </FlatList>
            </ScrollView>
            <ScrollView tabLabel='Pendidikan'>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={this.props.data.filter((item) => item.type == "Pendidikan").map((item) => item)}
                renderItem={({ item, index }) => (
                  <View>
                    <View>
                      <View style={{ flex: 1 }}>
                        <ImageBackground source={{ uri: item.img }} style={{ height: devHeight / 2.5, flexDirection: "column-reverse", marginTop: 20 }}>
                          <View style={{ width: "100%", marginLeft: 20, height: devHeight / 12 }}>
                            <Text style={styles.feedsCardTitle}>{item.title}</Text>
                            <Text style={styles.feedsCardUrl}>{item.url}</Text>
                            <Text style={styles.feedsCardDescription}>{item.description}</Text>
                          </View>
                        </ImageBackground>
                        <Text style={{ fontSize: 12, marginLeft: 15, marginVertical: 10 }}>{item.caption}</Text>
                        <Divider style={{ marginVertical: "1%", height: 1, width: "95%", alignSelf: "center" }} />
                      </View>
                    </View>
                  </View>
                )}
              >
              </FlatList>
            </ScrollView>
            <ScrollView tabLabel='Keuangan'>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={this.props.data.filter((item) => item.type == "Keuangan").map((item) => item)}
                renderItem={({ item, index }) => (
                  <View>
                    <View>
                      <View style={{ flex: 1 }}>
                        <ImageBackground source={{ uri: item.img }} style={{ height: devHeight / 2.5, flexDirection: "column-reverse", marginTop: 20 }}>
                          <View style={{ width: "100%", marginLeft: 20, height: devHeight / 12 }}>
                            <Text style={styles.feedsCardTitle}>{item.title}</Text>
                            <Text style={styles.feedsCardUrl}>{item.url}</Text>
                            <Text style={styles.feedsCardDescription}>{item.description}</Text>
                          </View>
                        </ImageBackground>
                        <Text style={{ fontSize: 12, marginLeft: 15, marginVertical: 10 }}>{item.caption}</Text>
                        <Divider style={{ marginVertical: "1%", height: 1, width: "95%", alignSelf: "center" }} />
                      </View>
                    </View>
                  </View>
                )}
              >
              </FlatList>
            </ScrollView>
            <ScrollView tabLabel='Psikologi'>
              <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={this.props.data.filter((item) => item.type == "Psikologi").map((item) => item)}
                renderItem={({ item, index }) => (
                  <View>
                    <View>
                      <View style={{ flex: 1 }}>
                        <ImageBackground source={{ uri: item.img }} style={{ height: devHeight / 2.5, flexDirection: "column-reverse", marginTop: 20 }}>
                          <View style={{ width: "100%", marginLeft: 20, height: devHeight / 12 }}>
                            <Text style={styles.feedsCardTitle}>{item.title}</Text>
                            <Text style={styles.feedsCardUrl}>{item.url}</Text>
                            <Text style={styles.feedsCardDescription}>{item.description}</Text>
                          </View>
                        </ImageBackground>
                        <Text style={{ fontSize: 12, marginLeft: 15, marginVertical: 10 }}>{item.caption}</Text>
                        <Divider style={{ marginVertical: "1%", height: 1, width: "95%", alignSelf: "center" }} />
                      </View>
                    </View>
                  </View>
                )}
              >
              </FlatList>
            </ScrollView>
          </ScrollableTabView>
        </Content>
        <Footer style={{ backgroundColor: "transparent" }}>
          <FooterTab style={{ backgroundColor: "transparent" }}>
            <Button button onPress={() => this.props.onPress("Home")}>
              <View style={{}}>
                <Icon name="home" color="gray" style={{}} />
              </View>
            </Button>
            <Button onPress={() => this.props.onPress("RiwayatTransaksi")} >
              <Icon name="attach-money" color="gray" />
            </Button>
            <Button >
              <Icon type="font-awesome" name="newspaper-o" color="#CA020F" />
            </Button>
            <Button onPress={() => this.props.onPress("ProfileUser")}>
              <Icon name="person" color="gray" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
