export default {
    container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  headerLeft: {
    flex: 0.3
  },
  headerBody: {
    flex: 0.4
  },
  textBody: {
    alignSelf: "center",
    color: "#CC0611",
    fontWeight: "bold",
    fontSize: 18
  },
  headerRight: {
    flex: 0.3
  },
  dateText: {
    color: '#CA020F',
    fontSize: 18,
    fontWeight: '600',
    marginLeft: 20,
    marginVertical: 10
  },
  containerList: {
    backgroundColor: '#FCFCFC',
    marginLeft: 20,
    width: '70%'
  }
};
