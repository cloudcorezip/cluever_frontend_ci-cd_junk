import React, { Component } from "react";
import { StatusBar, ImageBackground } from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Footer,
  Right,
  Button,
  Title,
  View,
  FooterTab,
  Text,
  Card,
  CardItem,
  Thumbnail,
  Content
} from "native-base";

import styles from "./styles";
import { Icon, Divider } from "react-native-elements";

var img_banner = require("../../../assets/banner_riwayat_transaksi.png");

export default class RiwayatTransaksi extends Component {
  constructor() {
    super();

    this.state = {
      banner: img_banner
    };
  }
  render() {
    return (
      <Container style={styles.container}>
        <StatusBar hidden={true} />
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ backgroundColor: "#fff" }}
        >
          <Left style={styles.headerLeft} />
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>Riwayat Transaksi</Title>
          </Body>
          <Right style={styles.headerRight}>
            <Button transparent>
              <Icon name="notifications" color="#CC0611" style={{}} />
            </Button>
          </Right>
        </Header>
        <Content>
          <View>
            <ImageBackground
              source={this.state.banner}
              style={{
                height: 150,
                flexDirection: "column",
                justifyContent: "flex-end",
                alignItems: "center"
              }}
            />
          </View>
          <View>
            <Text style={styles.dateText}>25 Mei 2019</Text>
          </View>

          <View style={{ alignItems: "center" }}>
            <Card style={{ width: "90%", flex: 0 }}>
              <CardItem style={{ backgroundColor: "#FCFCFC" }}>
                <Left>
                  <Thumbnail
                    square
                    source={require("../../../assets/cart_clr_3x.png")}
                    style={{ height: 28, width: 28 }}
                  />
                  <Body style={{ flex: 0.8 }}>
                    <Text style={{ fontSize: 14 }}>Pembelian</Text>
                    <Text style={{ fontSize: 12 }}>3 Buah Seragam Sekolah</Text>
                  </Body>
                </Left>
                <Right style={{ flex: 0.3 }}>
                  <Text style={{ fontSize: 12 }}>Rp.432.000</Text>
                </Right>
              </CardItem>
              <Divider style={{ width: "85%", marginHorizontal: 20 }} />
              <CardItem style={{ backgroundColor: "#FCFCFC" }}>
                <Left>
                  <Thumbnail
                    square
                    source={require("../../../assets/biayapend_clr_3x.png")}
                    style={{ height: 28, width: 28 }}
                  />
                  <Body style={{ flex: 0.8 }}>
                    <Text style={{ fontSize: 14 }}>
                      Pembayaran Edulab Cicilan ke - 5
                    </Text>
                    <Text style={{ fontSize: 12 }}>Bagas Alfiansyah</Text>
                  </Body>
                </Left>
                <Right style={{ flex: 0.3 }}>
                  <Text style={{ fontSize: 10 }}>Rp.1.000.000</Text>
                </Right>
              </CardItem>
            </Card>
          </View>
          <View>
            <Text style={styles.dateText}>25 April 2019</Text>
          </View>
          <View style={{ alignItems: "center", flex: 1 }}>
            <Card style={{ width: "90%" }}>
              <CardItem style={{ backgroundColor: "#FCFCFC" }}>
                <Left>
                  <Thumbnail
                    square
                    source={require("../../../assets/biayapend_clr_3x.png")}
                    style={{ height: 28, width: 28 }}
                  />
                  <Body style={{ flex: 0.8 }}>
                    <Text style={{ fontSize: 14 }}>
                      Pembayaran ITB Cicilan ke - 4
                    </Text>
                    <Text style={{ fontSize: 12 }}>Putra Samudra</Text>
                  </Body>
                </Left>
                <Right style={{ flex: 0.3 }}>
                  <Text style={{ fontSize: 12 }}>Rp.700.000</Text>
                </Right>
              </CardItem>
              <Divider style={{ width: "85%", marginHorizontal: 20 }} />
              <CardItem style={{ backgroundColor: "#FCFCFC" }}>
                <Left>
                  <Thumbnail
                    square
                    source={require("../../../assets/biayapend_clr_3x.png")}
                    style={{ height: 28, width: 28 }}
                  />
                  <Body style={{ flex: 0.8 }}>
                    <Text style={{ fontSize: 14 }}>
                      Pembayaran Edulab Cicilan ke - 4
                    </Text>
                    <Text style={{ fontSize: 12 }}>Putra Samudra</Text>
                  </Body>
                </Left>
                <Right style={{ flex: 0.4 }}>
                  <Text style={{ fontSize: 12 }}>Rp.700.000</Text>
                </Right>
              </CardItem>
            </Card>
          </View>
        </Content>
        <Footer style={{ backgroundColor: "transparent" }}>
          <FooterTab style={{ backgroundColor: "transparent" }}>
            <Button button onPress={() => this.props.onPress("Home")}>
              <View style={{}}>
                <Icon name="home" color="gray" style={{}} />
              </View>
            </Button>
            <Button>
              <Icon name="attach-money" color="#CA020F" />
            </Button>
            <Button onPress={() => this.props.onPress("Feeds")}>
              <Icon type="font-awesome" name="newspaper-o" color="gray" />
            </Button>
            <Button onPress={() => this.props.onPress("ProfileUser")}>
              <Icon name="person" color="gray" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
