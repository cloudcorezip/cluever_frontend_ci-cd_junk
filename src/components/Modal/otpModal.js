import React, { Component } from "react";
import axios from 'axios';
import qs from 'qs';
import { Keyboard, TouchableOpacity } from "react-native";
import Modal from "react-native-modalbox";
import { View, Text, Button } from "native-base";
import { Input, Icon } from "react-native-elements";
import Spinner from 'react-native-loading-spinner-overlay';

import styles from "./styles";
import { color } from '../styles.js';

const STATE_INPUT = 'INPUT'
const STATE_VERIFY = 'VERIFY'
const STATE_VERIFIED = 'VERIFIED'
const STATE_INVALID = 'INVALID'
const STATE_ERROR_NETW = 'ERROR_NETW'

const OTP_RESEND_INTERVAL = 180

const authyConfig = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'X-Authy-API-Key': '2iZ7BcXBlGPPthGDKMc1whIqStRNtBDQ'
  }
}

export default class OtpModal extends Component {
  constructor() {
    super();
    this.state = {
      show: false,
      tokens: [null, null, null, null, null, null],
      loading: false,
      otpState: STATE_INPUT,
      authyUserId: null,
      timer: null,
      counter: OTP_RESEND_INTERVAL,
      counterDisplay: '',
      canSendOtp: true
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.show !== state.show) {
      return {
        show: props.show
      }
    }

    return null
  }

  componentWillUnmount() {
    clearInterval(this.state.timer)
  }

  inputOtp = async (input, index) => {
    let tokens = [ ...this.state.tokens ]
    tokens[index - 1] = input
    await this.setState({ tokens })

    let emptyIdx = null
    for (var i = 5; i > -1; i--) {
      if (!this.state.tokens[i]) {
        emptyIdx = i + 1
      }
    }

    if (emptyIdx) {
      this[`otpInput${ emptyIdx }`].focus()
    } else {
      this.submitOtp()
    }
  }

  sendOtp = async () => {
    await this.setState({ canSendOtp: false })
    const newUserEndPoint = 'https://api.authy.com/protected/json/users/new'
    const { fullname, phone_no, email } = this.props.data
    const data = {
      user: {
        email: email,
        cellphone: phone_no,
        country_code: '+62'
      }
    }
    axios.post(newUserEndPoint, qs.stringify(data), authyConfig)
    .then((newUserresponse) => {
      const authyUserId = newUserresponse.data.user.id
      this.setState({
        otpState: STATE_INPUT,
        authyUserId
      })
      const otpEndPoint = `https://api.authy.com/protected/json/sms/${ authyUserId }?force=true`
      axios.get(otpEndPoint, authyConfig)
      .then((response) => {
        console.log('OTP Response', response.data)
      })
      .catch((error) => {
        console.log('OTP Error', error)
      })
    })
    .catch((error) => {
      console.log('New User Error', error)
      if (!error.status) {
        this.setState({ otpState: STATE_ERROR_NETW })
      }
    });

    this.setState({
      timer: setInterval(() => {
        let { counter } = this.state
        if (counter > 1) {
          counter -= 1
          const minutes = Math.floor(counter / 60)
          let seconds = counter - minutes * 60
          if (seconds < 10) {
            seconds = `0${ seconds }`
          }
          this.setState({
            counter,
            counterDisplay: `(${ minutes }:${ seconds })`
          })
        } else {
          clearInterval(this.state.timer)
          this.setState({
            canSendOtp: true,
            counterDisplay: '',
            counter: OTP_RESEND_INTERVAL
          })
        }
      }, 1000)
    })
  }

  submitOtp = (input) => {
    Keyboard.dismiss()
    let token = ''
    this.state.tokens.map((item) => {
      token += item
    })
    this.setState({ otpState: STATE_VERIFY })
    const verifyOtpEndPoint = `https://api.authy.com/protected/json/verify/${ token }/${ this.state.authyUserId }`
    axios.get(verifyOtpEndPoint, authyConfig)
    .then(async (response) => {
      const { success } = response.data
      console.log('success', success)
      if (success === 'true') {
        await this.props.submitForm()
        this.setState({ otpState: STATE_VERIFIED })
      } else {
        this.setState({ otpState: STATE_INVALID })
      }
    })
    .catch((error) => {
      console.log('Send OTP Error', error)
    })
  }

  backToInput = () => {
    this.props.handleVisibility(false)
    this.props.phoneNumberFocus()
  }

  displayModalContent = () => {
    switch (this.state.otpState) {
      case STATE_INPUT:
        const { phone_no } = this.props.data
        return (
          <View style={[styles.contentModal, styles.modalCenter, { backgroundColor: 'white', borderRadius: 16 }]}>
            <Text style={ otpStyles.otpTitle }>
              Masukkan kode yang dikirimkan via SMS ke nomor telepon Anda
            </Text>
            <View style={ otpStyles.otpModalInputWrapper }>
              <Input 
                ref={(input) => { this.otpInput1 = input }}
                autoFocus
                containerStyle={ otpStyles.otpModalInput }
                inputStyle={ otpStyles.inputFormModal }
                onChangeText={ (value) => this.inputOtp(value, 1) }
                keyboardType={'numeric'}
                maxLength={1}
              />
              <Input
                ref={(input) => { this.otpInput2 = input }}
                containerStyle={ otpStyles.otpModalInput }
                inputStyle={ otpStyles.inputFormModal }
                onChangeText={ (value) => { this.inputOtp(value, 2) }}
                keyboardType={'numeric'}
                maxLength={1}
              />
              <Input
                ref={(input) => { this.otpInput3 = input }}
                containerStyle={ otpStyles.otpModalInput }
                inputStyle={ otpStyles.inputFormModal }
                onChangeText={ (value) => { this.inputOtp(value, 3) }}
                keyboardType={'numeric'}
                maxLength={1}
              />
              <Input
                ref={(input) => { this.otpInput4 = input }}
                containerStyle={ otpStyles.otpModalInput }
                inputStyle={ otpStyles.inputFormModal }
                onChangeText={ (value) => { this.inputOtp(value, 4) }}
                keyboardType={'numeric'}
                maxLength={1}
              />
              <Input
                ref={(input) => { this.otpInput5 = input }}
                containerStyle={ otpStyles.otpModalInput }
                inputStyle={ otpStyles.inputFormModal }
                onChangeText={ (value) => { this.inputOtp(value, 5) }}
                keyboardType={'numeric'}
                maxLength={1}
              />
              <Input
                ref={(input) => { this.otpInput6 = input }}
                containerStyle={ otpStyles.otpModalInput }
                inputStyle={ otpStyles.inputFormModal }
                onChangeText={ (value) => { this.inputOtp(value, 6) }}
                keyboardType={'numeric'}
                maxLength={1}
              />
            </View>
            <TouchableOpacity onPress={ () => this.backToInput() }>
              <Text style={ otpStyles.otpWrongNumber }>
                Nomor Anda bukan { phone_no }?
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={ () => {
              if (this.state.canSendOtp) {
                this.sendOtp()
              }
            } }>
              <Text style={[ otpStyles.otpWrongNumber, this.state.canSendOtp ? {} : { color: color.darkGrey } ]}>
                Kirim ulang OTP { this.state.counterDisplay }
              </Text>
            </TouchableOpacity>
          </View>
        )
        break
      case STATE_VERIFY:
        return (
          <View style={[styles.contentModal, styles.modalCenter, { backgroundColor: 'white', borderRadius: 16 }]}>
            <Spinner visible={ true } />
          </View>
        )
        break
      case STATE_VERIFIED:
        setTimeout(() => this.props.handleVisibility(false), 3000)
        return (
          <View style={[styles.contentModal, styles.modalCenter, { backgroundColor: 'white', borderRadius: 16 }]}>
            <Icon 
              name="check"
              style={ otpStyles.txtMessage }
              size={48}
              color={ color.red }
            />
            <Text style={{ textAlign: 'center', padding: 16 }}>Nomor Anda telah terverifikasi</Text>
          </View>
        )
        break
      case STATE_INVALID:
        setTimeout(() => this.setState({ otpState: STATE_INPUT }), 5000)
        return (
          <View style={[styles.contentModal, styles.modalCenter, { backgroundColor: 'white', borderRadius: 16 }]}>
            <Icon 
              name="clear"
              style={ otpStyles.txtMessage }
              size={48}
              color={ color.red }
            />
            <Text style={{ textAlign: 'center', padding: 16 }}>Verifikasi gagal, silakan masukan nomor yang kami kirim melalui SMS</Text>
          </View>
        )
        break
      case STATE_ERROR_NETW:
        setTimeout(() => this.props.handleVisibility(false), 8000)
        return (
          <View style={[styles.contentModal, styles.modalCenter, { backgroundColor: 'white', borderRadius: 16 }]}>
            <Icon 
              name="signal_cellular_off"
              style={ otpStyles.txtMessage }
              size={48}
              color={ color.red }
            />
            <Text style={{ textAlign: 'center', padding: 16 }}>Anda tidak terhubung ke internet, silakan periksa kembali jaringan Anda, kemudian coba lagi.</Text>
          </View>
        )
        break
    }
  }

  render() {
    return (
      <Modal
        style={[ styles.modalCenter, { backgroundColor: 'transparent' } ]}
        coverScreen={ true }
        backdropPressToClose={ false }
        isOpen={ this.state.show }
        onClosed={ () => this.props.handleVisibility(false) }
      >
      { this.displayModalContent() }
      </Modal>
    );
  }
}

const otpStyles = {
  inputForm: {
    fontSize: 14
  },
  inputFormModal: {
    fontSize: 14,
    textAlign: 'center'
  },
  otpModalInputWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.green
  },
  otpModalInput: {
    width: 36,
    borderWidth: 2,
    marginHorizontal: 4,
    borderColor: color.grey,
    borderRadius: 4,
    backgroundColor: color.white,
    paddingHorizontal: 4,
    paddingTop: 4
  },
  otpTitle: {
    paddingHorizontal: 16,
    textAlign: 'center',
    color: color.black,
    fontSize: 14,
    marginBottom: 32
  },
  otpBtnClose: {
    position: "absolute",
    top: 4,
    right: 16
  },
  otpWrongNumber: {
    fontSize: 12,
    color: color.red,
    marginVertical: 16
  }
}