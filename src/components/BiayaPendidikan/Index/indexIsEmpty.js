import React, { Component } from 'react';
import {
  Container,
  Title,
  Header,
  Left,
  Button,
  Body,
  Right,
  Content,
  Text
} from "native-base";
import { Icon } from 'react-native-elements';
import { StatusBar, Image, ImageBackground, View, Dimensions, ScrollView, RefreshControl } from 'react-native';
import Dialog, {DialogContent } from 'react-native-popup-dialog';
import { connect } from "react-redux";
import axios from "axios";

import { TAGIHAN_USER_URI } from "../../../constants/apis";
import { setInstallment } from "../../../actions";
import { getInstallment } from "../../../utils"

import styles from './styles';
import { bindActionCreators } from 'redux';

const imgBanner = require("../../../../assets/banner_biaya_pend.png");
const iconBanner = require("../../../../assets/icon/hand_dollar.png");
const emptyListImage = require("../../../../assets/cicilan_empty.png");

const devHeight = Dimensions.get('window').height;
const flex_percent = 20 / devHeight;



class BiayaPendidikanIndexEmpty extends Component {
  constructor() {
    super()
    this.state = {
      banner: imgBanner,
      bannerIcon: iconBanner,
      isVisible: false,
      refreshing: false
    }
  }

  // ADD POPUP FOR QR CODE AND CARI INSTITUSI
  set_isVisible = () => {
    let isVisible_ = this.state.isVisible
    isVisible_ = !isVisible_
    this.setState({ isVisible: isVisible_ })
  }

  onRefresh = () => {
    this.setState({ refreshing: true})
    axios.post(TAGIHAN_USER_URI, null, {
      headers: {
        'Authorization': this.props.auth.token
      }
    }).then((response) => {
      console.log(response)
      getInstallment(this.props.auth.token).then((res) => {
        this.props.setInstallment(res)
      })
      this.setState({ refreshing: false })
    }).catch((err) => {
      console.log(err)
    })
  }

  render() {
    return (
      <Container style={{ backgroundColor: "white" }}>
        <StatusBar hidden={true} />
        <Header noShadow iosBarStyle={"dark-content"} style={{ backgroundColor: "#fff" }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" color="#CC0611" />
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>
              Biaya Pendidikan
                </Title>
          </Body>
          <Right style={styles.headerRight}>
            <Button transparent>
              <Icon name="tune" color="#CC0611" />
            </Button>
          </Right>
        </Header>
        <ScrollView refreshControl = {
          <RefreshControl 
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }
        >
        <Content style={{ backgroundColor: "#FFF" }} contentContainerStyle={{ flexDirection: "column", justifyContent: "space-between" }}>
          <View style={{ flex: flex_percent }}>
            <ImageBackground source={this.state.banner} style={{ height: 150, flex: 1, flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
              <Left>
                <Image source={this.state.bannerIcon} style={{ tintColor: "#fff", height: 48, width: 48, marginLeft: 40 }} align="center" />
              </Left>
              <View style={{ paddingHorizontal: 50, marginTop: 50 }}>
                <Right>
                  <Text style={{ color: "#fff", fontSize: 15.5, fontWeight: "400" }}>
                    Ajukan Cicilan
                      </Text>
                  <Text style={{ color: "#fff", fontSize: 23, fontWeight: "700", fontStyle: "italic", textAlign: "right" }}>
                    Biaya Pendidikan
                      </Text>
                </Right>
              </View>
            </ImageBackground>
            <View style={{ alignItems: "center" }}>
              <View style={{ maxHeight: 14, marginTop: 10 }}>
                <View style={{ flexDirection: "row", width: "90%", maxHeight: 14 }}>
                  <Text style={{ flex: 7, fontSize: 12, color: "#969FAA" }}>
                    Daftar Cicilan
                      </Text>
                  <Icon name="unfold-more" size={14} color="#969FAA" style={{ flex: 1 }} />
                </View>
              </View>
              <Image source={emptyListImage} style={{ width: "100%", height: devHeight / 4, marginTop: devHeight / 12 }} />
            </View>
          </View>
        </Content>
        </ScrollView>
        <View style={{ flexDirection: "column", paddingLeft: 15, paddingRight: 15, backgroundColor: '#fff' }}>
          <Button block rounded onPress={() => this.props.onPress("CariInstitusi") } style={ styles.buttonPengajuanCicilan } >
            <Text uppercase={false} >Ajukan Cicilan</Text>
          </Button>
        </View>
      </Container>
    )
  }
}

const mapStateToProps = (state) => {
  const { auth } = state;
  return { auth }
}

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({
    setInstallment
  }, dispatch)
)

export default connect(mapStateToProps, mapDispatchToProps)(BiayaPendidikanIndexEmpty)