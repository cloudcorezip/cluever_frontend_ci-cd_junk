import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Text, Content
} from "native-base";
import { StatusBar, Image, View, ImageBackground, Dimensions, FlatList, TouchableOpacity, ScrollView, RefreshControl } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Icon, Divider } from "react-native-elements";
import Collapsible from 'react-native-collapsible';
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import moment from "moment";
import axios from "axios";
import "moment/locale/id";

import { getInstallment } from "../../../utils";
import { TAGIHAN_USER_URI } from "../../../constants/apis"
import { setInstallment } from "../../../actions"


import styles from "./styles";


var devWidth = Dimensions.get('window').width;
var devHeight = Dimensions.get('window').height;

class PaymentList extends Component {


  listPaymentItemStyle(status) {
    if (status !== "Unpaid") {
      return {
        borderRadius: 25, backgroundColor: '#CCD2D9', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', opacity: 0.1
      }
    } else {
      return {
        borderRadius: 25, backgroundColor: '#CCD2D9', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', opacity: 1
      }
    }
  }

  statusRendder(index, item) {
    // console.log(item)
    if (item.status !== "Unpaid") {
      return (
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
          <View style={{ flex: 4, flexDirection: 'row', justifyContent: 'flex-end' }} >
            <Text style={{ fontStyle: 'italic', fontSize: 11.5 }} >
              {item.due_date}
            </Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }} >
            <Icon name='done' color='#000' size={16} />
          </View>
        </View>
      );
    } else {
      if (index == 0 || (index > 0 && this.props.data[index - 1].status !== "Unpaid")) {
        return (
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
            <View style={{ flex: 3, flexDirection: 'row', justifyContent: 'flex-end' }} >
              <Text style={{ fontStyle: 'italic', fontSize: 11.5, color: '#CA020F' }} >
                {item.due_date}
              </Text>
            </View>
            <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'flex-end' }} >
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                <Icon name='timer' size={9.5} color='#CA020F' />
                <Text style={{ color: '#CA020F', fontSize: 9, fontStyle: 'italic', marginLeft: 3 }} >
                  {(Math.floor((Date.parse(item.due_date) - Date.now()) / 86400000))} hari lagi
                </Text>
              </View>
            </View>
          </View>
        );
      } else {
        return (
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
            <View style={{ flex: 3, flexDirection: 'row', justifyContent: 'flex-end' }} >
              <Text style={{ fontStyle: 'italic', fontSize: 11.5, color: '#969FAA' }} >
                {item.payment_date}
              </Text>
            </View>
          </View>
        );
      }
    }
  }



  render() {
    return (
      <FlatList
        style={{ flex: 0 }}
        data={this.props.data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (

          <View style={{ width: devWidth * 0.8, marginBottom: 10 }} >
            <View style={this.listPaymentItemStyle(item.status)} >
              <View transparent style={{ width: 9 }}>
              </View>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                <Text style={{ fontSize: 13, flex: 3, fontWeight: 'bold' }} >
                  {index + 1}. {moment(item.due_date).format("MMMM")}
                </Text>
                <Text style={{ fontSize: 12, flex: 2 }} >
                </Text>
              </View>
              {this.statusRendder(index, item)}
              <View transparent style={{ width: 9 }}>
              </View>
            </View>
          </View>

        )}
      />
    )
  }

}


class InstallmentList extends Component {

  constructor(props) {
    super(props);

    this.state = this.props.state;
  }

  componentWillReceiveProps(nextProps) {
    this.setState(nextProps.state);
  }

  shouldComponentUpdate(nextProps) {
    return (this.state !== nextProps.state)
  }

  iconStyleA(index, arrIndex) {
    if (this.state.arrColl[this.props.arrIndex][index]) {
      return 0;
    } else {
      return 14;
    }
  }

  iconStyleB(index, arrIndex) {
    if (this.state.arrColl[this.props.arrIndex][index]) {
      return 14;
    } else {
      return 0;
    }
  }


  redirect(data) {
    // console.log(data)
    // if(data.instalm)
    this.props.navigation.navigate("BiayaPendidikanPayment", { installmentData: data })
  }

  numberWithDots(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }


  render() {
    if (this.props.data[0].installments.length > 0) {
      return (
        <FlatList
          style={{ flex: 0 }}
          data={this.props.data}
          extraData={this.props.state}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (

            <View style={{ width: devWidth * 0.9, alignItems: 'center', }} >
              <TouchableOpacity transparent onPress={() => this.props.pressFunction(index, this.props.arrIndex)}>
                <View style={{ flexDirection: 'column', borderRadius: 25, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', width: '100%' }} >
                  <View style={{ flex: 1, flexDirection: 'row', width: '95%', marginVertical: 4, marginBottom: 14 }} >

                    <View style={{ flex: 4, flexDirection: 'column' }} >
                      <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center' }} >
                        <Text style={{ fontWeight: 'bold', color: '#5A6978', fontSize: 16 }} >
                          {moment(item.installments[0].due_date).format("MMMM")}
                        </Text>
                        <Text style={{ color: '#969FAA', fontSize: 13 }} >
                          {item.institution}
                        </Text>
                      </View>
                      <View style={{ flex: 4, flexDirection: 'column' }} >
                        <Text style={{ flex: 1, color: '#C5C5C5', fontSize: 8 }} >
                          Jatuh Tempo
                    </Text>
                        <View style={{ flex: 4, flexDirection: 'row' }} >
                          <View style={{ flex: 3, flexDirection: 'column', justifyContent: 'space-evenly', alignItems: 'center' }} >
                            <Text style={{ flex: 1, fontSize: 19, color: '#CA020F', fontWeight: 'bold', height: 22 }} >
                              {moment(item.installments[0].due_date).format("Do")}
                            </Text>
                          </View>
                          <View style={{ flex: 4, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }} >
                            <Icon name='timer' size={9} color='#FF969FAA' />
                            <Text style={{ color: '#CA020F', fontSize: 9 }} >
                              {(Math.floor((Date.parse(item.installments[0].due_date) - Date.now()) / 86400000))} hari lagi
                        </Text>
                          </View>
                        </View>
                      </View>
                    </View>

                    <View style={{ flex: 6, flexDirection: 'column', alignItems: 'flex-end' }} >
                      <View style={{ flex: 4, flexDirection: 'column', alignItems: 'flex-end', justifyContent: 'center' }} >
                        <Text style={{ flex: 2, fontSize: 11, color: '#969FAA' }} >
                          Jumlah Biaya
                    </Text>
                        <View style={{ flex: 3, flexDirection: 'row', alignItems: 'center' }} >
                          <Text style={{ fontSize: 16, color: '#5A6978' }} >
                            {this.numberWithDots(item.installments[0].amount * item.installments.length)}
                          </Text>
                          <Text style={{ fontSize: 9, color: '#969FAA' }} >
                            / {item.installments.length} bulan
                      </Text>
                        </View>
                      </View>
                      <View style={{ flex: 4, flexDirection: 'column', justifyContent: 'flex-end', alignItems: 'flex-end' }} >
                        <Text style={{ flex: 2, fontSize: 11, color: '#969FAA' }} >
                          Tagihan Bulan Ini
                    </Text>
                        <Text style={{ flex: 3, fontSize: 19, color: '#CB0714' }} >
                          {this.numberWithDots(item.installments[0].amount)}
                        </Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }} >
                      <Icon name='arrow-drop-down' />

                    </View>

                  </View>
                  <View transparent style={{ height: 0 }}>
                  </View>
                </View>
              </TouchableOpacity>
              <View style={{ alignItems: 'center' }}>
                <Button
                  onPress={() => this.redirect(item)}
                  style={{ borderRadius: 25, backgroundColor: '#CA020F', top: -10, height: 18 }}>
                  <Text uppercase={false} style={{ color: '#fff', fontSize: 9 }}>
                    Bayar
                </Text>
                </Button>
              </View>
              <Collapsible collapsed={this.state.arrColl[this.props.arrIndex][index]} collapsedHeight={0} style={{ marginBottom: 0 }} >
                <PaymentList data={item.installments} instIndex={index} arrIndex={this.props.arrIndex} />
              </Collapsible>

            </View>

          )}
        />
      )
    }
    else {
      return (
        <Text>Nothing To See Here</Text>
      )
    }
  }
}


class BiayaPendidikanIndexIsNotEmpty extends Component {

  state = {
    collapsed: this.props.data.map(item => item.collapsed),
    arrColl: this.props.data.map((item, index) => item.installments.map(obj => obj.collapsed)),
    visible: false,
    refreshing: false
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.collapsed.length !== nextProps.data.map(item => item.collapsed).length) {
      return {
        collapsed: nextProps.data.map(item => item.collapsed),
        arrColl: nextProps.data.map((item, index) => item.installments.map(obj => obj.collapsed)),
      }
    }
    return null;
  }

  onRefresh = () => {
    this.setState({ refreshing: true })
    axios.post(TAGIHAN_USER_URI, null, {
      headers: {
        'Authorization': this.props.auth.token
      }
    }).then((response) => {
      getInstallment(this.props.auth.token).then((res) => {
        this.props.setInstallment(res)
      })
      this.setState({ refreshing: false })
    }).catch((err) => {
      console.log(err)
    })
  }


  toggleCollapse = (index) => {
    let temp = this.state.collapsed;
    let tempArr = this.state.arrColl;
    temp[index] = !temp[index];
    this.setState({ collapsed: temp, arrColl: tempArr });
  }

  togglePaymentCollapse = (instIndex, arrIndex) => {
    let temp = this.state.collapsed;
    let tempArr = this.state.arrColl;
    tempArr[arrIndex][instIndex] = !tempArr[arrIndex][instIndex];
    this.setState({ collapsed: temp, arrColl: tempArr });
  }

  numberWithDots = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  listItemContainerStyle = (index) => {
    if (this.state.collapsed[index]) {
      return {
        alignItems: 'center', paddingVertical: 10, backgroundColor: '#fff'
      }
    } else {
      return {
        alignItems: 'center', paddingVertical: 10, backgroundColor: '#F5F5F5'
      }
    }
  }

  togglePengajuan = () => {
    this.setState({ visible: false })
    setTimeout(() => this.props.navigation.navigate("CariInstitusi"), 20);

    console.log(this.state)
  }

  installmentStatus = (item) => {
    if (item.collapsed === true) {
      return (
        <View style={{ flex: 2, flexDirection: 'column', alignItems: 'flex-end' }}>
          <View>
            <Text style={{ fontWeight: 'bold', color: '#CC0611' }} >
              {this.numberWithDots(item.length * item[0].amount)}
            </Text>
          </View>
          <View>
            <Text style={{ fontSize: 11, textDecorationLine: 'underline', color: '#969FAA' }} >
              Details
          </Text>
          </View>
        </View>
      )
    } else {
      return (
        <View style={{ flex: 2, flexDirection: 'column', alignItems: 'flex-end' }}>
          <View>
            <Text style={{ fontWeight: 'bold', color: '#CC0611' }} >
              Being Reviewed
          </Text>
          </View>
        </View>
      )
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar translucent={true} hidden={true} />
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ borderBottomWidth: 1, borderBottomColor: '#95251d', backgroundColor: '#fff' }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" color='#CC0611' style={{}} />
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>Biaya Pendidikan</Title>
          </Body>
          <Right style={styles.headerRight}>
            <Button transparent>
              <Icon name="tune" color='#CC0611' style={{}} />
            </Button>
          </Right>
        </Header>
        <ScrollView refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh}
          />
        }>
          <Content>
            <View>
              <ImageBackground
                source={{ uri: 'https://img.freepik.com/free-photo/card-soft-template-paper-report_1258-167.jpg?size=626&ext=jpg' }}
                style={{ width: '100%', height: devHeight / 7 }}
              >
                <View style={{ flexDirection: 'row', widht: devWidth }}>
                  <View style={{ flex: 1, height: devHeight / 7, justifyContent: 'center', alignItems: 'center' }}>
                    <Icon
                      name="monetization-on" color="#fff" size={52} style={{ width: devWidth / 2, height: devHeight / 7 }}
                    />
                  </View>
                  <View style={{ flex: 1 }}>
                    <View style={{ width: devWidth / 2, height: devHeight / 7, justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{ fontSize: 12, color: '#fff' }}>
                        Sisa tagihan bulan ini
                    </Text>
                      <Text style={{ color: '#fff' }}>
                        {this.props.placeholder.tagihan}

                      </Text>
                    </View>
                  </View>
                </View>
              </ImageBackground>

              <View style={{ borderRadius: 15, width: devWidth + 1, bottom: 15, backgroundColor: 'white' }}>
                <View style={{ flex: 1, alignItems: 'center' }}>
                  <View>
                    <Button transparent style={{ flexDirection: 'column', marginTop: 10, justifyContent: 'center', alignItems: 'center' }}
                      onPress={() => this.props.onPress("CariInstitusi")}>
                      <Icon name="add" size={30} color="#969FAA" />
                      <Text uppercase={false} style={{ color: "#969FAA" }}>
                        Pengajuan
                   </Text>
                    </Button>
                  </View>
                </View>
                <Divider style={{ backgroundColor: '#000', marginVertical: 10 }} />

                <View style={{ alignItems: 'center', maxHeight: 14 }} >
                  <View style={{ flexDirection: 'row', width: '90%', maxHeight: 14 }} >
                    <Text style={{ flex: 7, fontSize: 12, color: '#969FAA' }} >
                      Daftar Cicilan
                  </Text>
                    <Icon name="unfold-more" size={14} color="#969FAA" style={{ flex: 1 }} />
                  </View>
                </View>

                <View transparent style={{ height: 8 }}>
                </View>

                {/* {this.checkInstallmentList()} */}
                <FlatList
                  style={{ flex: 0 }}
                  data={this.props.data}
                  extraData={this.state}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item, index }) => (
                    <TouchableOpacity onPress={() => this.toggleCollapse(index)} disabled={!this.state.collapsed[index]}>
                      <View style={this.listItemContainerStyle(index)} >
                        <View style={{ flex: 1, flexDirection: 'row', width: '90%', marginBottom: 10 }} >
                          <View style={{ flex: 5, flexDirection: 'row' }} >
                            <View style={{ flex: 3, flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center' }}>
                              <View>
                                <Text style={{ fontWeight: 'bold', color: '#5A6978' }} >
                                  {item.fullname}
                                </Text>
                              </View>

                              {this.state.collapsed[index] &&
                                <View>
                                  <View>
                                    <Text style={{ fontSize: 12, color: '#969FAA' }} >
                                      {item.institution}
                                    </Text>
                                  </View>
                                  <View>
                                    <Text style={{ fontSize: 11, fontStyle: 'italic', color: '#969FAA' }} >
                                      Diajukan pada {moment(item.accepted).format("Do MMMM YYYY")}
                                    </Text>
                                  </View>
                                </View>
                              }
                            </View>
                            {this.state.collapsed[index] &&
                              this.installmentStatus(item.installments)
                            }
                          </View>
                        </View>

                        <View>
                          {/* {console.log("collapsed index")}
                          {console.log(this.state.collapsed[index])}
                          {console.log(index)}

                          {console.log("this.state")}
                          {console.log(this.state)}
                          {console.log("this.props.data bp")}
                          {console.log(this.props.data)} */}
                          {/* {console.log("state")} */}
                          {/* {console.log("item")} */}
                          {/* {console.log(item)} */}
                          <Collapsible collapsed={this.state.collapsed[index]} collapsedHeight={0} >
                            <InstallmentList data={[this.props.data[index]]} arrIndex={index} navigation={this.props.navigation} state={this.state} pressFunction={(instIndex, arrIndex) => this.togglePaymentCollapse(instIndex, arrIndex)} onPress={(data) => this.props.onPress(data)} />

                            <View style={{ flex: 1, flexDirection: 'column', width: '100%', justifyContent: 'flex-end', alignItems: 'flex-end' }} >
                              <TouchableOpacity transparent onPress={() => this.toggleCollapse(index)} style={{ flex: 1, height: 14, flexDirection: 'column', justifyContent: 'flex-end', alignItems: 'flex-end' }} >
                                <Text style={{ textDecorationLine: 'underline', color: '#969FAA', fontSize: 10 }} >
                                  Sembunyikan rincian
                              </Text>
                              </TouchableOpacity>
                            </View>

                          </Collapsible>
                        </View>
                      </View>
                    </TouchableOpacity>
                  )}
                />

              </View>
            </View>
          </Content>
        </ScrollView>
        <Dialog
          visible={this.state.visible}
          onTouchOutside={() => {
            this.setState({ visible: false });
          }}
        >
          <DialogContent>
            <View style={{ flexDirection: 'row' }}>
              <Button transparent
                onPressIn={() => this.togglePengajuan()}>
                <Image
                  source={require("../../../../assets/biayapend_clr_2x.png")} />
              </Button>
              <Image source={require("../../../../assets/biayapend_clr_2x.png")} />
            </View>
          </DialogContent>
        </Dialog>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({
    setInstallment
  }, dispatch)
);

const mapStateToProps = (state) => {
  const { installment, auth } = state
  return { installment, auth }
}

export default connect(mapStateToProps, mapDispatchToProps)(BiayaPendidikanIndexIsNotEmpty);