import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Text, Content, Card, CardItem, Footer, List, ListItem } from "native-base";
import { StatusBar, Image, View, ImageBackground, LayoutAnimation, ScrollView, TouchableOpacity, Platform, UIManager, StyleSheet } from "react-native";
import Slideshow from 'react-native-image-slider-show'
import { Icon } from 'react-native-elements'
import styles from './styles'
import { connect } from 'react-redux';
import { getInstallmentsRequest, getPaymentResponse } from "../../../actions";
import { bindActionCreators } from "redux";
import axios from "axios";
import Dialog, { DialogContent } from 'react-native-popup-dialog'

import moment from "moment"
import "moment/locale/id"

const checkA = require("../../../../assets/pop_checkA.png")
const check = require("../../../../assets/checkrnd_clr_3x.png")

class BiayaPendidikanPayment extends Component {
    constructor(props) {
      super(props)
      var data = this.props.navigation.state.params.installmentData;
      console.log(data)

      this.state = {
         profile_pic     : "https://pbs.twimg.com/profile_images/433975350423732224/lLQ5IxLv_400x400.jpeg",
         nth_installment : 6,
         rest_bill       : 0,
         installmentData : data,
         payment : [
            {
              name: "bca",
              logo: require("../../../../assets/bca.png"),
              method: "transfer",
              text: "BCA"
            },
            // {
            //   name: "bca_syariah",
            //   logo: require("../../../../assets/bca_syariah.png"),
            //   method: "transfer",
            //   text: "BCA Syariah"
            // },
            {
              name: "bni",
              logo: require("../../../../assets/bni.png"),
              method: "transfer",
              text: "BNI"
            },
            // {
            //   name: "bni_syariah",
            //   logo: require("../../../../assets/bni_syariah.png"),
            //   method: "transfer",
            //   text: "BNI Syariah"
            // },
            // {
            //   name: "bri",
            //   logo: require("../../../../assets/bri.png"),
            //   method: "transfer",
            //   text: "BRI"
            // },
            // {
            //   name: "bri_syariah",
            //   logo: require("../../../../assets/bri_syariah.png"),
            //   method: "transfer",
            //   text: "BRI Syariah"
            // },
            {
              name: "mandiri",
              logo: require("../../../../assets/mandiri.png"),
              method: "transfer",
              text: "Mandiri"
            },
            // {
            //   name: "mandiri_syariah",
            //   logo: require("../../../../assets/mandiri_syariah.png"),
            //   method: "transfer",
            //   text: "Mandiri Syariah"
            // },
            // {
            //   name: "alfamart",
            //   logo: require("../../../../assets/alfamart.png"),
            //   method: "gerai",
            //   text: "Alfamart"
            // },
            // {
            //   name: "indomaret",
            //   logo: require("../../../../assets/indomaret.png"),
            //   method: "gerai",
            //   text: "Indomaret"
            // }
          ],
         method_selected : "transfer",
         type_selected : "bca",
         text_selected : "BCA",
         isVisible: false,
         payment_data: null,
       }
    }

  setDate(date){
    var parts = date.split('-');
    var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    parts[1] = months[parseInt(parts[1]) - 1 ]
    return parts[0]+" "+parts[1]+" "+parts[2]
  }

  numberWithDots(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  checkState = () => {
    console.log(this.props)
  }

  getSisaTagihan = () => {
    var result = 0;
    for(var k = 0; k < Object.keys(this.state.installmentData.installments).length - 1; k++) {
      if(this.state.installmentData.installments[k.toString()].status === "Unpaid") {
          result += this.state.installmentData.installments[k.toString()].amount
        }
      }
    this.setState({ rest_bill: result})
  }

  methodButton = (method_string, method_state) => {
    if (this.state.method_selected != method_state) {
      return (
        <TouchableOpacity onPress={() => this.setState({ method_selected: method_state, type_selected: null })} style={{ width: "35%", aspectRatio: 5 / 1, justifyContent: "center", backgroundColor: '#FFF', borderRadius: 12, marginRight: "5%", borderColor: "#969FAA", borderWidth: StyleSheet.hairlineWidth}}>
          <Text uppercase={false} style={{ color: "#969FAA", fontSize: 12, textAlign: 'center', fontWeight: "bold" }}>{method_string}</Text>
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity onPress={() => this.setState({ method_selected: null, type_selected: "null" })} style={{ width: "35%", aspectRatio: 5 / 1, justifyContent: "center", shadowColor: 'rgba(0, 0, 0, 0.1)', elevation: 4, shadowOpacity: 0.8, backgroundColor: '#F5F5F5', borderRadius: 12, shadowRadius: 4, marginRight: "5%"}}>
          <Text uppercase={false} style={{ color:'#343F4B', fontSize: 12, textAlign: 'center', fontWeight: "bold" }}>{method_string}</Text>
        </TouchableOpacity>
      )
    }
  }

  typeItem = (array, method) => {
    return array.map((obj) => {
      if (obj.method == method) {
        if (this.state.method_selected == obj.method && this.state.type_selected == obj.name) {
          return (
            <TouchableOpacity onPress={() => {this.setState({type_selected: null, text_selected: "null"})}} style={{ shadowColor: "#CCD2D9", elevation: 4, shadowOpacity: 0.4, shadowRadius: 4, backgroundColor: "white", borderWidth: StyleSheet.hairlineWidth, borderColor: 'black', borderRadius: 15, width: "22%", aspectRatio: 2 / 1, alignItems: "center", justifyContent: "center", marginLeft: "2.5%", paddingBottom: "5%", marginBottom: "2.5%" }}>
              <Image
                style={{ width: "80%", height: "80%", }}
                resizeMode="contain"
                source={obj.logo}
              />
            </TouchableOpacity>
          )
        } else {
          return (
            <TouchableOpacity onPress={() => {this.setState({type_selected: obj.name, text_selected: obj.text})}} style={{ borderRadius: 15, width: "22%", aspectRatio: 2 / 1, alignItems: "center", justifyContent: "center", marginLeft: "2.5%", paddingBottom: "5%", marginBottom: "2.5%" }}>
              <Image
                style={{ width: "80%", height: "80%", }}
                resizeMode="contain"
                source={obj.logo}
              />
            </TouchableOpacity>
          )
        }
      }
    })
  }

  set_isVisible = async () => {
    this.setState({ isVisible: !this.state.isVisible })

    await axios.post("http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/api/v1a/payment/charge", {
      fullname: this.props.profile.data.user.fullname,
      transfer: {
        method: "bank_transfer",
        destination: "bca",
      },
      trf_id: this.state.installmentData.installments[0].id,
      type: "biaya_pendidikan",
      amount: this.state.installmentData.installments[0].amount,
    }, {
      headers: {
        'Authorization': this.props.auth.token
      }
    }).then((response) => {
      console.log(response)

      this.setState({
        payment_data: response.data
      })

    }).catch((error) => {
      console.log(error)
    })

  }

  redirect = async  () => {
    await this.setState({ isVisible: !this.state.isVisible })
    this.props.navigation.navigate("DetailPembayaran", {payment_response: this.state.payment_data})
  }

  capitalize(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  componentDidMount() {
    this.getSisaTagihan()
  }

  render () {
    return (
      <Container>
        <StatusBar translucent={true} hidden={true}/>
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ backgroundColor: '#fff' }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" color="#CC0611" />
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>Pembayaran</Title>
          </Body>
          <Right style={styles.headerRight}>
            <Button transparent>
              <Icon name="tune" color="#CC0611"/>
            </Button>
          </Right>
        </Header>

        <Content style={{ backgroundColor: "#FFFFFF", padding: 15, marginTop: 5}}>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', paddingTop: 10}}>
              <Image
                style={{ tintColor: '#CC0611'}}
                source={require('../../../../assets/biayapend_clr_3x.png')}
              />
            </View>

            <Text style={{ color: "#969FAA", paddingTop: 10, fontSize: 14}}> Dibayar Atas Nama </Text>
            <View style={{ flex: 1, flexDirection: 'row', paddingTop: 10 }}>
              <Image
                style={{ width: 50, height: 50, borderRadius: 25 }}
                source={{ uri: this.state.profile_pic}}
              />
              <View style={{ alignSelf: 'center', paddingLeft: 20}}>
                <Text style={{ color: "#5A6978", fontWeight: "700", fontSize: 16 }}>{ this.props.profile.data.user.fullname }</Text>
                <Text style={{ color: "#969FAA", fontWeight: "400", fontSize: 14 }}>{ this.state.installmentData.institution }</Text>
              </View>
            </View>

            <Card transparent>
              <View>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, marginVertical: 5 }}>
                  <Left>
                    <Text style={{ color: "#969FAA", fontWeight: "400", fontSize: 14 }}>Tagihan ke</Text>
                  </Left>
                  <Text style={{ color: "#969FAA", fontWeight: "400", fontSize: 14, paddingVertical:6 }}>:</Text>
                  <Right style={{ alignItems: "flex-start", marginLeft: "5%" }} >
                    <Text style={{ color: "#5A6978", fontWeight: "700", fontSize: 16 }}>{this.state.installmentData.installments[0].id}</Text>
                  </Right>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, marginVertical: 5 }}>
                  <Left>
                    <Text style={{ color: "#969FAA", fontWeight: "400", fontSize: 14 }}>Jumlah yang harus dibayar</Text>
                  </Left>
                  <Text style={{ color: "#969FAA", fontWeight: "400", fontSize: 14, paddingVertical:6 }}>:</Text>
                  <Right style={{ alignItems: "flex-start", marginLeft: "5%" }} >
                    <Text style={{ color: "#CA020F", fontWeight: "700", fontSize: 19 }}>{ "Rp "+this.numberWithDots(this.state.installmentData.installments[0].amount) }</Text>
                  </Right>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, marginVertical: 5 }}>
                  <Left>
                    <Text style={{ color: "#969FAA", fontWeight: "400", fontSize: 14 }}>Jatuh tempo</Text>
                  </Left>
                  <Text style={{ color: "#969FAA", fontWeight: "400", fontSize: 14, paddingVertical:6 }}>:</Text>
                  <Right style={{ alignItems: "flex-start", marginLeft: "5%" }} >
                    <Text style={{ color: "#5A6978", fontWeight: "700", fontSize: 16 }}>{ moment(this.state.installmentData.installments[0].due_date).format("Do MMMM YYYY") }</Text>
                  </Right>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, marginVertical: 5 }}>
                  <Left>
                    <Text style={{ color: "#969FAA", fontWeight: "400", fontSize: 14  }}>Sisa tagihan</Text>
                  </Left>
                  <Text style={{ color: "#969FAA", fontWeight: "400", fontSize: 14, paddingVertical:6 }}>:</Text>
                  <Right style={{ alignItems: "flex-start", marginLeft: "5%" }} >
                    <Text style={{ color: "#5A6978", fontWeight: "700", fontSize: 14 }}>{ "Rp "+this.numberWithDots(this.state.rest_bill) }</Text>
                  </Right>
                </View>
              </View>
            </Card>

            <Text style={{ fontWeight: "700", color: "#5A6978", fontSize: 14, textAlign: 'center', marginBottom: "5%" }}>Pilih Metode Pembayaran</Text>

            <ScrollView style={{ width: "100%" }} >
              <View style={{ marginHorizontal: "5%", height: "auto"}}>
                <View style={{ flexDirection: 'row', alignItems: "center", marginVertical: "2%" }}>
                    {this.methodButton("Transfer Bank", "transfer")}
                    { this.state.method_selected == "transfer" &&
                      <View style={{ flexDirection: 'row', width: "60%", flexWrap: "wrap" }}>
                        {this.typeItem(this.state.payment, "transfer")}
                      </View>
                    }
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: "2%"}}>
                    {this.methodButton("Virtual Account", "va")}
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: "2%" }}>
                    {this.methodButton("Gerai", "gerai")}
                    { this.state.method_selected == "gerai" &&
                      <View style={{ flexDirection: 'row', width: "60%", flexWrap: "wrap" }}>
                        {this.typeItem(this.state.payment, "gerai")}
                      </View>
                    }
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: "2%" }}>
                    {this.methodButton("Internet Banking", "e-banking")}
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: "2%", marginBottom: "10%" }}>
                    {this.methodButton("Kartu Kredit/Debit", "card")}
                </View>
              </View>
            </ScrollView>

        </Content>
        <View style={{ flexDirection: "column", paddingLeft: 15, paddingRight: 15, backgroundColor: '#fff', marginTop: 5 }}>
          <View style={{ flexDirection: "row", flexWrap: "wrap", padding: 5 }}>
            <Text style={{ fontWeight: "700", color: "#5A6978", fontSize: 14.5, flex: 0.5 }}>Total Pembayaran </Text>
            <Text style={{ fontWeight: "400", color: "#5A6978", fontSize: 14.5, flex: 0.5, textAlign: "right" }}>{ "Rp " + this.numberWithDots(this.state.installmentData.installments[0].amount) } </Text>
          </View>
          <Button rounded style={{ backgroundColor: "#CA020F", alignSelf: 'stretch', justifyContent: 'center', margin: 10 }} onPress={() => this.set_isVisible()}>
            <Text uppercase={false} >{ (this.state.method_selected != null && this.state.type_selected != null) ? "Bayar dengan " + this.capitalize(this.state.method_selected) + " " + this.capitalize(this.state.text_selected) : "Bayar"}</Text>
          </Button>
          {/*<Button
            dark
            block
            rounded
            onPress={() => this.checkState()} style={styles.buttonLogin}
          >
            <Text uppercase={false} style={styles.textLogin}>CheckState</Text>
          </Button>*/}
        </View>
        <Dialog
          visible={this.state.isVisible}
          onTouchOutside={() => this.set_isVisible()}
          width={0.9}
          dialogStyle={{ backgroundColor: 'transparent' }}
        >
          <DialogContent>
            <View style={{ backgroundColor: 'transparent' }}>
              <ImageBackground source={ checkA } style={{ zIndex: 1, justifyContent: 'flex-end', position: 'absolute', alignItems: 'flex-end', width: 150, height: 150, alignSelf: 'center' }} imageStyle={{ borderWidth: 4, borderColor: 'white', borderRadius: 75 }}>
                <Image source={ check } style={{ backgroundColor: 'white', borderRadius: 24, borderWidth: 4, borderColor: 'white' }}/>
              </ImageBackground>
              <View style={{ borderTopLeftRadius:12, borderTopRightRadius:12, marginTop: 80, height:80, backgroundColor: 'white', alignItems: 'center' }}>
              </View>
            </View>
            <View style={{ backgroundColor: 'white', alignItems: 'center', borderBottomLeftRadius:12, borderBottomRightRadius:12 }}>
              <Text style={{ color: '#3ADC00', fontWeight: '700', fontSize: 20 }}>Transaksi Berhasil!</Text>
              <Text style={{ color: '#808080', fontWeight: '700', fontSize: 15, marginTop: 20}}>Transaksi Biaya Pendidikan berhasil,</Text>
              <Text style={{ color: '#808080', fontWeight: '700', fontSize: 15, textAlign: 'center'}}>silakan selesaikan pembayaran anda dalam waktu:</Text>
              <Text style={{ color: '#CA020F', fontSize: 15, marginVertical: 15 }}>23 Jam 59 Menit dari sekarang</Text>
              <Button transparent onPress={() => this.redirect()} style={{ borderTopWidth:0.5, width: '100%', justifyContent: 'center'}}>
                <Text style={{ color: 'black', fontWeight: '700', fontSize: 19, marginVertical: 10 }}>OK</Text>
              </Button>
            </View>
          </DialogContent>
        </Dialog>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const { auth, payment_response, installment,profile } = state
  return { auth, payment_response, installment,profile  }
};

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({
    getPaymentResponse
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(BiayaPendidikanPayment)
