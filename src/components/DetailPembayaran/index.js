import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Text, Content, Card, CardItem, Footer, List, ListItem
} from "native-base";
import { StatusBar, Image, View, ImageBackground, LayoutAnimation, ScrollView, TouchableWithoutFeedback, TouchableOpacity, Platform, UIManager, Dimensions } from "react-native";
import Slideshow from 'react-native-image-slider-show'
import { Icon, SearchBar, Divider } from 'react-native-elements'
import styles from './styles'
import Collapsible from 'react-native-collapsible'
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import { connect } from 'react-redux';
import moment from 'moment'

class DetailPembayaran extends Component {
  constructor(props) {
    super(props)

    console.log("props")
    console.log(this.props)

    var payment_response = this.props.navigation.getParam("payment_response", null)
    var trans_time = moment(payment_response.transaction_time, "YYYY-MM-DD HH:mm:ss");

    console.log(payment_response)
    var method_string = null

    if (payment_response.payment_type == "bank_transfer") {
      method_string = "Transfer Bank "
    }

    if (payment_response.va_numbers[0].bank == "bca") {
      method_string += "BCA"
    }

    this.state = {
      total: Math.floor(parseFloat(payment_response.gross_amount)),
      trans_time: trans_time,
      method: method_string,
      trf_to: payment_response.va_numbers[0].va_number,
      curfew: trans_time,
      due_date: trans_time.add(1, 'days'),
      collapsed_detail: true,
      collapsed_method: [true, true, true],
      time_diff: trans_time.diff(moment(), "seconds")
    }
  }

  secondToString = (second) => {
    var hours = Math.floor(second / (60 * 60))
    var minutes = Math.floor((second % (60 * 60)) / 60)
    var seconds = ((second % (60 * 60)) % 60)
    return (hours + " jam " + minutes + " menit " + seconds + " detik lagi")
  }

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        const due_date = this.state.due_date
        this.setState({
          time_diff: due_date.diff(moment(), "seconds")
        });
      }, 1000)
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
    this.setState({ isVisible: !this.state.isVisible })
  }

  collapse_detail = () => {
    this.setState({
      collapsed_detail: !this.state.collapsed_detail
    })
  }

  collapse_method = (method) => {
    switch (method) {
      case 0:
        this.setState({
          collapsed_method: [!this.state.collapsed_method[0], this.state.collapsed_method[1], this.state.collapsed_method[2]]
        })
        break;
      case 1:
        this.setState({
          collapsed_method: [this.state.collapsed_method[0], !this.state.collapsed_method[1], this.state.collapsed_method[2]]
        })
        break;
      case 2:
        this.setState({
          collapsed_method: [this.state.collapsed_method[0], this.state.collapsed_method[1], !this.state.collapsed_method[2]]
        })
        break;
    }
  }

  navigate_to = (dest) => {
    this.props.navigation.navigate(dest);
  }

  numberWithDots(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  render() {
    return (
      <Container style={{ backgroundColor: 'white' }}>
        <StatusBar translucent={true} hidden={true} />
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ backgroundColor: '#fff' }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" color="#CC0611" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.textBody}>Rincian Pembayaran</Title>
          </Body>
          <Right style={styles.headerRight}>
          </Right>
        </Header>

        <Content style={{ marginHorizontal: 10 }}>
          <Text style={{ color: '#CA020F', fontWeight: "400", fontSize: 14, margin: 10 }}>Tagihan Anda</Text>
          <Card style={{ borderRadius: 12, borderWidth: 5, overflow: 'hidden' }}>
            <CardItem>
              <Text style={{ flex: 0.3, color: '#969FAA', fontSize: 14, fontWeight: "400" }}>Total Pembayaran</Text>
              <View style={{ flex: 0.7 }}>
                <Text style={{ textAlign: 'right', fontWeight: "400", fontSize: 17 }}>{"Rp " + this.numberWithDots(this.state.total)}</Text>
              </View>
            </CardItem>
            <CardItem style={{ flex: 1, alignItems: 'flex-start' }}>
              <Text style={{ flex: 0.3, color: '#969FAA', fontSize: 14, fontWeight: "400" }}>Pembayaran Melalui</Text>
              <View style={{ flex: 0.7 }}>
                <Text style={{ textAlign: 'right', fontWeight: "400", fontSize: 17 }}>{this.state.method}</Text>
                <View style={{ justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center' }}>
                  <Icon name="filter-none" size={14} color="#8492A6" />
                  <Text style={{ textAlign: 'right', fontWeight: "400", fontSize: 14, color: "#CA020F" }}> {this.state.trf_to}</Text>
                </View>
              </View>
            </CardItem>
            <CardItem style={{ alignItems: 'flex-start' }}>
              <Text style={{ flex: 0.3, color: '#969FAA', fontSize: 14, fontWeight: "400" }}>Batas waktu pembayaran</Text>
              <View style={{ flex: 0.7, flexWrap: 'wrap' }}>
                <Text style={{ textAlign: 'right', fontWeight: "400", fontSize: 17 }}>{this.state.curfew.format("DD MMM YYYY - HH:mm") + " WIB"}</Text>
                <Text style={{ textAlign: 'right', fontWeight: "400", fontSize: 13, color: "#CA020F" }}>{this.secondToString(this.state.time_diff)}</Text>
              </View>
            </CardItem>
            {/*<CardItem style={{ alignSelf: 'center' }}>
                      <Button rounded style={{ backgroundColor: "#CA020F", margin: 5 }} onPress={() => this.collapse_detail()}>
                        <Text uppercase={false} style={{ fontWeight: "400", fontSize: 15 }}>Detail</Text>
                      </Button>
                    </CardItem>*/}
            <Collapsible collapsed={this.state.collapsed_detail}>
              <CardItem>
                <Text style={{ flex: 0.4, color: '#969FAA', fontSize: 14, fontWeight: "400" }}>Tagihan untuk</Text>
                <View style={{ flex: 0.6 }}>
                  <Text style={{ fontWeight: "400", fontSize: 17 }}>Biaya Pendidikan</Text>
                </View>
              </CardItem>
              <CardItem>
                <Text style={{ flex: 0.4, color: '#969FAA', fontSize: 14, fontWeight: "400" }}>Tagihan ke</Text>
                <View style={{ flex: 0.6 }}>
                  <Text style={{ fontWeight: "400", fontSize: 17 }}>2</Text>
                </View>
              </CardItem>
              <CardItem>
                <Text style={{ flex: 0.4, color: '#969FAA', fontSize: 14, fontWeight: "400" }}>Jatuh Tempo</Text>
                <View style={{ flex: 0.6 }}>
                  <Text style={{ fontWeight: "400", fontSize: 17 }}>{this.state.due_date.format("DD MMM YYYY - HH.mm") + " WIB"}</Text>
                </View>
              </CardItem>
            </Collapsible>
          </Card>

          <Divider style={{ backgroundColor: 'black', marginVertical: 10 }} />
          <Text style={{ color: '#CA020F', fontWeight: "400", fontSize: 14, marginHorizontal: 10, marginBottom: 5 }}>Tata Cara Pembayaran</Text>
          <Card style={{ borderRadius: 12, borderWidth: 5, overflow: 'hidden' }}>
            <CardItem style={{ flexDirection: 'column', borderWidth: 1, borderColor: '#CCD2D9' }}>
              <TouchableWithoutFeedback onPress={() => this.collapse_method(0)}>
                <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity style={{ flex: 1, alignSelf: 'flex-start' }}>
                    <Text style={{ fontWeight: "400", fontSize: 17 }}>Petunjuk transfer Virtual Account</Text>
                  </TouchableOpacity>
                  <Right >
                    <Text>▼</Text>
                  </Right>
                </View>
              </TouchableWithoutFeedback>
              <Collapsible collapsed={this.state.collapsed_method[0]}>
                <List>
                  <ListItem style={{ flexWrap: 'wrap' }}>
                    <Text>1. Pilih <Text style={{ fontWeight: "bold" }}>Menu Lain > Transfer </Text></Text>
                  </ListItem>
                  <ListItem>
                    <Text>2. Pilih Jenis rekening asal dan pilih <Text style={{ fontWeight: "bold" }}>Virtual Account Billing</Text></Text>
                  </ListItem>
                  <ListItem>
                    <Text>3. Masukkan nomor Virtual Account <Text style={{ color: "#CA020F" }}>{this.state.trf_to}</Text> dan pilih <Text style={{ fontWeight: "bold" }}>Benar</Text></Text>
                  </ListItem>
                  <ListItem>
                    <Text>4. Tagihan yang harus dibayar akan muncul pada layar konfirmasi</Text>
                  </ListItem>
                  <ListItem>
                    <Text>5. Periksa informasi yang tertera di layar. Pastikan <Text style={{ fontWeight: "bold" }}>Merchant</Text> adalah <Text style={{ fontWeight: "bold" }}>Cluever</Text>, dan <Text style={{ fontWeight: "bold" }}>Total Tagihan</Text> sudah benar. Jika benar, pilih <Text style={{ fontWeight: "bold" }}>Ya</Text></Text>
                  </ListItem>
                </List>
              </Collapsible>
            </CardItem>
            {/*<TouchableWithoutFeedback onPress={() => this.collapse_method(1)}>
                      <CardItem style={{ flexDirection: 'column', alignItems: 'flex-start', borderWidth: 1, borderColor: '#CCD2D9' }}>
                        <View style={{ flexDirection: 'row' }}>
                          <TouchableOpacity onPress={() => this.collapse_method(1)} style={{ flex: 1, alignSelf: 'flex-start'}}>
                            <Text style={{ fontWeight: "400", fontSize: 17 }}>Petunjuk transfer SMS</Text>
                          </TouchableOpacity>
                          <Right>
                            <Text>▼</Text>
                          </Right>
                        </View>
                        <Collapsible collapsed={this.state.collapsed_method[1]}>
                          <List>
                            <ListItem style={{ flexWrap: 'wrap'}}>
                              <Text>1. Step 1 asidjasodiasodiwq </Text>
                              <Text style={{ color: "#CA020F" }}>{ this.state.trf_to }</Text>
                              <Text> ifjeufwwe oelorem ipsum sit dolor amet</Text>
                            </ListItem>
                            <ListItem>
                              <Text>2. Step 2 sadasiudh uwhuwh u hu hqow iehw basidjasodiasodiwq ifjeufwwe oelorem ipsum sit dolor amet</Text>
                            </ListItem>
                          </List>
                        </Collapsible>
                      </CardItem>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this.collapse_method(2)}>
                      <CardItem style={{ flexDirection: 'column', alignItems: 'flex-start', borderWidth: 1, borderColor: '#CCD2D9'}}>
                        <View style={{ flexDirection: 'row' }}>
                          <TouchableOpacity onPress={() => this.collapse_method(2)} style={{ flex: 1, alignSelf: 'flex-start'}}>
                            <Text style={{ fontWeight: "400", fontSize: 17 }}>Petunjuk transfer ATM</Text>
                          </TouchableOpacity>
                          <Right>
                            <Text>▼</Text>
                          </Right>
                        </View>
                        <Collapsible collapsed={this.state.collapsed_method[2]}>
                          <List>
                            <ListItem style={{ flexWrap: 'wrap'}}>
                              <Text>1. Step 1 asidjasodiasodiwq </Text>
                              <Text style={{ color: "#CA020F" }}>{ this.state.trf_to }</Text>
                              <Text> ifjeufwwe oelorem ipsum sit dolor amet</Text>
                            </ListItem>
                            <ListItem>
                              <Text>2. Step 2 sadasiudh uwhuwh u hu hqow iehw basidjasodiasodiwq ifjeufwwe oelorem ipsum sit dolor amet</Text>
                            </ListItem>
                            <ListItem>
                              <Text>3. Step 3 sadasiudh uwhuwh u hu hqow iehw basidjasodiasodiwq ifjeufwwe oelorem ipsum sit dolor amet</Text>
                            </ListItem>
                          </List>
                        </Collapsible>
                      </CardItem>
                    </TouchableWithoutFeedback>*/}
          </Card>

        </Content>

        <View style={{ flexDirection: "column", paddingLeft: 15, paddingRight: 15, backgroundColor: '#fff', marginTop: 5 }}>
          <Button rounded style={{ backgroundColor: "#CA020F" , justifyContent: "center", alignSelf: 'center', margin: 10 , width: "90%" }} onPress={() => this.props.navigation.navigate("RiwayatTransaksi")}>
            <Text uppercase={true}>ok</Text>
          </Button>
        </View>
      </Container>
    );
  }

}

const mapStateToProps = (state) => {
  const { payment_response } = state
  return { payment_response }
};

export default connect(mapStateToProps)(DetailPembayaran)