import { color, devWidth } from '../styles.js'
const margin10 = devWidth * 0.1
export default {
  container: {
    backgroundColor: "#fff"
  },
  logo: {
    marginTop: 28,
    marginBottom: 28,
    alignSelf: "center",
    width: 240,
    flexWrap: "wrap"
  },
  topText: {
    marginTop: 28,
    alignSelf: "center",
    color: "#CA020F",
    fontSize: 28,
    fontWeight: "700"
  },
  headerBody: {
    flexDirection: 'row',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textBody: {
    color: color.red,
    textAlign: 'center'
  },
  input: {
    borderRadius: 24,
    borderWidth: 1.5,
    borderColor: '#CCD2D9',
    paddingTop: 3,
    paddingBottom: 3
  },
  inputForm: {
    fontSize: 14,
    fontWeight: '400'
  },
  buttonEnabled: {
    backgroundColor: "#CC0611",
    flex: 0.8
  },
  buttonDisabled: {
    backgroundColor: "#464E51",
    flex: 0.8 
  },
  errorField: {
    color: color.red,
    fontSize: 10,
    textAlign: 'right',
    marginRight: margin10
  },
  inputField: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 12
  },
  headerLeft: {

  }
};
