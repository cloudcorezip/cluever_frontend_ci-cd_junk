import React, { Component } from "react";
import { Image, StatusBar, TextInput } from "react-native";
import { Container, Content, Text, View, Button, Header, Left, Body, Title, Right } from "native-base";
import { Input, Icon } from "react-native-elements";
import Spinner from "react-native-loading-spinner-overlay";
import DropdownAlert from 'react-native-dropdownalert';
import styles from "./styles";
import axios from 'axios';
import modalStyles from "../Modal/styles";
import { color, devHeight } from "../styles";
import Modal from "react-native-modalbox";
import OtpModal from "../Modal/otpModal";
import * as Validation from "../../constants/Validation"

const logo = require("../../../assets/logoApp.png");

export default class Register extends Component {
  constructor() {
    super()

    this.state = {
      form: {
        fullname: null,
        email: null,
        phone_no: null,
        password: null,
        password_confirmation: null
      },
      errors: {
        fullname: false,
        email: false,
        phone_no: false,
        password: false,
        password_confirmation: false
      },
      otpModalShow: false,
      otpInput: ''
    }
  }

  validateInput = (type, text) => {
    switch (type){
      case 'fullname':
        return VALID_FULLNAME_REGEX.test(text)
      case 'email':
        return VALID_EMAIL_REGEX.test(text)
      case 'phone_no':
        return VALID_PHONENO_REGEX.test(text)
      case 'password':
        return VALID_PASSWORD_REGEX.test(text)
      case 'password_confirmation':
        return text === this.state.form.password
    }

  }

  register = () => {
    axios.post("http://ec2-3-0-20-41.ap-southeast-1.compute.amazonaws.com:3000/register",{
      user: this.state
    })
    .then((response) => {
      console.log(response.data.status)
    })
    .catch((error) => {
      console.log(error)
    });
  }

  phoneNumberFocus = () => {
    this.TextInput3.focus()
  }

  handleChange = (key, val) => {
    const form = {
      ...this.state.form,
      [key]: val
    }
    const errors = {
      ...this.state.errors,
      [key]: !this.validateInput(key, val)
    }
    console.log('errors', errors)
    this.setState({ form, errors })
  }

  setOtpModalVisibility = (otpModalShow) => {
    this.setState({ otpModalShow })
  }

  triggerSendOtp = () => {
    this.setState({ otpModalShow: true })
    this.otpModal.sendOtp()
  }

  registerBtnClick = () => {
    let inputValid = true
    let errors = this.state.errors
    const form = this.state.form
    Object.keys(form).map((key) => {
      const isInputValid = !this.validateInput(key, form[key])
      errors[key] = isInputValid
      if (!isInputValid) {
        inputValid = false
      }
    })
    this.setState({ errors, inputValid })
    if (inputValid) {
      this.triggerSendOtp()
    } else {
      alert('not valid')
    }
  }

  isFormEmpty = () => {
    const form = this.state.form
    return !(
      form.email &&
      form.fullname &&
      form.phone_no &&
      form.password &&
      form.password_confirmation
    )
  }

  render() {
    return (
      <Container style={styles.container}>
        <OtpModal
          ref={(component) => this.otpModal = component}
          show={ this.state.otpModalShow }
          handleVisibility={ this.setOtpModalVisibility }
          submitForm={ this.register }
          data={{
            fullname: this.state.fullname,
            phone_no: this.state.phone_no,
            email: this.state.email
          }}
          phoneNumberFocus={ this.phoneNumberFocus }
        />
        <StatusBar hidden={ false }/>
        <Spinner visible={this.props.loading}/>
        <Header
          iosBarStyle={"dark-content"}
          style={{ backgroundColor: color.white, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <Left style={[styles.headerLeft, {flex: 1}]}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" color='#CC0611' />
            </Button>
          </Left>
          <Body style={[ styles.headerBody, { flex: 1 }]}>
            <Title style={styles.textBody}>Daftar</Title>
          </Body>
          <Right style={{ flex: 1 }}>
          </Right>
        </Header>
        <Content>
          <Image
            source={ logo }
            style={ styles.logo }
          />
          {
            this.state.errors.fullname &&
            <Text style={ styles.errorField }>Nama harus 5-30 huruf</Text>
          }
          <View style={ styles.inputField }>
            <Button transparent style={{ flex: 0.8 }}>
              <Input
                placeholder={"Nama Lengkap"}
                containerStyle={ styles.input }
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={ styles.inputForm }
                style={{ fontWeight: "400" }}
                value={ this.state.fullname }
                onChangeText={ (value) => this.handleChange("fullname",value)}
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.TextInput2.focus(); }}
                blurOnSubmit={false}
              />
            </Button>
          </View>
          {
            this.state.errors.email &&
            <Text style={ styles.errorField }>Email tidak valid</Text>
          }
          <View style={ styles.inputField }>
            <Button transparent style={{ flex: 0.8 }}>
              <Input
                ref={(input) => { this.TextInput2 = input }}
                placeholder={ "Email" }
                containerStyle={ styles.input }
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={ styles.inputForm }
                style={{ fontWeight: "400"}}
                value={ this.state.email }
                onChangeText={ (value) => this.handleChange("email",value) }
                keyboardType={ "email-address" }
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.TextInput3.focus(); }}
                blurOnSubmit={false}
              />
            </Button>
          </View>
          {
            this.state.errors.phone_no &&
            <Text style={ styles.errorField }>Nomor HP tidak valid</Text>
          }
          <View style={ styles.inputField }>
            <Button transparent style={{ flex: 0.8, borderWeight: 2.5}}>
              <Input
                ref={(input) => { this.TextInput3 = input }}
                placeholder={ "Nomor HP"}
                containerStyle={ styles.input }
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={ styles.inputForm }
                style={{ fontWeight: "400"}}
                value={ this.state.phone_no }
                onChangeText={ (value) => this.handleChange("phone_no",value) }
                keyboardType={"phone-pad"}
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.TextInput4.focus(); }}
                blurOnSubmit={false}
              />
            </Button>
          </View>
          {
            this.state.errors.password &&
            <Text style={ styles.errorField }>Password paling sedikit 6 karakter</Text>
          }
          <View style={ styles.inputField }>
            <Button transparent style={{ flex: 0.8, borderWeight: 2.5}}>
              <Input
                ref={(input) => { this.TextInput4 = input }}
                placeholder="Password"
                secureTextEntry={true}
                containerStyle={ styles.input }
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={ styles.inputForm }
                style={{ fontWeight: "400"}}
                value={ this.state.password }
                onChangeText={ (value) => this.handleChange("password",value) }
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.TextInput5.focus(); }}
                blurOnSubmit={false}
              />
            </Button>
          </View>
          {
            this.state.errors.password_confirmation &&
            <Text style={ styles.errorField }>Password tidak sama</Text>
          }
          <View style={ styles.inputField }>
            <Button transparent style={{ flex: 0.8, borderWeight: 2.5}}>
              <Input
                ref={(input) => { this.TextInput5 = input }}
                placeholder="Ulangi Password"
                secureTextEntry={true}
                containerStyle={ styles.input }
                inputContainerStyle={{ borderBottomWidth: 0 }}
                inputStyle={ styles.inputForm }
                style={{ fontWeight: "400"}}
                value={ this.state.password_confirmation }
                onChangeText={ (value) => this.handleChange("password_confirmation",value) }
              />
            </Button>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 35 }}>
            <Button
              dark
              block
              rounded
              style={ this.isFormEmpty() ? styles.buttonDisabled : styles.buttonEnabled }
              disabled={ this.isFormEmpty() }
              onPress={ () => this.registerBtnClick() }>
              <Text
                uppercase={ false }
                style={{ fontWeight: 'bold', color: '#F7F5EB', fontSize: 16 }}>
                Buat Akun
              </Text>
            </Button>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center',  marginVertical: 24}}>
            <View style={{ flex: 0.75 , flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontSize: 12, fontWeight: "400", alignItems: 'center' }}>
                Sudah punya akun?
              </Text>
              <Text
                onPress={() => this.props.onPress("Login")}
                style={{ fontSize: 12, fontWeight: "400", color: '#FF7636', marginLeft: 2, textDecorationLine: 'underline' }}
              >
                Masuk di sini
              </Text>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
