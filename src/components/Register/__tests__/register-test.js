import React from "react";
import renderer from "react-test-renderer";

import Register from "../index";


test('register render correctly', () => {
  const registerTes = renderer.create(<Register />).toJSON();
  expect(registerTes).toMatchSnapshot();
})