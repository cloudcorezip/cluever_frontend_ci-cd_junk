import { color } from '../styles.js';

export default {
  container: {
    backgroundColor: "#fff"
  },
  logo: {
    marginTop: 104,
    alignSelf: "center",
    width: 240
  },
  textLogo: {
    marginTop: 18,
    alignSelf: "center",
    fontSize: 16
  },
  containerForm: {
    flex: 1,
    marginLeft: 32,
    marginRight: 32
  },
  contentForm: {
    marginTop: 46
  },
  buttonLogin: {
    marginTop: 24,
    backgroundColor: "#CC0611"
  },
  textLogin: {
    fontWeight: 'bold',
    color: '#F7F5EB',
    fontSize: 16
  },
  containerEnv: {
    alignItems: "center",
    marginTop: 20
  },
  textChangeEnv: {
    marginTop: 5
  },
  input: {
    borderRadius: 22,
    borderWidth: 1.5,
    borderColor: '#CCD2D9',
    paddingVertical: 3
  },
  inputError: {
    borderRadius: 22,
    borderWidth: 1,
    borderColor: '#CA020F',
    paddingTop: 3,
    paddingBottom: 3
  },
  inputForm: {
    fontSize: 12,
    fontWeight: '400',
    marginVertical: 12
  },
  inputFormError: {
    marginTop: -10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
};
