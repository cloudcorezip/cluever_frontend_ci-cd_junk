import React, { Component } from "react";
import { Image, StatusBar, Platform, UIManager, ImageBackground, TouchableOpacity, Dimensions } from "react-native"
import { CheckBox, Input, Overlay, Divider, Icon } from "react-native-elements"
import { Container, Content, Button, Text, View, Spinner } from "native-base"
import styles from "./styles"
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import axios from 'axios';
import { UserLoginRequest, setUserToken } from "../../actions"
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { color } from '../styles.js';
import { createNewToken } from '../../utils/AuthUtils.js'
import * as NetInfo from "@react-native-community/netinfo"

const logo = require("../../../assets/logoApp.png")
const cryface = require("../../../assets/cryface.png")
const check = require("../../../assets/checkrnd_clr_3x.png")
const cross = require("../../../assets/closernd_clr_3x.png")
const thanks = require("../../../assets/tunggu_verif_3x.png")
const checkA = require("../../../assets/pop_checkA.png")
const crossA = require("../../../assets/pop_crossA.png")
const thanksA = require("../../../assets/pop_thanks.png")

var devWidth = Dimensions.get('window').width;

class Login extends Component {

  constructor(props) {
    super(props)

    var conn = false
    const subscription = undefined

    NetInfo.isConnected.fetch().then(isConnected => {
      console.log("Is connected", isConnected);
      conn = isConnected
    });

    this.state = {
       isConnected: conn,
       isVisible: [false,false,false,false],
       phone_no: null,
       password: null,
       form_valid: ["initial","initial"],
       req_error: "",
       isLoading: false
     }

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    if (this.props.auth.token != undefined) {
      this.props.navigation.navigate("Home")
    }
  }

  componentDidMount() {
    if (this.props.auth.token != undefined) {
      this.props.navigation.navigate("Home")
    } else {
      subscription = NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }
  }

  componentWillUnmount() {
    // NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    subscription.remove()
  }

  handleConnectivityChange = (isConnected) => {
    console.log("Is connected", isConnected);
      conn = isConnected
    this.setState({
      isConnected: isConnected
    })
  }

  login = async () => {

    if (this.state.isConnected) {
      
      this.setState({
        isLoading: true
      })
      const newTokenAsync = await createNewToken(this.state.phone_no, this.state.password)

      if (newTokenAsync.hasOwnProperty("data")) {
        console.log("async")
        console.log(newTokenAsync)

        await this.props.setUserToken(newTokenAsync.data.auth_token)
        await this.setState({
          isLoading: false
        })
        this.props.navigation.navigate("Home")
      } else {
        this.setState({
          isLoading: false
        })
        console.log("error")
        console.log(newTokenAsync)
        if (newTokenAsync.request.status == 401) {
          this.setState({
            req_error: "Nomor HP atau password tidak ditemukan"
          })
        }
      }  
    }
  }

  checkState = () => {
    console.log(this.props)
  }

  set_isVisible = (index) => {
    let isVisible_ = [...this.state.isVisible]
    isVisible_[index] = !isVisible_[index]
    this.setState({ isVisible: isVisible_ })
  }

  handleChange = (key,val) => {
    this.setState({ [key]: val })
  }

  renderError = (message) => {
    return (
      <View style={ styles.inputFormError }>
        <Icon name="error" size={14} color="#CA020F" />
        <Text style={{ fontSize: 14, color:"#CA020F" }}>
          message
        </Text>
      </View>
    )
  }

  checkFormInvalid = (n,valtrue,valfalse) => {
    return (this.state.form_valid[n] == "" ? valtrue : valfalse )
  }

  isFormEmpty = () => {
    return !(this.state.phone_no && this.state.password && this.state.isConnected)
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar hidden={true}/>
        <Content>
          { !this.state.isConnected &&
            <View style={{ width: "100%", aspectRatio: 10 / 1, backgroundColor: "#CA020F", justifyContent: "center", alignItems: "center", marginBottom: - devWidth / 10  }} >
              <Text style={{ color: "white", fontSize: 14, textAlign: "center" }} >
                Tidak ada koneksi internet. Silahkan cek koneksi Anda
              </Text>
            </View>
          }
          <Image source={logo} style={styles.logo}/>
          <View style={styles.containerForm}>
            <View style={styles.contentForm}>
              <View style={ styles.inputForm }>
                
                  <Input
                    placeholder="Nomor HP"
                    placeholderTextColor={ this.checkFormInvalid(0,"#CA020F","") }
                    containerStyle={  this.checkFormInvalid(0,styles.inputError, styles.input) }
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                    inputStyle={{ fontSize: 16}, this.checkFormInvalid(0, {color: "#CA020F"},"") }
                    style={{ fontWeight: 400}}
                    onChangeText={ (value) => this.handleChange("phone_no",value)}
                    returnKeyType = { "next" }
                    onSubmitEditing={() => { this.TextInput2.focus() }}
                    blurOnSubmit={false}
                  />
                
              </View>
              { this.state.phone_no == "" &&
                <View style={ styles.inputFormError }>
                  <Icon name="error" size={14} color="#CA020F" />
                  <Text style={{ fontSize: 14, color:"#CA020F" }}>
                    Nomor HP tidak boleh kosong
                  </Text>
                </View>
              }
              <View style={ styles.inputForm }>
                
                  <Input
                    ref={(input) => { this.TextInput2 = input }}
                    placeholder="Password"
                    placeholderTextColor={ this.checkFormInvalid(1,"#CA020F","") }
                    secureTextEntry={true}
                    containerStyle={  this.checkFormInvalid(1, styles.inputError, styles.input) }
                    inputContainerStyle={{ borderBottomWidth: 0 }}
                    inputStyle={{ fontSize: 16 }, this.checkFormInvalid(1, {color: "#CA020F"},"") }
                    style={{ fontWeight: 400}}
                    onChangeText={ (value) => this.handleChange("password",value)}
                  />
                
              </View>
              { this.state.password == "" &&
                <View style={ styles.inputFormError }>
                  <Icon name="error" size={14} color="#CA020F" />
                  <Text style={{ fontSize: 14, color:"#CA020F" }}>
                    Password tidak boleh kosong
                  </Text>
                </View>
              }
              { this.state.req_error != "" &&
                this.state.password != "" &&
                <View style={ styles.inputFormError }>
                  <Icon name="error" size={14} color="#CA020F" />
                  <Text style={{ fontSize: 14, color:"#CA020F" }}>
                    {this.state.req_error}
                  </Text>
                </View>
              }
              <Button
                dark
                block
                rounded
                onPress={() => this.login()}
                style={[styles.buttonLogin, (this.isFormEmpty()) ? { backgroundColor: color.button.disabled } : {} ]}
                disabled={ this.isFormEmpty() }
              >
                { !this.state.isLoading &&
                  <Text uppercase={false} style={ styles.textLogin }>Masuk</Text>
                }
                { this.state.isLoading &&
                    <Spinner color="white" style={{  }} />
                }
              </Button>
              <View style={{ flexDirection: 'row', marginTop: 20, alignSelf: 'flex-start', justifyContent: 'center'}}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ fontSize: 14, fontWeight: "400", color: '#3B4045' }}>
                    Belum punya akun?
                  </Text>
                  <TouchableOpacity onPress={() => this.props.onPress("Register")}>
                    <Text uppercase={false} style={{ fontSize: 14, fontWeight: "400", color: '#FF7636', marginLeft: 2, textDecorationLine: 'underline' }}>
                      Daftar di sini
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>

              <Text onPress={() => this.props.onPress("Forgetpass")}
                style={{ marginTop: 12, fontSize: 14, fontWeight: "400", color: '#FF7636', textAlign: 'center', textDecorationLine: 'underline', alignSelf: 'flex-start', }}>
                Lupa password?
              </Text>
            </View>
          </View>
        </Content>
        <Dialog
          visible={this.state.isVisible[0]}
          onTouchOutside={() => this.set_isVisible(0)}
          width={0.9}
          dialogStyle={{ backgroundColor: 'transparent' }}
        >
          <DialogContent>
            <View style={{ backgroundColor: 'transparent' }}>
              <ImageBackground source={ checkA } style={{ zIndex: 1, justifyContent: 'flex-end', position: 'absolute', alignItems: 'flex-end', width: 150, height: 150, alignSelf: 'center' }} imageStyle={{ borderWidth: 4, borderColor: 'white', borderRadius: 75 }}>
                <Image source={ check } style={{ backgroundColor: 'white', borderRadius: 24, borderWidth: 4, borderColor: 'white' }}/>
              </ImageBackground>
              <View style={{ borderTopLeftRadius:12, borderTopRightRadius:12, marginTop: 80, height:80, backgroundColor: 'white', alignItems: 'center' }}>
              </View>
            </View>
            <View style={{ backgroundColor: 'white', alignItems: 'center', borderBottomLeftRadius:12, borderBottomRightRadius:12 }}>
              <Text style={{ color: '#3ADC00', fontWeight: '700', fontSize: 20 }}>Transaksi Berhasil!</Text>
              <Text style={{ color: '#808080', fontWeight: '700', fontSize: 15, marginTop: 20}}>Transaksi Biaya Pendidikan berhasil,</Text>
              <Text style={{ color: '#808080', fontWeight: '700', fontSize: 15, textAlign: 'center'}}>silakan selesaikan pembayaran anda dalam waktu:</Text>
              <Text style={{ color: '#CA020F', fontSize: 15, marginVertical: 15 }}>23 Jam 59 Menit dari sekarang</Text>
              <Button transparent onPress={() => this.set_isVisible(0)} style={{ borderTopWidth:0.5, width: '100%', justifyContent: 'center'}}>
                <Text style={{ color: 'black', fontWeight: '700', fontSize: 19, marginVertical: 10 }}>OK</Text>
              </Button>
            </View>
          </DialogContent>
        </Dialog>

        <Dialog
          visible={this.state.isVisible[1]}
          onTouchOutside={() => this.set_isVisible(1)}
          width={0.9}
          dialogStyle={{ backgroundColor: 'transparent' }}
        >
          <DialogContent>
            <View style={{ backgroundColor: 'transparent' }}>
              <ImageBackground source={ checkA } style={{ zIndex: 1, justifyContent: 'flex-end', position: 'absolute', alignItems: 'flex-end', width: 150, height: 150, alignSelf: 'center' }} imageStyle={{ borderWidth: 4, borderColor: 'white', borderRadius: 75 }}>
                <Image source={ check } style={{ backgroundColor: 'white', borderRadius: 24, borderWidth: 4, borderColor: 'white' }}/>
              </ImageBackground>
              <View style={{ borderTopLeftRadius:12, borderTopRightRadius:12, marginTop: 80, height:80, backgroundColor: 'white', alignItems: 'center' }}>
              </View>
            </View>
            <View style={{ backgroundColor: 'white', borderBottomLeftRadius:12, borderBottomRightRadius:12 }}>
              <Text style={{ color: '#3ADC00', fontWeight: '700', fontSize: 20, textAlign: 'center' }}>Transaksi Berhasil!</Text>
              <View style={{ flexDirection: 'row', padding: 10, flexWrap: 'wrap', justifyContent: 'center' }}>
                <Text style={{ fontWeight: '400', fontSize: 17, textAlign: 'center'}}>
                  Selamat, pembayaran dengan nomor
                  <Text style={{ fontWeight: '700', fontSize: 17, textAlign: 'center'}}> 0800x atas nama Aulia </Text>
                telah berhasil pada </Text>
              </View>
                <Text style={{ fontSize: 13, textAlign: 'center' }}>12 Juli 2019, 19.00 WIB</Text>
              <Button transparent onPress={() => this.set_isVisible(1)} style={{ borderTopWidth:0.5, width: '100%', justifyContent: 'center', marginTop: 10 }}>
                <Text style={{ color: 'black', fontWeight: '700', fontSize: 19, marginVertical: 10 }}>OK</Text>
              </Button>
            </View>
          </DialogContent>
        </Dialog>

        <Dialog
          visible={this.state.isVisible[3]}
          onTouchOutside={() => this.set_isVisible(3)}
          width={0.9}
          dialogStyle={{ backgroundColor: 'transparent' }}
        >
          <DialogContent>
            <View style={{ backgroundColor: 'transparent' }}>
              <ImageBackground source={ crossA } style={{ zIndex: 1, justifyContent: 'flex-end', position: 'absolute', alignItems: 'flex-end', width: 150, height: 150, alignSelf: 'center' }} imageStyle={{ borderWidth: 4, borderColor: 'white', borderRadius: 75 }}>
                <Image source={ cross } style={{ backgroundColor: 'white', borderRadius: 24, borderWidth: 4, borderColor: 'white' }}/>
              </ImageBackground>
              <View style={{ borderTopLeftRadius:12, borderTopRightRadius:12, marginTop: 80, height:80, backgroundColor: 'white', alignItems: 'center' }}>
              </View>
            </View>
            <View style={{ backgroundColor: 'white', borderBottomLeftRadius:12, borderBottomRightRadius:12 }}>
              <Text style={{ color: '#CA020F', fontWeight: '700', fontSize: 20, textAlign: 'center' }}>Transaksi Gagal</Text>
              <View style={{ flexDirection: 'row', padding: 10, flexWrap: 'wrap', justifyContent: 'center' }}>
                <Text style={{ fontWeight: '400', fontSize: 17, marginHorizontal: 10, textAlign: 'center'}}>
                  Transaksi gagal, mohon ulangi transaksi
                </Text>
              </View>
              <Button transparent onPress={() => this.set_isVisible(3)} style={{ borderTopWidth:0.5, width: '100%', justifyContent: 'center', marginTop: 10 }}>
                <Text style={{ color: 'black', fontWeight: '700', fontSize: 19, marginVertical: 10 }}>Kembali</Text>
              </Button>
            </View>
          </DialogContent>
        </Dialog>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const { auth } = state;
  return { auth };
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    UserLoginRequest,
    setUserToken,
  }, dispatch)
);

export default connect(mapStateToProps ,mapDispatchToProps)(Login);
