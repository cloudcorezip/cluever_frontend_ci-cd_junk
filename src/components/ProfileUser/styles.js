export default {
  headerLeft: {
    flex: 0.3
  },
  headerBody: {
    flex: 0.4
  },
  textBody:{
    alignSelf: "center",
    color: "#CC0611",
    fontWeight: "bold"
  },
  headerRight: {
    flex: 0.3
  }
};
