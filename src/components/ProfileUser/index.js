import React, { Component } from "react";
import {
  Container,
  Header,
  Left,
  Button,
  Body,
  Title,
  Right,
  Footer,
  Text,
  FooterTab,
  Content,
} from "native-base";
import { StatusBar, TouchableOpacity, Dimensions, ImageBackground, Image, View } from "react-native";
import { Icon, Divider } from "react-native-elements";
import { connect } from "react-redux";
import { bindActionCreators } from "redux"

import styles from "./styles";
import { userLogoutRequest } from "../../actions"

var imgBanner = require("../../../assets/banner_profil_user.png");
var imgUser = require("../../../assets/user_img.png");

var screenHeight = Dimensions.get('window').height;
var flexPercent = 200 / screenHeight;
var screenWidth = Dimensions.get('window').width;


class ProfileUser extends Component {

  userLogout = () => {
    this.props.userLogoutRequest();
    this.props.navigation.navigate("Login")
  }

  constructor(){
    super()
    
    this.state = {
      banner: imgBanner,
      userProfilePhoto: imgUser
    }
  }

  render() {
    return (
      <Container style={{ backgoundColor: "#fff" }}>
        <StatusBar hidden={true} />
        <Header noShadow iosBarStyle={"dark-content"} style={{ backgroundColor: "#fff" }}>
          <Left style={styles.headerLeft} />
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>Profil Saya</Title>
          </Body>
          <Right style={styles.headerRight}>
            <Button transparent>
              <Icon name="notifications" color="#CC0611" />
            </Button>
          </Right>
        </Header>
        <Content>
          <View>
            <ImageBackground source={this.state.banner} style={{ height: 150, flexDirection: "column", justifyContent: "flex-end", alignItems: "center" }}>
              <View>
                <Text style={{ color: "#fff", fontSize: 18, fontWeight: "700", textAlign: "center", marginLeft: screenWidth/18, marginBottom: 10 }}>
                  {this.props.profile.data.user.fullname}
                </Text>
              </View>
            </ImageBackground>
            <Image source={this.state.userProfilePhoto} style={{ marginLeft: screenWidth / 18, marginTop: screenHeight/9, height: 120, width: 120, borderRadius: 100, position: "absolute" }} />
            <Text style={{ marginTop: 10, marginLeft: screenWidth/9, textAlign: "center", fontWeight: "700", fontSize: 18 }}>
              {this.props.profile.data.user.phone_no}
            </Text>
          </View>
          {/* TODO Mapping Menu List  */}
          <View style={{ flex: flexPercent }}>
            <View style={{ flexDirection: "column", justifyContent: "space-around", marginHorizontal: 25, marginVertical: 60 }}>
              <TouchableOpacity>
                <Text>Ubah profil</Text>
              </TouchableOpacity>
              <Divider style={{ marginVertical: 10 }} />
              <TouchableOpacity>
                <Text>Ganti Password</Text>
              </TouchableOpacity>
              <Divider style={{ marginVertical: 10 }} />
              <TouchableOpacity>
                <Text>Pengaturan akun</Text>
              </TouchableOpacity>
              <Divider style={{ marginVertical: 10 }} />
              <TouchableOpacity>
                <Text>Kustomisasi fitur</Text>
              </TouchableOpacity>
              <Divider style={{ marginVertical: 10 }} />
              <TouchableOpacity>
                <Text>FAQ</Text>
              </TouchableOpacity>
              <Divider style={{ marginVertical: 10 }} />
              <TouchableOpacity>
                <Text>Contact Us</Text>
              </TouchableOpacity>
              <Divider style={{ marginVertical: 10 }} />
              <TouchableOpacity>
                <Text>Term & Privacy Policy</Text>
              </TouchableOpacity>
              <Divider style={{ marginVertical: 10 }} />
              <TouchableOpacity>
                <Text>Apps Info </Text>
              </TouchableOpacity>
              <Divider style={{ marginVertical: 10 }} />
              <TouchableOpacity button onPress={() => this.userLogout()}>
                <Text style={{ color: "#CA020F" }}>Logout</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Content>
        <Footer style={{ backgroundColor: "transparent" }}>
          <FooterTab style={{ backgroundColor: "transparent" }}>
            <Button onPress={() => this.props.onPress("Home")}>
              <View style={{}}>
                <Icon name="home" color="gray" style={{}} />
              </View>
            </Button>
            <Button onPress={() => this.props.onPress("RiwayatTransaksi")}>
              <Icon name="attach-money" color="gray" />
            </Button>
            <Button onPress={() => this.props.onPress("Feeds")}>
              <Icon type="font-awesome" name="newspaper-o" color="gray" />
            </Button>
            <Button>
              <Icon name="person" color="#CA020F" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  const { profile } = state
  return { profile }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    userLogoutRequest,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUser);