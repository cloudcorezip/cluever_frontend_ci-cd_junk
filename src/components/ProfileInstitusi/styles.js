export default {
  container: {
    width: "100%",
    backgroundColor: "#fff"
  },
  headerLeft: {
    flex: 0.3
  },
  headerBody: {
    flex: 0.4
  },
  textBody:{
    alignSelf: "center",
    color: "#CB0714",
    fontWeight: "700"
  },
  headerRight: {
    flex: 0.3
  },
  fullWidthAbsolute: {
    position: 'absolute',
    top: 0,
    left: 8,
    right: 8,
    bottom: 0
  },
  transparent: {
    opacity: 0.6
  },
  darkRed: {
    // backgroundColor: '#95251d'
    backgroundColor: '#000'
  },
  rounded: {
    borderRadius: 8
  },
  padding: {
    padding: 8
  },
  cardTitleContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  cardTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#fff'
  },
  cardImage: {
    height: 120,
    width: null,
    flex: 1
  }
};
