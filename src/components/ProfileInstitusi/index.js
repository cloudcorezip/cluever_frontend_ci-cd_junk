import React, { Component } from "react";
import { Header, Left, Container, Button, Body, Title, Right, Text, Content, Card, CardItem, List, ListItem, Footer, FooterTab } from "native-base";
import { StatusBar, Image, View, ImageBackground, Dimensions, FlatList, TouchableOpacity, ScrollView } from "react-native";
import { Icon, Divider } from "react-native-elements";
import Slideshow from 'react-native-image-slider-show';
import  Axios from "axios"; 
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import styles from './styles'

var devHeight = Dimensions.get('window').height;
var devWidth = Dimensions.get('window').width;

var imgBanner = require("../../../assets/banner_edulab_1080x469.png");
var logo1 = require("../../../assets/edulab_logo.png");

const jsonFile = require("../../../public/edu.json")

export default class ProfileInstitusi extends Component {

	componentDidMount() {
		Axios.get('https://api.myjson.com/bins/aswcb')
		.then((response) => {
			console.log(response)
		}).catch((err) => {
			console.log(err)
		})
		console.log(this.props.navigation.state)
	}

	constructor(props) {
		super(props);

		this.state = {
			position: 0,
			// dataSource: [
				// { url: imgBanner },
				// { url: 'https://live.staticflickr.com/3810/9606789399_02a0579525_n.jpg' },
				// { url:	 'https://cdn.pixabay.com/photo/2017/01/12/16/19/kit-1975078_960_720.png' }
			// ],
			banner: imgBanner,
			bannerLogo1: logo1,
			value: 0,
		}
	}

	// componentWillMount() {
	// 	this.setState({
	// 		interval: setInterval(() => {
	// 			this.setState({
	// 				position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
	// 			});
	// 		}, 2000)
	// 	});

	// 	console.log(this.state);
	// }

	componentWillUnmount() {
		clearInterval(this.state.interval);
	}

	render() {

		var radio_props = [
			{
				label: 'Suite Class',
				subtitle: "Program pembelaran regular untuk siswa SMA",
				price: "30.000.000",
				value: 0
			},
			{
				label: 'Flexi Max',
				subtitle: "Program pembelajaran yang mempunyai kebebasan untuk memilih mata pelajaran dan waktu belajar untuk persiapan UTBK SBMPTN ",
				price: "25.000.000",
				value: 1
			},
			{
				label: 'Deluxe Class',
				subtitle: "Program pembelajaran regular untuk siswa SMP dan SMA",
				price: "15.000.000",
				value: 2
			},
			{
				label: 'Velvet Class',
				subtitle: "Program pembelajaran regular khusus SMP kelas 3 untuk fokus mengajar SMA favorit",
				price: "9.000.000",
				value: 3
			}
		];

		return (
			<Container style={styles.container}>
				<StatusBar translucent={true} hidden={true} />
				<Header
					noShadow
					iosBarStyle={"dark-content"}
					style={{ borderBottomWidth: 1, borderBottomColor: '#95251d', backgroundColor: '#fff' }}>
					<Left style={styles.headerLeft}>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Icon name="arrow-back" color='#CC0611' style={{}} />
						</Button>
					</Left>
					<Body style={styles.headerBody}>
						<Title style={styles.textBody}>Profile Institusi</Title>
					</Body>
					<Right style={styles.headerRight}>
						<Button transparent>
							<Icon name="tune" color='#CC0611' style={{}} />
						</Button>
					</Right>
				</Header>
				<Content>
					<ScrollView contentContainerStyle={{ alignItems: 'center' }} >
						<ImageBackground
							source={this.state.banner}
							style={{ width: '100%', height: devHeight / 4 }}
							>
						<View style={{ flexDirection: 'row', widht: devWidth }}>
							<View style={{ flex: 1, height: devHeight / 4, justifyContent: 'center', alignItems: 'center' }}>
								<Image source={this.state.bannerLogo1} style={{width: "50%", height: "50%"}}/>
							</View>
							<View style={{ flex: 1 }}>
								<View style={{ width: devWidth , height: devHeight / 4, justifyContent: 'center' }}>
									<Text style={{ fontSize: 12, color: '#fff' }}>
										Jl.Kalimantan No.7, Bandung
									</Text>
								</View>
							</View>
						</View>
						</ImageBackground>
						{/* <View>
							<Slideshow
								dataSource={this.state.dataSource}
								position={this.state.position}
								onPositionChanged={position => this.setState({ position })}
								arrowSize={0}
							/>
							
						</View> */}

						<View style={{ width: "90%", marginTop: 10 }} >
							<Text style={{ fontSize: 19, fontWeight: 'bold', fontStyle: 'italic', color: '#5A6978' }}>
								Tentang Edulab
			            	</Text>
							<Text style={{ fontSize: 12, color: '#5A6978', marginTop: 3 }}>
								Edulab bergerak sebagai lembaga konsultan pendidikan yang tidak hanya melihat potensi siswa secara seragam,
								tetapi juga melihat potensi siswa secara individu agar dapat memberikan pelayanan personal sehingga setiap siswa dapat mencapai potensi maksimal masing-masing.
								Edulab memaknai pendidikan dengan mengenalkan konsep-konsep utama dalam memaknai suatu pembelajaran, sehingga membuat pendidikan menjadi hal yang adiktif, menarik, dan menyenangkan.
			            	Metode yang diusung pun lebih fleksibel dan variatif menghindari kesan kaku yang selama ini hadir dalam pendidikan yang bersifat formal.{"\n"}
								{"\n"}
								Visi dan Misi Edulab adalah “Menjadi konsultan pendidikan terbesar dan terbaik di Indonesia, yang memberikan pelayanan personal untuk mencapai masa depan pendidikan berkualitas”.
								Sesaat setelah daftar, edulab langsung mencari tahu minat, bakat & potensimu untuk memilih jurusan kuliah yang akurat.

			            	</Text>
						</View>
						<View style={{ width: "90%", marginTop: 10 }}>
							<Text style={{ fontSize: 19, fontWeight: 'bold', fontStyle: 'italic', color: '#5A6978', marginBottom: 10 }}>
								Program Belajar
			      </Text>
							<RadioForm
							>
								{radio_props.map((obj, i) => {
									return (
										<RadioButton labelHorizontal={true} key={i} style={{ justifyContent: 'space-between', marginBottom: 10, flexDirection: 'column' }} >
											<TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => { this.setState({ value: i }) }} >
												<View style={{ width: "80%", alignItems: "flex-start" }} >
													<RadioButtonLabel
														obj={obj}
														index={i}
														labelHorizontal={true}
														labelStyle={{ fontSize: 16, color: '#CA020F' }}
														labelWrapStyle={{ width: "100%", alignItems: "flex-start" }}
													/>
													<Text style={{ color: "#5A6978", fontSize: 14, paddingLeft: 10 }} >
														{obj.subtitle}
													</Text>
													<Text style={{ color: "#5A6978", fontSize: 21, fontWeight: "bold", paddingLeft: 10 }} >
														{"Rp "} {obj.price}
													</Text>
												</View>
												<RadioButtonInput
													obj={obj}
													index={i}
													isSelected={this.state.value == i}
													onPress={(i) => { this.setState({ value: i }) }}
													borderWidth={2}
													buttonInnerColor={'#CA020F'}
													buttonOuterColor={"#8492A6"}
													buttonSize={9}
													buttonOuterSize={18}
													buttonStyle={{}}
													buttonWrapStyle={{ width: "20%", justifyContent: 'center' }}
												/>
											</TouchableOpacity>
											<Divider style={{ marginTop: 10 }} />
										</RadioButton>
									);
								})}
							</RadioForm>
						</View>
					</ScrollView>
				</Content>
				<View style={{ alignItems: 'center' }} >
					<Footer style={{ backgroundColor: 'transparent', width: "90%", alignItems: 'center', paddingBottom: 10 }} >
						<FooterTab style={{ backgroundColor: 'transparent', width: "90%" }} >
							<Button style={{ backgroundColor: '#CA020F', width: "90%", height: "90%", borderRadius: 25 }} onPress={() => this.props.onPress("FormPengajuan")} >
								<Text style={{ color: '#FFF', fontSize: 14, height: "100%", textAlignVertical: 'center', paddingTop: 5 }} uppercase={false} >
									Isi Data Pengajuan
			          </Text>
							</Button>
						</FooterTab>
					</Footer>
				</View>
			</Container>
		);
	}
}
