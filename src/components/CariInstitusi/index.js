import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Text, Content
} from "native-base";
import { StatusBar, View, ScrollView, Dimensions, FlatList, } from "react-native";
import Slideshow from 'react-native-image-slider-show';
import ScrollableTabView, { ScrollableTabBar } from "react-native-scrollable-tab-view";
import { Icon, SearchBar } from 'react-native-elements';
import Collapsible from 'react-native-collapsible';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import styles from './styles';
import { devWidth, devHeight } from '../styles';

class CategoryCollapsible extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapsed: this.props.collapsed,
      data: this.props.data,
      isSelected: this.props.isSelected
    };

    this.type_name = ""
    if (this.props.type == "sekolah") {
      this.type_name = "Sekolah"
    } else if (this.props.type == "bimbel") {
      this.type_name = "Bimbel"
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      collapsed: nextProps.collapsed,
      data: nextProps.data,
      isSelected: nextProps.isSelected
    });
  }

  isGeometricCollapsed() {
    if (this.state.collapsed == true) {
      return (
        <Text style={{ color: '#5A6978' }}>▼</Text>
      )
    }
    else {
      return (
        <Text style={{ color: '#5A6978' }}>▲</Text>
      )
    }
  }

  render() {
    return (
      <View>
        <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => (
            <RadioButton labelHorizontal={true} key={item.value} style={{ alignItems: "center", marginVertical: devHeight/80, marginLeft: devWidth/20  }}>
              <RadioButtonInput
                obj={item}
                index={item.value}
                isSelected={this.state.isSelected === item.value}
                onPress={() => this.props.radioSelectFunction(item.value)}
                borderWidth={1}
                buttonInnerColor={'#FFBA5C'}
                buttonOuterColor={"#8492A6"}
                buttonSize={devWidth / 75}
                buttonOuterSize={devWidth / 30}
                buttonStyle={{}}
                buttonWrapStyle={{}}
              />
              <RadioButtonLabel
                obj={item}
                index={item.value}
                labelHorizontal={true}
                onPress={() => this.props.radioSelectFunction(item.value)}
              />
            </RadioButton>
          )}
        />
      </View>
    )
  }
}

export default class CariInstitusi extends Component {
  constructor() {
    super()

    this.state = {
      collapsed_1: true,
      collapsed_2: true,
      search: "",
      data: [
        {
          label: "SMA Edu Global School",
          type: "sekolah",
          value: 0
        },
        {
          label: "SMP Edu Global School",
          type: "sekolah",
          value: 1
        },
        {
          label: "Edulab Cilaki",
          type: "bimbel",
          value: 2
        },
        {
          label: "Edulab Kalimantan No. 7",
          type: "bimbel",
          value: 3
        }
      ],
      isSelected: -1,
    }
  }

  redirect() {
    if (this.state.isSelected != -1)
      this.props.navigation.navigate("ProfileInstitusi", { indexInstitusi: this.state.isSelected })
  }

  updateSearch = search => {
    this.setState({
      search: search
    })
  }

  buttonStyle() {
    if (this.state.isSelected === -1) {
      return {
        width: "90%", backgroundColor: "#CCD2D9", alignSelf: "center", marginBottom: 10, elevation: 0, opacity: 0.5
      }
    } else {
      return {
        width: "90%", backgroundColor: "#CC0611", alignSelf: "center", marginBottom: 10, elevation: 0
      }
    }
  }

  buttonDisabled() {
    if (this.state.isSelected == -1) {
      return true
    } else {
      return false
    }
  }

  render() {
    return (
      <Container>
        <StatusBar translucent={true} hidden={true} />
        <Header
          noShadow
          iosBarStyle={"dark-content"}
          style={{ backgroundColor: '#fff', borderBottomWidth: 3, borderBottomColor: "#F5F5F5" }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" color="#CC0611" />
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>Cari Institusi</Title>
          </Body>
          <Right style={styles.headerRight}>
            <Button transparent>
              <Icon name="tune" color="#CC0611" />
            </Button>
          </Right>
        </Header>
        <Content style={{ backgroundColor: 'white' }} contentContainerStyle={{ alignItems: "center", width: "100%" }}>
          <View style={{ width: "100%" }}>
            <View style={{ width: "100%", alignItems: "center", aspectRatio: 8 / 1 }} >
              <SearchBar
                onChangeText={this.updateSearch}
                value={this.state.search}
                lightTheme={true}
                containerStyle={{ backgroundColor: "#FFF", width: "75%", borderTopWidth: 0, borderBottomWidth: 0, height: "80%", alignItems: "center", justifyContent: "center" }}
                inputContainerStyle={{ backgroundColor: "#F5F5F5", borderRadius: 25, height: "80%" }}
                inputStyle={{ fontSize: 16, color: "#5A6978" }}
                round={true}
              />
            </View>
            <ScrollableTabView
              style={{ width: "100%", aspectRatio: 1 / 1.5 }}
              initialPage={0}
              renderTabBar={() => <ScrollableTabBar style={{ width: "100%", marginBottom: devHeight/40 }} />}
              tabBarActiveTextColor={"#5A6978"}
              tabBarInactiveTextColor={"#969FAA"}
              tabBarUnderlineStyle={{ backgroundColor: "#CA020F" }}
              tabBarTextStyle={{ fontSize: 14 }}
            >
              <ScrollView tabLabel="Sekolah" >
                <RadioForm>
                  <CategoryCollapsible
                    data={this.state.data.filter((item) => item.type == "sekolah").map((item) => item)}
                    type="sekolah"
                    isSelected={this.state.isSelected}
                    collapsed={this.state.collapsed_1}
                    collapseFunction={() => this.setState({ collapsed_1: !this.state.collapsed_1 })}
                    radioSelectFunction={(i) => this.setState({ isSelected: i })}
                  />
                </RadioForm>
              </ScrollView>
              <ScrollView tabLabel="Bimbel">
                <RadioForm>
                  <CategoryCollapsible
                    data={this.state.data.filter((item) => item.type == "bimbel").map((item) => item)}
                    type="bimbel"
                    isSelected={this.state.isSelected}
                    collapsed={this.state.collapsed_2}
                    collapseFunction={() => this.setState({ collapsed_2: !this.state.collapsed_2 })}
                    radioSelectFunction={(i) => this.setState({ isSelected: i })}
                  />
                </RadioForm>
              </ScrollView>
            </ScrollableTabView>
          </View>
        </Content>
        <View>
          <Button disabled={this.buttonDisabled()} block rounded onPress={() => this.redirect()} style={this.buttonStyle()} >
            <Text uppercase={false} >Lanjut</Text>
          </Button>
        </View>
      </Container>
    );
  }

}

