import { createSwitchNavigator } from "react-navigation";
import AppNavs from "./AppNavigator";
import Loading from "../containers/LoadingContainer";
import Login from "../containers/LoginContainer";
import AuthNavs from "./AuthNavigator";

export default createSwitchNavigator(
  { Loading, AppNavs, AuthNavs },
  {
    initialRouteName: "Loading"
  }
);

