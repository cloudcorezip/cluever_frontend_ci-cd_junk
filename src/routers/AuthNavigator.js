import { createStackNavigator } from "react-navigation";
import Login from "../containers/LoginContainer";
import Register from "../containers/RegisterContainer";
import Forgetpass from "../containers/ForgetpassContainer";

export default createStackNavigator(
  {
    Forgetpass: { screen: Forgetpass },
    Login: { screen: Login },
    Register: { screen: Register }
  },
  {
    initialRouteName: "Login",
    headerMode: "none"
  }
);

