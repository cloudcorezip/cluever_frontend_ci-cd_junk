import React, { Component } from "react";
import SwitchNavigator from "./SwitchNavigator";
import { Root } from "native-base";
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import CombineReducer from '../reducers';
import configureStore from '../stores/configureStore'
import { PersistGate } from "redux-persist/integration/react";

//const store = createStore(CombineReducer);

export default class App extends Component {
  constructor(props){
    super(props)
    const { persistor, store } = configureStore()
    this.persistor = persistor;
    this.store = store;
  }

  render() {
    return (
      <Root>
        <Provider store={this.store}>
          <PersistGate persistor={this.persistor}>
            <SwitchNavigator/>
          </PersistGate>
        </Provider>
      </Root>
    );
  }
}

