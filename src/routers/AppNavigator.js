import React from "react";
import { createStackNavigator } from "react-navigation";
import SideBar from "../containers/SlidebarContainer";
import Home from "../containers/HomeContainer";
import Modal from "../containers/ModalContainer";
import BiayaPendidikanIndex from "../containers/BiayaPendidikanContainer/Index";
import BiayaPendidikanPayment from "../containers/BiayaPendidikanContainer/Payment";
import Register from "../containers/RegisterContainer";
import Forgetpass from "../containers/ForgetpassContainer";
import Tagihan from "../containers/TagihanContainer";
import CariInstitusi from "../containers/CariInstitusiContainer";
import UnderDevelopment from "../components/UnderDevelopment";
import DetailPembayaran from "../containers/DetailPembayaranContainer";
import ProfileInstitusi from "../containers/ProfileInstitusiContainer";
import FormPengajuan from "../containers/FormPengajuanContainer";
import FormPenanggungJawab from "../containers/FormPenanggungJawabContainer";
import ProfileUser from "../containers/ProfileUserContainer";
import RiwayatTransaksi from "../containers/RiwayatTransaksiContainer";
import Feeds from "../containers/FeedsContainer";
import Notification from "../containers/NotificationContainer";

export default createStackNavigator(
  {
    Home: { 
        screen: Home,
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
        }
    },
    Modal: { screen: Modal },
    BiayaPendidikanIndex: { screen: BiayaPendidikanIndex },
    BiayaPendidikanPayment: { screen: BiayaPendidikanPayment },
    Register: { screen: Register },
    Forgetpass: { screen: Forgetpass },
    Tagihan: { screen: Tagihan },
    CariInstitusi: { screen: CariInstitusi },
    UnderDevelopment: { screen: UnderDevelopment},
    ProfileInstitusi: { screen: ProfileInstitusi },
    DetailPembayaran: { screen: DetailPembayaran },
    FormPengajuan: { screen: FormPengajuan },
    FormPenanggungJawab: { screen: FormPenanggungJawab},
    ProfileUser : { screen: ProfileUser },
    RiwayatTransaksi : { screen: RiwayatTransaksi },
    Feeds: { screen: Feeds},
    Notification : { screen: Notification },
  },
  {
    initialRouteName: "Home",
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
  }
);

