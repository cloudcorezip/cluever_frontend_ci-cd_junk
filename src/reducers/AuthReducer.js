import * as ACTION_TYPES from "../constants/ActionTypes";
import axios from "axios"
import { AsyncStorage } from "react-native";
import { createNewToken } from "../utils"

const INITIAL_STATE = {
  token: undefined
}

export default AuthReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_LOGOUT_REQUEST:
      return AsyncStorage.removeItem('token');
    case SET_USER_TOKEN:
      newstate = { ...state }
      newstate.token = action.payload.token
      return newstate
    default:
      return state
  }
};
