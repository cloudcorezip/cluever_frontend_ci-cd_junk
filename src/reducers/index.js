import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import { reducer as modalReducer } from "redux-modal";
import AuthReducer from "./AuthReducer";
import ApplicationFormReducer from "./ApplicationFormReducer";
import InstallmentReducer from "./InstallmentReducer";
import PaymentReducer from "./PaymentReducer";
import ProfileReducer from "./ProfileReducer";
import TagihanReducer from "./TagihanReducer"
export default combineReducers({
  form: formReducer,
  modal: modalReducer,
  auth: AuthReducer,
  appform: ApplicationFormReducer,
  installment: InstallmentReducer,
  payment: PaymentReducer,
  profile: ProfileReducer,
  tagihan: TagihanReducer
});
