import axios from "axios";
import * as  ACTION_TYPES from "../constants/ActionTypes"
import { GET_INSTALLMENTS_URI } from "../constants/apis";

var INITIAL_STATE = {
  items: {}
}

export default InstallmentReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_INSTALLMENT:
      newstate = {...state}
      newstate.items = action.payload.data
      return newstate
    default:
      return state
  }
}
