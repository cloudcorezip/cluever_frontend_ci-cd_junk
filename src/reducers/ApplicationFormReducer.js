import * as ACTION_TYPES from "../constants/ActionTypes";
//import axios from "axios"

const INITIAL_STATE = {
  fullname: undefined,
  birthplace: undefined,
  dob: undefined,
  address: undefined,
  phone_no: undefined,
  email: undefined,
}

export default ApplicationFormReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FORM_SUBMIT_APPLICATION:
      newstate = action.payload.data
      console.log(newstate)
      return newstate
    default:
      return state
  }
};
