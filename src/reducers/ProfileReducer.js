import axios from "axios";
import * as  ACTION_TYPES from "../constants/ActionTypes"

const INITIAL_STATE = {
  data: undefined
}

export default ProfileReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_PROFILE:
      newstate = {...state}
      newstate.data = action.payload.data
      return newstate
    default:
      return state
  }
};