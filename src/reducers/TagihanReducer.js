import axios from "axios";
import * as ACTION_TYPES from "../constants/ActionTypes"
import {TAGIHAN_USER_URI} from "../constants/apis"

var INITIAL_STATE = {
  data: undefined
}

export default TagihanReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case SET_TAGIHAN:
      newstate = {...state}
      newstate.data = action.payload.data
      return newstate
    default:
      return state
  }
}